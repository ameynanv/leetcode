#include <iostream>
#include <vector>

using namespace std;

class NQueens {
    
private:
    int N;
    vector<vector<string>> result;
    vector<int> positions;
    
    void solveQueens(int row) {
        if (row >= N) {
            result.push_back(printPositions());
        }
        for (int i = 0; i < N; i++) {
            if (isValidPosition(row, i)) {
                positions[row] = i;
                solveQueens(row + 1);
            }
        }
    }
    
    bool isValidPosition(int row, int i) {
        for (int k = 0; k < row; k++) {
            if (positions[k] == i) return false;
            if ((positions[k] - i) == (row - k)) return false;
            if ((positions[k] - i) == (k - row)) return false;
        }
        return true;   
    }
    
    vector<string> printPositions() {
        vector<string> answer;
        for (int i = 0; i < N; i++) {
            string line = "";
            for (int j = 0; j < N; j++) {
                if (positions[i] == j)
                    line += "Q";
                else
                    line += ".";
            }
            answer.push_back(line);
        }
        return answer;
    }
    
public:
    vector<vector<string>> solveNQueens(int n) {
        if (n < 1) return result;
        N = n;
        for (int i = 0; i < n; i++) {
            positions.push_back(-1);
        }
        solveQueens(0);
        return result;
    }
    
    int totalNQueens(int n) {
        solveNQueens(n);
        return result.size();
    }
    
};

int main(int argc, char**args) {
    NQueens service;
    vector<vector<string>> result = service.solveNQueens(8);
    for (uint32_t r = 0; r < result.size(); r++) {
        cout << "Solution #" << r + 1 << endl;
        for (uint32_t c = 0; c < 8; c++) {
            cout << result[r][c] << endl;
        }
    }
    NQueens service2;
    cout << "Total Solutions = " << service2.totalNQueens(8) << endl;
    return 0;
}