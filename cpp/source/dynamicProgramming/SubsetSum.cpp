#include <iostream>
#include <vector>

using namespace std;

class SubsetSum {
    public:
        bool isSubsetSum(vector<int>& nums) {
            int sum = 0;
            for (uint32_t i = 0; i < nums.size(); i++) {
                sum += nums[i];
            }
            if (sum%2 != 0) return false;
            return isSubsetSum(nums, nums.size(), sum/2);
        }
        
        bool isSubsetSum(vector<int>&nums, int range, int target) {
            if (target == 0) return true;
            if (range <= 0) return false;
            return isSubsetSum(nums, range - 1, target) || isSubsetSum(nums, range-1 , target - nums[range - 1]);
        }
};

int main(int argc, char**args) {
    vector<int> nums = {1,3,5,5,2,1,1,6};
    SubsetSum service;
    cout << service.isSubsetSum(nums) << endl;
    return 0;
}