#include <iostream>

using namespace std;

class PathFinder {
private:
    int** F;
   
    int paths(int i, int j, int m, int n) {
        if (i >= m) return 0;
        if (j >= n) return 0;
        if (F[i][j] != -1) return F[i][j];
        int p = paths(i+1,j,m,n) + paths(i,j+1,m,n);
        F[i][j] = p;
        return F[i][j];
    }
   
public:
    int findNumPaths(int m, int n) {
        m++;
        n++;
        F = new int*[m];
        for (int r= 0;r < m; r++) {
            F[r] =  new int[n];
            for (int c = 0; c < n; c++) {
                F[r][c] = -1;
             }
        }
        for (int r = 0; r < m-1; r++)
            F[r][n-1] = 1;
        for (int c = 0; c < n-1; c++)
            F[m-1][c] = 1;         
        F[m-1][n-1] = 0;
        return paths(0,0,m,n);
    }
};


int main(int argc, char**argv) {
    PathFinder pf;
    cout << pf.findNumPaths(1,1) << endl;
}