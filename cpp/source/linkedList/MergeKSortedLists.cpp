#include <iostream>
#include <vector>
#include <queue>
#include "ListNode.h"

using namespace std;

class MergeKSortedLists {
    
private:
    struct compare {
        bool operator() (const ListNode* x, const ListNode* y) {
            return x->val > y->val;
        }
    };
    
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        
        priority_queue<ListNode*, vector<ListNode*> , compare> queue;
        for (uint32_t i = 0; i < lists.size(); i++)
            if (lists[i] != NULL)
                queue.push(lists[i]);
        if (queue.empty()) {
            return NULL;
        }
        ListNode* result = queue.top();
        queue.pop();
        
        if (result->next != NULL)
            queue.push(result->next);
        
        ListNode* tail = result;
        while(!queue.empty()) {
            ListNode *node = queue.top();
            queue.pop();
            tail->next = node;
            tail = tail->next;
            if (node->next != NULL)
                queue.push(node->next);
        }
        return result;
    }
};

int main(int argc, char**argv) {
    ListNode* list1 = new ListNode(1);
    list1->next = new ListNode(2);
    list1->next->next = new ListNode(5);
    ListNode* list2 = new ListNode(3);
    list2->next = new ListNode(6);
    list2->next->next = new ListNode(9);
    ListNode* list3 = new ListNode(4);
    list3->next = new ListNode(7);
    list3->next->next = new ListNode(8);
    
    vector<ListNode*> lists;
    lists.push_back(list1);
    lists.push_back(list2);
    lists.push_back(list3);
    
    MergeKSortedLists service;
    
    ListNode* result = service.mergeKLists(lists);
    printList(result);
    return 0;
}
