#include <iostream>
#include "ListNode.h"

using namespace std;

/**
 * https://leetcode.com/problems/delete-node-in-a-linked-list/
 */
class DeleteNodeInLinkedList {
public:
	void deleteNode(ListNode* node) {
		*node = *node->next;
	}
};

int main(int argc, char** args) {
	DeleteNodeInLinkedList service;
	ListNode* head = new ListNode(1);
	head->next = new ListNode(2);
	head->next->next = new ListNode(3);
	head->next->next->next = new ListNode(4);
	service.deleteNode(head);
	printList(head);
	return 0;
}

