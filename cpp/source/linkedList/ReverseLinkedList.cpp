#include <iostream>

using namespace std;

class LinkNode {
    public:
        LinkNode* next;
        int val;
        
        LinkNode(int value) {
            val = value;
            next = NULL;
        }
};


class ReverseLinkedList {
    private:
        LinkNode* deepCopy(LinkNode* orgHead) {
            if (orgHead == NULL) return NULL;
            LinkNode* head = new LinkNode(orgHead->val);
            LinkNode* tmp = head;
            while (orgHead->next != NULL) {
                tmp->next = new LinkNode(orgHead->next->val);
                tmp = tmp->next;
                orgHead = orgHead->next;
            }
            return head;
        }
    public:
        LinkNode* reverseLinkedList(LinkNode* orgHead) {
            LinkNode *head = deepCopy(orgHead);
            if (head == NULL || head->next == NULL) return head;
            LinkNode *current = head->next;
            LinkNode *prev = head;
            prev->next = NULL;
            while (current != NULL) {
                LinkNode *tmp = current->next;
                current->next = prev;
                prev = current;
                current = tmp;
            }
            return prev;
        }
        
        void printLinkedList(LinkNode* head) {
            LinkNode* tmp = head;
            while(tmp != NULL) {
                cout << tmp->val << " ";
                tmp = tmp->next;
            }
            cout << endl;
        }
};

int main(int argc, char**args) {
    LinkNode* head = new LinkNode(1);
    head->next = new LinkNode(2);
    head->next->next = new LinkNode(3);
    ReverseLinkedList service;
    LinkNode* rev = service.reverseLinkedList(head);
    service.printLinkedList(head);
    service.printLinkedList(rev);
    
    return 0;
}
