/*
 * ListNode.h
 *
 *  Created on: Sep 27, 2015
 *      Author: Amey
 */

#ifndef SOURCE_LINKEDLIST_LISTNODE_H_
#define SOURCE_LINKEDLIST_LISTNODE_H_
#include <iostream>

struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x) :
			val(x), next(NULL) {
	}
};

void printList(ListNode* head) {
	ListNode* tmp = head;
	while (tmp != NULL) {
		std::cout << tmp->val << " ";
		tmp = tmp->next;
	}
	std::cout << std::endl;
}

#endif /* SOURCE_LINKEDLIST_LISTNODE_H_ */
