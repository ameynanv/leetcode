#include <iostream>
#include <vector>
#include <string>

using namespace std;
// http://www.careercup.com/question?id=15555796


class HasPermutation {
    private:
        vector<int> buildHistogram(string str) {
            vector<int> hist;
            for (int i = 0; i < 128; i++)
                hist.push_back(0);
            for (size_t i = 0; i < str.size(); i++)
                hist[str.at(i)]++;
            return hist;
        }
        
        bool compareHistograms(vector<int> one, vector<int> second) {
            for (int i = 0; i < 128; i++) {
                if (one[i] != second[i]) return false;
            }
            return true;
        }

    public:
        bool hasPermutation(string source, string target) {
            // Edge Cases:
            if (target.size() > source.size()) return false;
            vector<int> targetHist = buildHistogram(target);
            vector<int> sourceHist = buildHistogram(source.substr(0, target.size()));
            for (size_t i = 1; i <= (source.size() - target.size()); i++) {
                sourceHist[source.at(i-1)]--;
                sourceHist[source.at(i + target.size()- 1)]++;
                if (compareHistograms(sourceHist, targetHist)) return true;
            }
            return false;
            
        }
};

int main(int argc, char**argv) {
    HasPermutation service;
    cout << service.hasPermutation("abcdefgh", "hfeg") << endl;
    cout << service.hasPermutation("abcdefgh", "") << endl;
}