#include <iostream>

using namespace std;


class Base {
    friend class C;
    private:
        int a;
    public:
        Base() {
            a = 5;
            cout << "Base constructor" << endl;
        }
        
        virtual int val() {
            return a;
        }
};


class Derived1 : public Base {
    friend class C;
    private:
        int b = 99;
        int a = 0;
    public:
        Derived1() {
            a = 6;
            cout << "Derived1 constructor" << endl;
        }
        
        int val() {
            return 0;
        }
};

class C {
    public:
        void print(Derived1* b) {
            cout << b->a << endl;
        }
};


int main(int argc, char**argv) {
    C c;
    Derived1* b = new Derived1();
    c.print(b);
}