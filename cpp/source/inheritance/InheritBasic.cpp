#include <iostream>

using namespace std;

class Base {
    public:
        int a;
    public:
        Base() {
            a = 5;
            cout << "Base constructor" << endl;
        }
        
        virtual int val() {
            return a;
        }
};

class Derived1 : public Base {
    public:
        Derived1() {
            a = 6;
            cout << "Derived1 constructor" << endl;
        }
        
        int val() {
            return 0;
        }
};

int main(int argc, char**args) {
    Base* d = new Derived1();
    cout << d->val() << endl;
    
}