#include <iostream>
#include <map>

using namespace std;

int main() {
    map<int, int> treeMap;
    treeMap[1] = 3;
    treeMap[2] = 4;
    treeMap[3] = 5;
    
    cout << treeMap.find(3)->second << endl;
    if (treeMap.find(4) != treeMap.end())
        cout << treeMap.find(4)->second << endl;
    
}