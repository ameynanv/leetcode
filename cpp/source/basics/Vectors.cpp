#include <iostream>
#include <vector>

using namespace std;

void printVector(vector<int>& nums) {
    for (vector<int>::iterator it = nums.begin(); it != nums.end(); it++)
        cout << *it << " ";
    cout << endl;
}

void printVector(vector<int>* nums) {
    for (vector<int>::iterator it = nums->begin(); it != nums->end(); it++)
        cout << *it << " ";
    cout << endl;
}

void printReverseVector(vector<int>* nums) {
    for (vector<int>::reverse_iterator it = nums->rbegin(); it != nums->rend(); it++)
        cout << *it << " ";
    cout << endl;
}

void addElement(vector<int>* nums, int number) {
    nums->push_back(number);
}

void addElement(vector<int>& nums, int number) {
    nums.push_back(number);
}

int main() {
    vector<int> nums = {1,2,3};
    printVector(nums);
    printVector(&nums);
    addElement(nums, 4);
    addElement(&nums, 5);
    printVector(&nums);
    printReverseVector(&nums);
    return 0;
}