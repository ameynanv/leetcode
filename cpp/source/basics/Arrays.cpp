#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

bool revComparator(int i, int j) {
    return i > j;
}

int main(int argc, char**argv) {
    int k = atoi(argv[1]);
    int a[k][k];
    for (int i = 0; i < k; i++) {
        for (int j = 0; j < k; j++) {
            a[i][j] = i*k+j;
        }
    }
    for (int i = 0; i < k; i++) {
        for (int j = 0; j < k; j++) {
            cout << a[i][j] << "\t";
        }
        cout << endl;
    }
    
    int b[] = {1,2,3,4,5};
    vector<int> vecB(b, b + (sizeof(b) / sizeof(int))); 
    for (uint32_t i = 0; i < vecB.size(); i++) {
        cout << vecB[i] << " ";
    }
    cout << endl;
    sort(vecB.begin(), vecB.end(), revComparator);
    for (uint32_t i = 0; i < vecB.size(); i++) {
        cout << vecB[i] << " ";
    }    
    cout << endl;
    
}
