#include<iostream>
#include <queue>

using namespace std;

struct MyComp {
    bool operator()(int i, int j) {
        return i > j;
    }
};

int main() {
    priority_queue<int, vector<int>, MyComp> maxPq;
    queue<int> q;
    
    maxPq.push(1);
    maxPq.push(3);
    maxPq.push(2);
    
    while(!maxPq.empty()) {
        cout << maxPq.top() << " ";
        maxPq.pop();
    }
    cout << endl;
    
    q.push(5);
    q.push(3);
    q.push(2);
    while(!q.empty()) {
        cout << q.front() << " ";
        q.pop();
    }
    cout << endl;
}

