#include<iostream>
#include<set>
#include<unordered_set>

using namespace std;

int main() {
    set<int> treeSet;
    treeSet.insert(2);
    treeSet.insert(2);
    treeSet.insert(1);
    treeSet.insert(1);
    treeSet.insert(2);
    treeSet.insert(2);
    
    for (set<int>::iterator it = treeSet.begin(); it != treeSet.end(); it++)
        cout << *it << " ";
    cout << endl;
    cout << (treeSet.find(3) != treeSet.end())<< endl;
    
    
    
    unordered_set<int> hashSet;
    hashSet.insert(3);
    hashSet.insert(3);
    hashSet.insert(1);
    hashSet.insert(1);
    hashSet.insert(1);
    for (unordered_set<int>::iterator it = hashSet.begin(); it != hashSet.end(); it++)
        cout << *it << " ";
    cout << endl;
    cout << (hashSet.find(2) != hashSet.end())<< endl;
}