#include <iostream>
#include "TreeNode.h"

using namespace std;

class LCAOfBST {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        TreeNode* node = root;
        while(((node->val - p->val) * (node->val - q->val)) > 0) {
            node = node->val > p->val ? node->left : node->right;
        }
        return node;
    }
};

int main(int argc, char**args) {
    TreeNode* root = new TreeNode(6);
    root->left = new TreeNode(2);
    root->left->left = new TreeNode(1);
    root->left->right = new TreeNode(4);
    root->left->right->left = new TreeNode(3);
    root->left->right->right = new TreeNode(5);
    root->right = new TreeNode(7);
    root->right->right = new TreeNode(9);
    root->right->right->left = new TreeNode(8);
    LCAOfBST service;
    cout << service.lowestCommonAncestor(root, root->left->right->right, root->left->right->left)->val << endl;
    return 0;
}
