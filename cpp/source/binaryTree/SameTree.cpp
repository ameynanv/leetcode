#include <iostream>
#include "TreeNode.h"

using namespace std;

class SameTree {
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
        if (p == NULL && q == NULL) return true;
        if ((p == NULL && q != NULL) || (q == NULL && p != NULL)) return false;
        else {
            if (p->val != q->val) return false;
            return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
        }
    }
};

int main(int argc, char**args) {
    SameTree service;
    TreeNode* root1 = new TreeNode(1);
    root1->left = new TreeNode(2);
    root1->right = new TreeNode(3);
    TreeNode* root2 = new TreeNode(1);
    root2->left = new TreeNode(2);
    
    
    bool result = service.isSameTree(root1, root2);
    cout << result << endl;
}