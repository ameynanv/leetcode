#include <iostream>
#include "TreeNode.h"

using namespace std;

/**
 * https://leetcode.com/problems/maximum-depth-of-binary-tree/
 */
class MaximumDepthOfBinaryTree {
public:
    int maxDepth(TreeNode* root) {
    	if (root == NULL) return 0;
    	return findMaxDepth(root, 1);
    }

private:
    int findMaxDepth(TreeNode* node, int level) {
    	int leftDepth = level, rightDepth = level;
    	if (node->left != NULL)
    		leftDepth = findMaxDepth(node->left, level + 1);
    	if (node->right != NULL)
    		rightDepth = findMaxDepth(node->right, level + 1);
    	return leftDepth > rightDepth ? leftDepth: rightDepth;
    }
};

int main(int argc, char** argv) {
	TreeNode* root = new TreeNode(1);
	root->left = new TreeNode(2);
	root->right = new TreeNode(3);
	root->left->left = new TreeNode(4);
	MaximumDepthOfBinaryTree service;
	cout << service.maxDepth(root) << endl;
	return 0;
}
