/*
 * TreeNode.h
 *
 *  Created on: Sep 26, 2015
 *      Author: Amey
 */

#ifndef SOURCE_BINARYTREE_TREENODE_H_
#define SOURCE_BINARYTREE_TREENODE_H_
#include <iostream>
#include <vector>
#include <queue>

using namespace std;

struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) :
			val(x), left(NULL), right(NULL) {
	}
};


void dfs(TreeNode *node, vector<vector<int>>& result, unsigned int level) {
	if (node == NULL) return;
	if (result.size() < level) {
		vector<int> list = {node->val};
		result.push_back(list);
	} else {
		result[level - 1].push_back(node->val);
	}
	if (node->left != NULL)
		dfs(node->left, result, level+1);
	if (node->right != NULL)
		dfs(node->right, result, level+1);	
}

void printTree(TreeNode *root) {
	vector<vector<int>> result;
	dfs(root, result, 1);
	cout << "{";
	for (unsigned int i = 0; i < result.size(); i++) {
		cout << "[" << i << " : ";
		for (unsigned int j = 0; j < result[i].size(); j++) {
			cout << result[i][j] << " ";
		}
		cout << "] ";
	}
	cout << "}" << endl;
}


#endif /* SOURCE_BINARYTREE_TREENODE_H_ */
