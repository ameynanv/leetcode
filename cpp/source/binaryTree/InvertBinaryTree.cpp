#include <iostream>
#include "TreeNode.h"

using namespace std;

class InvertBinaryTree {
public:
    TreeNode* invertTree(TreeNode* root) {
        if (root == NULL) return NULL;
        TreeNode* newLeft = invertTree(root->right);
        TreeNode* newRight = invertTree(root->left);
        root->left = newLeft;
        root->right = newRight;
        return root;
    }
};

int main(int argc, char**args) {
    TreeNode* root = new TreeNode(1);
    root->left = new TreeNode(2);
    root->right = new TreeNode(3);
    printTree(root);
    InvertBinaryTree service;
    service.invertTree(root);
    printTree(root);
    return 0;
}