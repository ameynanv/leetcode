#include <iostream>
#include <vector>

using namespace std;

class FindMinRotatedSortedArray {
public:
    int findMin(vector<int>& nums) {
        return findMin(nums, 0, nums.size() - 1);
    }
    
private:
    int findMin(vector<int>&nums, uint32_t start, uint32_t end) {
        if (start == end) return nums[start];
        if (end - start == 1) return nums[start] > nums[end] ? nums[end] : nums[start];
        if (nums[start] < nums[end]) {
            return nums[start];
        }
        else {
            uint32_t mid = (start + end) >> 1;
            if (nums[mid] < nums[mid-1]) return nums[mid];
            if (nums[mid] >= nums[start]) {
                return findMin(nums, mid+1, end);
            } else {
                return findMin(nums, start, mid-1);
            }
        }
    }
};

int main(int argc, char**args) {
    FindMinRotatedSortedArray service;
    vector<int> nums = {3,6,6,8,0,1,2,3};
    cout << service.findMin(nums) << endl;
    
}