#include <iostream>
#include <set>
#include <vector>

using namespace std;

class ContainsDuplicate {
public :
    bool containsDuplicate(vector<int>& nums) {
        set<int> uniqueSet;
        for (uint32_t i = 0; i < nums.size(); i++)
            uniqueSet.insert(nums[i]);
        return uniqueSet.size() != nums.size();
    }
};

int main(int argc, char**argv) {
    vector<int> nums = {1,0};
    ContainsDuplicate service;
    cout << service.containsDuplicate(nums) << endl;
    return 0;
}