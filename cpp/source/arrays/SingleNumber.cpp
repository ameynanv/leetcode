#include <iostream>
#include <stdlib.h>
#include <stdexcept>
#include <vector>
#include <stdio.h>

using namespace std;

class SingleNumber {
public:
	int singleNumber(vector<int>& nums) {
		if (nums.size() == 0) {
			throw std::invalid_argument("Input is Empty");
		}
		int ans = 0;
		for (vector<int>::iterator it = nums.begin(); it != nums.end(); ++it)
			ans = ans ^ (*it);
		return ans;
	}
};

int main(int argc, char** args) {
	try {
		SingleNumber service;
		vector<int> nums = { 1, 2, 1, 2, 3 };
		cout << service.singleNumber(nums) << endl;
	} catch (std::invalid_argument& e) {
		cout << e.what() << endl;
	}
	return 0;
}
