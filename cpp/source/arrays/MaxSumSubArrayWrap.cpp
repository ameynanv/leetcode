#include <iostream>
#include <limits.h>
#include <vector>

using namespace std;

class MaxSubArraySumWrap {
private:
    int maxSumSubArray(vector<int>& nums) {
        int maxSum = INT_MIN;
        int sum  = 0;
        for (uint32_t i = 0; i < nums.size(); i++) {
            if (sum < 0)
                sum = nums[i];
            else
                sum += nums[i];
            if (sum > maxSum)
                maxSum = sum;
        }
        return maxSum;
    }
    
    int minSumSubArray(vector<int>& nums) {
        int minSum = INT_MAX;
        int sum  = 0;
        for (uint32_t i = 0; i < nums.size(); i++) {
            if (sum > 0)
                sum = nums[i];
            else
                sum += nums[i];
            if (sum < minSum)
                minSum = sum;
        }
        return minSum;
    }
    
    int arraySum(vector<int>& nums) {
        int sum  = 0;
        for (uint32_t i = 0; i < nums.size(); i++) {
            sum += nums[i];
        }
        return sum;
    }
public:

    int maxSumSubArrayWrap(vector<int>& nums) {
        int maxSum = maxSumSubArray(nums);
        int minSum = minSumSubArray(nums);
        int total = arraySum(nums);
        int sumWithWrap = total - minSum;
        return maxSum > sumWithWrap ? maxSum : sumWithWrap;
    }
};

int main(int argc, char**args) {
    MaxSubArraySumWrap service;
    vector<int> nums = {11, 10, -20, 5, -3, -5, 8, -13, 10};
    cout << service.maxSumSubArrayWrap(nums) << endl;
    return 0;
}