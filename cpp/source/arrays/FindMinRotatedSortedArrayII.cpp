#include <iostream>
#include <vector>

using namespace std;

class FindMinRotatedSortedArrayII {
public:
    int findMin(vector<int>& nums) {
        return findMin(nums, 0, nums.size() - 1);
    }
    
private:
    int findMin(vector<int>&nums, uint32_t start, uint32_t end) {
        if (start == end) return nums[start];
        if (end - start == 1) return nums[start] > nums[end] ? nums[end] : nums[start];
        if (nums[start] < nums[end]) {
            return nums[start];
        }
        else {
            uint32_t mid = (start + end) >> 1;
            if (nums[mid] < nums[mid-1]) return nums[mid];
            if (nums[mid] > nums[start]) {
                return findMin(nums, mid+1, end);
            } else if (nums[mid] < nums[start]) {
                return findMin(nums, start, mid-1);
            } else {
                return findMin(nums, start + 1, end);
            }
        }
    }
};

int main(int argc, char**args) {
    FindMinRotatedSortedArrayII service;
    vector<int> nums = {10,10,10,1,10};
    cout << service.findMin(nums) << endl;
    
}