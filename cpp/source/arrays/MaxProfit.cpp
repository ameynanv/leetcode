#include <iostream>
#include <vector>

using namespace std;

class MaxProfit {
public:
    int maxProfit(vector<int>& prices) {
        if (prices.size() < 2) return 0;
        int profit = 0;
        for (unsigned int i = 0; i < prices.size() - 1; i++) {
            if (prices[i+1] - prices[i] > 0)
                profit += prices[i+1] - prices[i];
        }
        return profit;
    }
};

int main(int argc, char**argv) {
    MaxProfit service;
    vector<int> prices = {1,2,1,4,1,1,1};
    cout << service.maxProfit(prices) << endl;
    return 0;
}