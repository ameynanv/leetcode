#include <iostream>
#include <vector>

using namespace std;

class MoveZeroes {
    public:
    void moveZeroes(vector<int>& nums) {
        if (nums.empty()) return;
        unsigned int wi,ri;
        for (wi = 0, ri = 0 ; ri < nums.size(); ri++) {
            if (nums[ri] == 0) continue;
            else nums[wi++] = nums[ri];
        }
        while(wi < nums.size()) {
            nums[wi++] = 0;
        }
    }
};

int main(int argc, char** args) {
    MoveZeroes service;
    vector<int> nums = {1,1,2,3,1,5};
    service.moveZeroes(nums);
    for (unsigned int i = 0; i < nums.size(); i++) {
        cout << nums[i] << " ";
        
    }
    return 0;
}