#include <iostream>
#include <stdlib.h>

using namespace std;

/**
 * https://en.wikipedia.org/wiki/Digital_root
 */

class AddDigits {
public:
    int addDigits(int num) {
    	int ans = num - 9 * (int)((num-1)/9);
    	return ans;
    }
};


int main(int argc, char** args) {
	AddDigits service;
	cout << service.addDigits(0) << endl;
	return 0;
}
