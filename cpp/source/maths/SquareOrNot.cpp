#include <iostream>

using namespace std;
// http://www.careercup.com/question?id=19638671
class SquareOrNot {
    public:
        // 1 + 3 + 5 .. + n = n^2
        bool squareOrNot(int num) {
            int cnt = 1;
            while (num > 0) {
                num -= cnt;
                cnt += 2;
            }
            if (num == 0) return true;
            return false;
        }
};

int main(int argc, char**args) {
    SquareOrNot service;
    cout << service.squareOrNot(81) << endl;
    cout << service.squareOrNot(80) << endl;
}