#include <iostream>

using namespace std;

class HammingWeight {
public:
    int hammingWeight(uint32_t n) {
        int ans = 0;
        while(n != 0x00) {
            if (n & 0x1) ans++;
            n = n >> 1;
        }
        return ans;
    }
};

int main(int argc, char**argv) {
    uint32_t num = 0b1101;
    HammingWeight service;
    cout << service.hammingWeight(num) << endl;
    return 0;
}