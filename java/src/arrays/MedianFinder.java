package arrays;
import java.util.PriorityQueue;
import java.util.Comparator;

public class MedianFinder {
    PriorityQueue<Integer> minPQ;
    PriorityQueue<Integer> maxPQ;
    
    public MedianFinder() {
        minPQ = new PriorityQueue<Integer>(1);
        maxPQ = new PriorityQueue<Integer>(1, new Comparator<Integer>() {
            public int compare(Integer num1, Integer num2) {
                return num2.compareTo(num1);
            }
        });
    }

    // Adds a number into the data structure.
    public void addNum(int num) {
        if (minPQ.size() == maxPQ.size()) {
            if (maxPQ.size() == 0) maxPQ.add(num);
            else if (maxPQ.peek() >= num) maxPQ.add(num);
            else minPQ.add(num);
        } else if (maxPQ.size() > minPQ.size()) {
            if (maxPQ.peek() >= num) {
                maxPQ.add(num);
                int temp = maxPQ.remove();
                minPQ.add(temp);
            } else {
                minPQ.add(num);
            }
        } else {
            if (minPQ.peek() < num) {
                minPQ.add(num);
                int temp = minPQ.remove();
                maxPQ.add(temp);
            } else {
                maxPQ.add(num);
            }
        }
    }

    // Returns the median of current data stream
    public double findMedian() {
        if (minPQ.size() == maxPQ.size()) return (minPQ.peek() + maxPQ.peek())/2.0;
        return maxPQ.size() > minPQ.size() ? maxPQ.peek() : minPQ.peek();
    }
};
