package arrays;
import java.util.Arrays;


public class Hindex {
        public int hIndex(int[] citations) {
        int L = citations.length;
        if (L < 1) return 0;
        int maxH = -1;
        Arrays.sort(citations);
        
        int max = citations[L-1];
        if (max == 0) return 0;
        int min = 1;
        for (int h = min; h <= max; h++) {
            if (condition1(citations, h) && condition2(citations, h)) {
                maxH = h;
            }
        }
        return maxH;
    }
    
    private boolean condition1(int[] citations, int h) {
        int L = citations.length;
        if (L-h >= 0) {
            return (citations[L-h] >= h);
        } else {
            return false;
        }
    }
    
    private boolean condition2(int[] citations, int h) {
        int L = citations.length;
        if (L-h-1 >= 0)
            return (citations[L-h-1] <= h);
        else 
            return true;
    }
    
    public static void main(String[] args) {
        int[] nums = {5,6};
        Hindex service =new Hindex();
        System.out.println(service.hIndex(nums));
    }
}