package arrays;

public class FindDuplicatesII {
    
    /*
        Given an array nums containing n + 1 integers where each integer is between 1 and n (inclusive), 
        prove that at least one duplicate number must exist. Assume that there is only one 
        duplicate number, find the duplicate one.
        
        Note:
        You must not modify the array (assume the array is read only).
        You must use only constant, O(1) extra space.
        Your runtime complexity should be less than O(n2).
        There is only one duplicate number in the array, but it could be repeated more than once.
    */  
    
    public int findDuplicate(int[] nums) {
        int slow = nums.length - 1;
        int fast = nums.length - 1;
        while (true) {
            slow = nums[slow] - 1;
            fast = nums[nums[fast] - 1] - 1;
            if (slow == fast) 
                break;
        }
        int restart = nums.length - 1;
        while(restart != slow) {
            restart = nums[restart] - 1;
            slow = nums[slow] - 1;
        }
        return slow + 1;
    }
    
    public static void main(String[] args) {
        int[] nums = {1,3,4,2,2};
        FindDuplicatesII service = new FindDuplicatesII();
        System.out.println(service.findDuplicate(nums));
    }
}