package arrays;

// http://www.geeksforgeeks.org/merge-one-array-of-size-n-into-another-one-of-size-mn/
public class MergeTwoSortedArraysII {

	private void sortArr(int[] arr) {
		int ir = arr.length - 1;
		for (int iw = arr.length - 1; iw >= 0; iw--) {
			while (ir >= 0 && arr[ir] == -1)
				ir--;
			if (ir >= 0) {
				arr[iw] = arr[ir];
				arr[ir] = -1;
			}
		}
	}

	public void merge(int[] arr, int[] nums) {
		sortArr(arr);
		int ar = 0;
		int aw = 0;
		int nr = 0;
		while (ar < arr.length && arr[ar] == -1)
			ar++;

		while (aw < arr.length) {
			if (ar < arr.length && nr < nums.length) {
				if (arr[ar] <= nums[nr]) {
					arr[aw++] = arr[ar++];
				} else {
					arr[aw++] = nums[nr++];
				}
			} else if (nr < nums.length) {
				arr[aw++] = nums[nr++];
			} else {
				arr[aw++] = arr[ar++];
			}
		}
	}

}
