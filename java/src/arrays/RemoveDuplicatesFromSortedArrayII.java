package arrays;

public class RemoveDuplicatesFromSortedArrayII {
    public int removeDuplicates(int[] nums) {
        if (nums.length <= 1) return nums.length;
        int read = 1;
        int write = 1;
        int count = 1;
        while (read < nums.length) {
            if (nums[read] == nums[write-1] && count >= 2) read++;
            else {
                if (nums[write - 1] == nums[read]) count++;
                else count = 1;
                nums[write++] = nums[read++];
            }
        }
        return write;
    }
}
