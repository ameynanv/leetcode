package arrays;

public class TrappingRainWater {
    public int trap(int[] height) {
        int start = 0;
        while(start + 1 < height.length && height[start] < height[start+1])
            start++;
        int end = height.length - 1;
        while(end - 1 >= 0 && height[end - 1] > height[end])
            end--;
        int total = 0;
        while(start < end) {
            int left = height[start];
            int right = height[end];
            if (left < right) {
                int i = start + 1;
                while(height[i] <= left && i < end) {
                    total += left - height[i];
                    i++;
                }
                start = i;
            } else {
                int i = end - 1;
                while(height[i] <= right && i > start) {
                    total += right - height[i];
                    i--;
                }
                end = i;
            }
        }
        return total;
    }
    
    public static void main(String[] args) {
    	int[] height = {1};
    	TrappingRainWater service = new TrappingRainWater();
    	System.out.println(service.trap(height));
    	
    }
}
