package arrays;

public class GameOfLife {
    public void gameOfLife(int[][] board) {
        int H = board.length;
        if (H == 0) return;
        int W = board[0].length;
        if (W == 0) return;
        for (int r = 0; r < H; r++) {
            for (int c = 0; c < W; c++) {
                int liveCells = getLiveCellsInNeighbor(board, r, c);   
                if (board[r][c]%2 == 1) {
                    if (liveCells < 2) killCell(board, r, c);    
                    else if (liveCells == 2 || liveCells == 3) keepAlive(board, r, c);
                    else if (liveCells > 3) killCell(board, r, c);
                } else {
                    if (liveCells == 3) keepAlive(board, r, c);
                }
            }
        }
        for (int r = 0; r < H; r++) {
            for (int c = 0; c < W; c++) {
                board[r][c] = board[r][c] / 2;
            }
        }
    }
    
    private void killCell(int[][] board, int r, int c) {
        board[r][c] += 0;
    }
    
    private void keepAlive(int[][] board, int r, int c) {
        board[r][c] += 1*2;
    }
    
    private int getLiveCellsInNeighbor(int[][] board, int r, int c) {
        int count = 0;
        count += getValue(board, r-1, c-1);
        count += getValue(board, r-1, c);
        count += getValue(board, r-1, c+1);
        count += getValue(board, r, c-1);
        count += getValue(board, r, c+1);
        count += getValue(board, r+1, c-1);
        count += getValue(board, r+1, c);
        count += getValue(board, r+1, c+1);
        return count;
    }
    
    private int getValue(int[][] board, int r, int c) {
        int H = board.length;
        int W = board[0].length;
        if (r >= 0 && r < H && c >= 0 && c < W) {
            return board[r][c] % 2;
        } else {
            return 0;
        }
    }
}
