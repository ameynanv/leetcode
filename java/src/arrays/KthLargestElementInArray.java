package arrays;
import java.util.PriorityQueue;
public class KthLargestElementInArray {

    public int findKthLargest(int[] nums, int k) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<Integer>();
        for (int i = 0; i < nums.length; i++) {
            if (minHeap.size() < k) minHeap.add(nums[i]);
            else if (minHeap.size() == k && minHeap.peek() < nums[i]) {
                minHeap.add(nums[i]);
                minHeap.remove();
            }
        }
        return minHeap.peek();
    }
    
	public static void main(String[] args) {
		KthLargestElementInArray service = new KthLargestElementInArray();
		int[] nums = {3,2,1,5,6,4};
		System.out.println(service.findKthLargest(nums, 2));

	}

}
