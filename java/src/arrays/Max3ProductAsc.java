package arrays;

import java.util.TreeSet;
import java.util.Iterator;
import java.util.NavigableSet;
/*
http://www.careercup.com/question?id=16813665

Given a sequence of non-negative integers find a subsequence of length 3 having maximum 
product with the numbers of the subsequence being in ascending order. 
Example: 
Input: 6 7 8 1 2 3 9 10 
Ouput: 8 9 10
*/
public class Max3ProductAsc {
    
    public int[] max3Product(int[] nums) {
        int maxProduct = Integer.MIN_VALUE;
        TreeSet<Integer> treeSet = new TreeSet<Integer>();
        for (int i = 0; i < nums.length; i++) {
            treeSet.add(nums[i]);
            int product = find3Product(treeSet, nums[i]);
            maxProduct = Math.max(maxProduct, product);
        }
        if (treeSet.size() < 3) return null;
        Iterator<Integer> it = treeSet.descendingIterator();
        int[] result = new int[3];
        result[2] = it.next();
        result[1] = it.next();
        result[0] = it.next();
        return result;
    }
    
    private int find3Product(TreeSet<Integer> treeSet, int maxNum) {
        int product = -1;
        NavigableSet<Integer> headSet = treeSet.headSet(maxNum, true);
        if (headSet.size() >= 3) {
            Iterator<Integer> it = headSet.descendingIterator();
            product = it.next();
            product *= it.next();
            product *= it.next();
        }
        return product;
    }
    
    public static void main(String[] args) {
        int[] nums = {6, 7, 8, 1, 2, 3, 9, 10 };
        Max3ProductAsc service = new Max3ProductAsc();
        int[] result = service.max3Product(nums);
        if (result == null) System.out.println("NULL");
        System.out.println(result[0] + " " + result[1] + " " + result[2]);
    }
}