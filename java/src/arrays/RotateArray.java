package arrays;

// http://www.geeksforgeeks.org/array-rotation/
public class RotateArray {
	
	private int gcd(int a, int b) {
		if (b == 0) return a;
		else return gcd(b, a%b);
	}
	
	// spits out which indexed number should be copied to startIndex
	private int nextIndex(int startIndex, int L, int d) {
		if ((startIndex + d) < L) return (startIndex + d);
		else return (startIndex + d - L);
	}
	
	public void rotate(int[] nums, int d) {
		int L = nums.length; // Length of array
		if (d > L) d = d % L;
		int N = gcd(L, d);   // Number of different sets
		
		for (int i = 0; i < N; i++) {
			int wIndex = i;
			int rIndex = nextIndex(i, L, d);
			
			int temp = nums[wIndex];
			while(rIndex != i) {
				nums[wIndex] = nums[rIndex];
				wIndex = rIndex;
				rIndex = nextIndex(rIndex, L, d);
			}
			nums[wIndex] = temp;
		}
	}
}
