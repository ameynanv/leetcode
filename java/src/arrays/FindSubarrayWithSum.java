package arrays;
/**
 * http://www.geeksforgeeks.org/find-subarray-with-given-sum/
 *
 */
public class FindSubarrayWithSum {
	
	public void findSubarrayWithSum(int[] nums, int target) {
		int sum = 0;
		int i = 0;
		int start = 0;
		
		while (true) {
			while (i < nums.length && sum < target) {
				sum += nums[i];
				i++;
			}
			if (target == sum) {
				System.out.print(start + " to " + (i-1));
				return;
			}
			else if (i >= nums.length) {
				System.out.println("Not found");
				return;
			}
			else {
				sum -= nums[start];
				start++;
			}
		}		
	}
	
	public static void main(String[] args) {
		int[] nums = {1, 4, 0, 0, 3, 10, 5};
		FindSubarrayWithSum service = new FindSubarrayWithSum();
		service.findSubarrayWithSum(nums, 7);
	}
}
