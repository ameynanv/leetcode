package arrays;

public class SingleNumberII {

	//
	// Idea is simple: consider Nth bit of every number of the sequence,
	// count number of times we got 1 on Nth bit in the sequence.
	// If it is multiple of 3 then solution has Nth bit  = 0 else 1
	//
	// Recall modulo 3 counter ==> 0, 1, 2, 0, 1, 2, 0
	// 1) Build a modulo 3 counter and increment it every time we get 1 in Nth bit
	// 2) If at the end of the sequence we have counter value == 0 then Nth bit of the solution is 0 else 1
	//
	// How to do this efficiently? 
	// 1) To build a modulo 3 counter for single bit, we need to store 3 states.: 00=>01=>10=>00 ...
	// 2) i.e. we require 2 bits of memory for every single bit counter.
	// 3) Let's say we store first bit of the counter in Bit0 and second bit of the counter in Bit1;
	// 4) Let initial values be Bit0 = 0, Bit1 = 0;
	// 5) Let Bit0, Bit1 and I be the input to a state machine and Bit0_ and Bit1_ be the output
	// 
	// B1   B0   I   |  B1_  B0_
	// 0    0    0   |  0    0
	// 0    0    1   |  0    1
	// 0    1    0   |  0    1
	// 0    1    1   |  1    0
	// 1    0    0   |  1    0
	// 1    0    1   |  0    0
	//
	// Build a Kmap and ans is : B1_ = (B1 & B0 & ~I ) | (~B1 & B0 & I) and B0_ = (~B1 & B0 & ~I) | (~B1 & ~B0 & I)
	// to go to next step B1 = B1_ and B0 = B0_
	//
	public int singleNumber(int[] nums) {
		int Bit0 = 0;
		int Bit1 = 0;
		
		for (int i = 0; i < nums.length; i++) {
			int Bit0_ = (~Bit1 & Bit0 & ~nums[i]) | (~Bit1 & ~Bit0 & nums[i]);
			int Bit1_ = (Bit1 & ~Bit0 & ~nums[i]) | (~Bit1 & Bit0 & nums[i]);
			Bit0 = Bit0_;
			Bit1 = Bit1_;
		}
		return Bit0 | Bit1;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 1,2,2,2};
		SingleNumberII service = new SingleNumberII();
		System.out.println(service.singleNumber(nums));
	}
}
