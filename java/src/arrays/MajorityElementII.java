package arrays;
import java.util.ArrayList;
import java.util.List;

public class MajorityElementII {
	public List<Integer> majorityElement(int[] nums) {
		List<Integer> list = new ArrayList<Integer>();
		int number1 = 0; int counter1 = -1;	int number2 = 0; int counter2 = -1;

		if (nums.length == 0)
			return list;

		for (int i = 0; i < nums.length; i++) {
			if (counter1 >= 0 && number1 == nums[i]) {
				counter1++;
			} else if (counter2 >= 0 && number2 == nums[i]) {
				counter2++;
			} else if (counter1 <= 0) {
				number1 = nums[i];
				counter1 = 1;
			} else if (counter2 <= 0) {
				number2 = nums[i];
				counter2 = 1;
			} else {
				counter1--;
				counter2--;
			}
		}

		if (counter2 == -1) {
			list.add(nums[0]);
			return list;
		}

		counter1 = 0; counter2 = 0;
		for (int i = 0; i < nums.length; i++) {
			if (number1 == nums[i]) 
				counter1++;
			if (number2 == nums[i]) 
				counter2++;
		}
		
		if(counter1 > nums.length/3) list.add(number1);
		if(counter2 > nums.length/3) list.add(number2);


		return list;
	}
}
