package arrays;
public class MaxSubArraySum {
	public int maxSubArray(int[] nums) {
		// Handle Edge Cases:
		if (nums.length == 0)
			return 0;

		int currentSum = nums[0];
		int maxSum = currentSum;

		for (int i = 1; i < nums.length; i++) {
			if (currentSum < 0) {
				currentSum = nums[i];
			} else {
				currentSum = currentSum + nums[i];
			}

			if (currentSum > maxSum) {
				maxSum = currentSum;
			}
		}

		return maxSum;
	}
}
