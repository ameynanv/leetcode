package arrays;
import java.util.Arrays;

public class MaximumGap {
    public int maximumGap(int[] nums) {
        int[] sorted = Arrays.copyOf(nums, nums.length);
        Arrays.sort(sorted);
        int maxGap = 0;
        for (int i = 0; i < sorted.length-1; i++) {
        	int gap = sorted[i+1] - sorted[i];
        	if (gap > maxGap)
        		maxGap = gap;
        }
        return maxGap;
    }
    
    public static void main(String[] args) {
    	int[] nums = {Integer.MAX_VALUE-1, Integer.MAX_VALUE-3, 23, 87, 100 , 0};
    	MaximumGap service = new MaximumGap();
    	System.out.println(service.maximumGap(nums));
    }
}
