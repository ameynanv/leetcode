package arrays;

// https://leetcode.com/problems/jump-game-ii/

public class JumpGameII {
    public int jump(int[] nums) {
        //Edge Cases:
        if (nums.length == 0) return 0;
        if (nums.length == 1) return 0;
        if (nums[0] >= nums.length -1) return 1;
        
        int i = 0;
        int nextIndex = i;
        int maxReachableIndex = i;
        int count = 0;        
        while (true) {
            maxReachableIndex = i;
            for (int j = 1; j <= nums[i]; j++) {
                if (i + nums[i] >= nums.length - 1) return count + 1;
                if (maxReachableIndex < i + j + nums[i + j]) {
                    maxReachableIndex = i + j + nums[i + j];
                    nextIndex = i + j;
                }
            }
            if (nextIndex == i) return Integer.MAX_VALUE;
            i = nextIndex;
            count++;
        }
    }
    
    public static void main(String[] args) {
        int[] nums = {2,3,1,1,4};
        JumpGameII service = new JumpGameII();
        System.out.println(service.jump(nums));
    }
    
}