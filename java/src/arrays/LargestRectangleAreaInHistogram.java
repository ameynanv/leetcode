package arrays;

import java.util.Stack;

/**
 * https://leetcode.com/problems/largest-rectangle-in-histogram/
 * http://www.geeksforgeeks.org/largest-rectangle-under-histogram/
 *
 */
public class LargestRectangleAreaInHistogram {
	
    public int largestRectangleArea(int[] height) {
        if (height.length == 0) return 0;
		Stack<Integer> valueStack = new Stack<Integer>();
		Stack<Integer> indexStack = new Stack<Integer>();
		
		int maxArea = Integer.MIN_VALUE;
		int i;
		for (i = 0; i < height.length; i++) {
			if (valueStack.isEmpty()) {
				valueStack.push(height[i]);
				indexStack.push(i);
			} else if (valueStack.peek() <= height[i]){
				valueStack.push(height[i]);
				indexStack.push(i);				
			} else {
				int valueTop;
				while (!valueStack.isEmpty() && valueStack.peek() > height[i]) {
					valueTop = valueStack.pop();
					indexStack.pop();
					int area;
					if (valueStack.isEmpty()) {
						area = valueTop * (i);
					} else {
						area = valueTop * (i - indexStack.peek() - 1);	
					}
					if (area > maxArea) {
						maxArea = area;
					}
				}
				valueStack.push(height[i]);
				indexStack.push(i);				
			}
		}
		
		while (!valueStack.isEmpty()) {
			int valueTop = valueStack.pop();
			indexStack.pop();
			int area;
			if (valueStack.isEmpty()) {
				area = valueTop * (i);
			} else {
				area = valueTop * (i - indexStack.peek() - 1);	
			}
			if (area > maxArea) {
				maxArea = area;
			}
		}
		
		return maxArea;
    }
	
	public static void main(String[] args) {
		int[] height = {6,2,5,4,5,1,6};
		LargestRectangleAreaInHistogram service = new LargestRectangleAreaInHistogram();
		System.out.println(service.largestRectangleArea(height));
	}
}

