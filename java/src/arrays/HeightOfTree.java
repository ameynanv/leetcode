package arrays;

/*
http://www.careercup.com/question?id=22809662

A tree, (NOT NECESSARILY BINARY), has nodes numbered 0 to N-1. An array has indices ranging from 0 to N-1. 
The indices denote the node ids and values denote the ids of parents. A value of -1 at some index k
denotes that node with id k is the root. For ex:
3 3 3 -1 2
0 1 2 3 4
In the above, nodes with ids 0, 1 & 2 have 3 as parent. 3 is the root as its parent = -1 and 2 is 
the parent of node id 4. 
Given such an array, find the height of the tree.

*/
public class HeightOfTree {
    
    public int heightOfTree(int[] nums) {
        int[] F = new int[nums.length];
        for (int i = 0; i < nums.length; i++) 
            F[i] = -1;
        
        int maxHeight = Integer.MIN_VALUE;
        for (int i = 0; i < nums.length; i++) {
            int ht = findHeight(i, F, nums);
            maxHeight = Math.max(maxHeight, ht);
        }
        return maxHeight;
    }
    
    private int findHeight(int index, int[] F, int[] nums) {
        if (F[index] != -1) return F[index];
        if (nums[index] == -1) return 0;
        F[index] = 1 + findHeight(nums[index], F, nums);
        return F[index];
    }
    
    public static void main(String[] args) {
        int[] nums = {3,3,3,-1,2};
        HeightOfTree service = new HeightOfTree();
        int result = service.heightOfTree(nums);
        System.out.println(result);
    }
}