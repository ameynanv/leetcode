package arrays;
/**
 * http://www.careercup.com/question?id=4909367207919616
 *
 * Modify the array such that arr[I] = arr[arr[I]] 
 * Do this in place i.e. with out using additional memory.
 * 
 * example : if a = {2,3,1,0}
 * o/p = a = {1,0,3,2}
 * 
 * Note : The array contains 0 to n-1 integers.
 */
public class RearrangeArray {

	/**
	 * for n > x && n > y
	 * 
	 * 1) x % n  = x;
	 * 2) y % n  = y;
	 * 3) (x + y*n) % n = x;
	 * 4) (x + y*n) / n = y;
	 * 
	 * i.e. (x + y*n) can store two numbers  x as well as y
	 * and they can be retrieved by eq 3 and eq 4
	 * 
	 * in this case x = nums[i];   and y = nums[nums[i]]
	 * 
	 */
	public void rearrange(int[] nums) {
		int n = nums.length;
		for (int i = 0; i < n; i++) {
			nums[i] = nums[i] + (nums[nums[i]] % n) * n;
		}
		for (int i = 0; i < n; i++) {
			nums[i] = nums[i] / n;
		}
	}
	
	public static void main(String[] args) {
		RearrangeArray service = new RearrangeArray();
		int[] nums = {2,3,1,0};
		service.rearrange(nums);
		for (int i = 0; i < nums.length; i++)
			System.out.print(nums[i] + " ");
		System.out.println("");
	}

}
