package arrays;

import java.util.HashSet;
// https://leetcode.com/problems/contains-duplicate-ii/
public class ContainsDuplicateII {
	public boolean containsNearbyDuplicate(int[] nums, int k) {
	    if (k <= 0) return false;
	    HashSet<Integer> set = new HashSet<Integer>();
	    for (int i = 0; i <= k ; i++) {
	        if (i < nums.length) {
	            if (set.contains(nums[i])) return true;
	            else set.add(nums[i]);
	        }
	    }
	    if (k >= nums.length) return false;
	    for (int i = 1; i < nums.length - k; i++) {
	        set.remove(nums[i-1]);
	        if (set.contains(nums[i+k])) return true;
	        else set.add(nums[i+k]);
	    }
	    return false;
	}
}
