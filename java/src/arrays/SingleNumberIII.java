package arrays;

public class SingleNumberIII {
	public int[] singleNumber(int[] nums) {
		if (nums.length < 2) return null;
        int XOR = 0;
        for (int i = 0; i < nums.length; i++)
            XOR = XOR ^ nums[i];
        int XORcopy = XOR;
        int bitNumber = 0;
        while((XORcopy & 0x1) == 0x00) {
            XORcopy = XORcopy >> 1;
            bitNumber++;
        }
        
        int number1 = 0;
        int number2 = 0;
        
        for (int i = 0; i < nums.length; i++) {
            int numCopy = nums[i];
            numCopy = numCopy >> bitNumber;
            if ((numCopy & 0x01) == 0x00) {
                number1 = number1 ^ nums[i];
            } else {
                number2 = number2 ^ nums[i];
            }
        }
        
        int[] result = new int[2];
        result[0] = number1;
        result[1] = number2;
        return result;
    }
	public static void main(String[] args) {
		SingleNumberIII service = new SingleNumberIII();
		int nums[] = {-1,2};
		int[] result = service.singleNumber(nums);
		System.out.println(result[0] + " " + result[1]);
	}

}
