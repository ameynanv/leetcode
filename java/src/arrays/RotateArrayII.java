package arrays;

// http://www.geeksforgeeks.org/array-rotation/
public class RotateArrayII {

	private void swap(int[] nums, int index1, int index2) {
		int temp = nums[index1];
		nums[index1] = nums[index2];
		nums[index2] = temp;
	}
	
	// reverses the the array from array[start] to array[end] both inclusive in
	// place
	private void reverse(int[] nums, int start, int end) {
		while (start < end) {
			swap(nums, start++, end--);
		}
	}

	public void rotate(int[] nums, int d) {
		int L = nums.length; // Length of array
		if (d > L)
			d = d % L;
		reverse(nums, 0, d - 1);
		reverse(nums, d, L - 1);
		reverse(nums, 0, L - 1);
	}
}
