package arrays;

public class MaxSubArraySumWrap {

    public int maxSubArraySumWrap(int[] nums) {
        if (nums.length == 0) return 0;
        int maxSum = maxSubArraySum(nums);
        int minSum = minSubArraySum(nums);
        int totalSum = arraySum(nums);
        return Math.max(maxSum, totalSum - minSum);
    }
    
    private int arraySum(int[] nums) {
        int total = 0;
        for (int i = 0; i < nums.length; i++) 
            total += nums[i];
        return total;
    }
    private int maxSubArraySum(int[] nums) {
        int maxSum = Integer.MIN_VALUE;
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            if (sum < 0) 
                sum = nums[i];
            else 
                sum += nums[i];
            
            if (sum > maxSum) {
                maxSum = sum;
            }
        }
        return maxSum;
    }
    
    private int minSubArraySum(int[] nums) {
        int minSum = Integer.MAX_VALUE;
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            if (sum > 0) 
                sum = nums[i];
            else 
                sum += nums[i];
            
            if (sum < minSum) {
                minSum = sum;
            }
        }
        return minSum;
    }

    public static void main(String[] args) {
        int[] nums = {11, 10, -20, 5, -3, -5, 8, -13, 10};
        MaxSubArraySumWrap service = new MaxSubArraySumWrap();
        int result = service.maxSubArraySumWrap(nums);
        System.out.println(result);
    }
}