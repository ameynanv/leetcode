package arrays;

// https://leetcode.com/problems/jump-game/

public class JumpGame {
    public boolean canJump(int[] nums) {
        if (nums.length == 0) return false;
        if (nums.length == 1) return true;
        if (nums[0] >= nums.length - 1) return true;
        
        int i = 0;
        int nextIndex = i;
        int maxReachableIndex = i;
        while (true) {
            maxReachableIndex = i;
            for (int j = 1; j <= nums[i]; j++) {
                if (i + nums[i] >= nums.length - 1) return true;
                if (maxReachableIndex < i + j + nums[i + j]) {
                    maxReachableIndex = i + j + nums[i + j];
                    nextIndex = i + j;
                }
            }
            if (nextIndex == i) return false;
            i = nextIndex;
        }
    }
    
    public static void main(String[] args) {
        int[] nums = {3,2,1,0,4};
        JumpGame service = new JumpGame();
        System.out.println(service.canJump(nums));
    }
}