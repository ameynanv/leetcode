package arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class CombinationSum {
	private class Combination {
		List<Integer> list;
		HashMap<Integer, Integer> histogram = new HashMap<Integer, Integer>();

		public Combination(int[] candidates, List<Integer> list) {
			for (int i = 0; i < candidates.length; i++) {
				int key = candidates[i];
				histogram.put(key, 1);
			}
			this.list = list;
			for (int i = 0; i < list.size(); i++) {
				int key = list.get(i);
				histogram.put(key, histogram.get(key) + 1);
			}

		}

		@Override
		public boolean equals(Object other) {
			if (other == this)
				return true;
			if (other == null)
				return false;
			if (other.getClass() != this.getClass())
				return false;
			Combination that = (Combination) other;
			return that.hashCode() == this.hashCode();
		}

		@Override
		public int hashCode() {
			int hashCode = 0;
			for (Integer key : histogram.keySet()) {
				hashCode = 31 * hashCode + histogram.get(key);
			}
			return hashCode;
		}
	}

	public List<List<Integer>> combinationSum(int[] candidates, int target) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		HashSet<Combination> set = new HashSet<Combination>();

		if (target < 0)
			return null;
		if (target == 0) {
			result.add(new ArrayList<Integer>());
			return result;
		}
		Arrays.sort(candidates);

		for (int i = 0; i < candidates.length; i++) {
			int num = candidates[i];
			List<List<Integer>> temp = combinationSum(candidates, target - num);
			if (temp != null) {
				for (int r = 0; r < temp.size(); r++) {
					List<Integer> sol = temp.get(r);
					sol.add(num);
					Combination comb = new Combination(candidates, sol);
					if (!set.contains(comb))
						set.add(comb);
				}
			}
		}
		Iterator<Combination> it = set.iterator();
		while (it.hasNext()) {
			List<Integer> list= it.next().list;
			Collections.sort(list);
			result.add(list);
		}
		return result;
	}

	public static void main(String[] args) {
		CombinationSum service = new CombinationSum();
		int[] nums = { 92,71,89,74,102,91,70,119,86,116,114,106,80,81,115,99,117,93,76,77,111,110,75,104,95,112,94,73};
		System.out.println(service.combinationSum(nums, 310));
	}
}
