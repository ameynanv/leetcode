package arrays;

import java.util.HashMap;
/**
 * https://leetcode.com/problems/two-sum/
 * 
 * Given an array of integers, find two numbers such that they add up to a specific target number.
 */
public class TwoSum {
	public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        if (nums.length <= 2) return result;
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], i);
        }
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target - nums[i]) && map.get(target - nums[i]) != i) {
                result[0] = i+1;
                result[1] = map.get(target - nums[i])+1;
                return result;
            }
        }
        return result;
    }
}
