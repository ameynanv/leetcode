package arrays;

//
// http://www.geeksforgeeks.org/median-of-two-sorted-arrays/
//
public class MedianOfSortedArrays {
	public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int L = (nums1.length + nums2.length);
        if (L%2  == 0) {
            int small = findKthSmallestNumberInMerge(nums1, nums2, L/2-1);
            int big = findKthSmallestNumberInMerge(nums1, nums2, L/2);
            return (small+big)/2.0;
        } else {
            return findKthSmallestNumberInMerge(nums1, nums2, L/2) * 1.0;
        }
    }
    
    public int findKthSmallestNumberInMerge(int[] nums1, int[] nums2, int k) {
    	int ans = searchKthSmallestNumberIndexInArray(nums1, nums2, k);
    	if (ans == -1) return nums2[searchKthSmallestNumberIndexInArray(nums2, nums1, k)];
    	return nums1[ans];
    }	
    
    public int searchKthSmallestNumberIndexInArray(int[] nums1, int[] nums2, int k) {
    	if (nums1.length == 0) return -1;
    	int start = 0; int end = nums1.length - 1;
    	while(start < end) {
    		int mid = (start + end) >> 1;
    		int n_floor, n_ceil;

    		n_floor = mid + findNumberOfSmallerValues(nums2, nums1[mid]);
    		n_ceil  = mid + 1 + findNumberOfSmallerOrEqualValues(nums2, nums1[mid]);
    		if (n_floor <= k && k < n_ceil) return mid;
    		if (k >= n_ceil) {
    			start = mid+1;
    		} else {
    			end = mid;
    		}
    	}
    	
		int n_floor = end + findNumberOfSmallerValues(nums2, nums1[end]);
		int n_ceil  = end + 1 + findNumberOfSmallerOrEqualValues(nums2, nums1[end]);
		if (n_floor <= k && k < n_ceil) return end;
    	
    	return -1;
    }
	
    public int findNumberOfSmallerValues(int[] nums, int target) {
    	if (nums.length == 0) return 0;
    	int start = 0;
    	int end = nums.length - 1;
    	while(start < end) {
    		int mid = (start + end) >> 1;
    		if (nums[mid] >= target) {
    			end = mid;
    		} else {
    			start = mid + 1;
    		}
    	}
    	if (nums[start] < target) return start + 1;
    	return start;
    }
    
    public int findNumberOfSmallerOrEqualValues(int[] nums, int target) {
    	if (nums.length == 0) return 0;
    	int start = 0;
    	int end = nums.length - 1;
    	while(start < end) {
    		int mid = (start + end) >> 1;
    		if (nums[mid] > target) {
    			end = mid;
    		} else {
    			start = mid + 1;
    		}
    	}
    	if (nums[start] <= target) return start + 1;
    	return start;
    }	
	
	public static void main(String[] args) {

		MedianOfSortedArrays service = new MedianOfSortedArrays();
		int[] nums1 = {1,2};
		int[] nums2 = {1,2};
		System.out.println(service.findMedianSortedArrays(nums1, nums2));
		
		int[] nums3 = {1,2};
		int[] nums4 = {};
		System.out.println(service.findMedianSortedArrays(nums3, nums4));
		
		int[] nums5 = {1,2};
		int[] nums6 = {1,1};
		System.out.println(service.findMedianSortedArrays(nums5, nums6));
		
		int[] nums7 = {2};
		int[] nums8 = {1};
		System.out.println(service.findMedianSortedArrays(nums7, nums8));

		int[] nums9  = {};
		int[] nums10 = {1};
		System.out.println(service.findMedianSortedArrays(nums9, nums10));

		int[] nums11  = {1};
		int[] nums12 = {};
		System.out.println(service.findMedianSortedArrays(nums11, nums12));
	}
}
