package arrays;

public class CountNumberOfOccurrencesInSortedArray {
	
	// returns the index of a min number greater than x
	// returns -1 if Ceiling is present on left side of array
	// returns nums.length if Ceiling is present on right side of array
	public int findCeilingIndex(int[] nums, int x) {
		return findCeilingIndex(nums, 0, nums.length - 1, x);
	}

	private int findCeilingIndex(int[] nums, int start, int end, int x) {
		if (start == end) {
			if (nums[start] == x) return nums.length;
			else return -1;
		}
		int mid = (start + end) >> 1;
		if (nums[mid] <= x && nums[mid + 1] > x) return mid + 1;
		else if (nums[mid] > x) return findCeilingIndex(nums, start, mid, x);
		else return findCeilingIndex(nums, mid + 1, end, x);
	}
	
	// returns the index of a max number smaller than x
	// returns -1 if Floor is present on left side of array
	// returns nums.length if Floor is present on right side of array
	public int findFloorIndex(int[] nums, int x) {
		return findFloorIndex(nums, 0, nums.length - 1, x);
	}

	private int findFloorIndex(int[] nums, int start, int end, int x) {
		if (start == end) {
			if (nums[start] == x) return -1;
			else return nums.length;
		}
		int mid = (start + end) >> 1;
		if (nums[mid] < x && nums[mid + 1] >= x) return mid;
		else if (nums[mid] >= x) return findFloorIndex(nums, start, mid, x);
		else return findFloorIndex(nums, mid + 1, end, x);
	}
	

	public int countOccurrences(int[] nums, int x) {
		int startIndex = findFloorIndex(nums, x);
		if (startIndex == nums.length) return 0;
		int endIndex = findCeilingIndex(nums, x);
		if (endIndex == -1) return 0;
		startIndex++;
		endIndex--;
		return (endIndex - startIndex+1);
	}
}
