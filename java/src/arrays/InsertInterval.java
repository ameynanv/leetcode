package arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class InsertInterval {

	public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
		if (intervals.isEmpty()) {
			intervals.add(newInterval);
			return intervals;
		}
		ListIterator<Interval> it = intervals.listIterator();
		int start = newInterval.start;
		int end = newInterval.end;
		while (it.hasNext()) {
			Interval currentIv = it.next();
			if (newInterval.start <= currentIv.end) {
				if (currentIv.start <= newInterval.end) {
					it.remove();
					start = Math.min(start, currentIv.start);
					end = Math.max(end, currentIv.end);
				} 
			}
		}
		
		int i = 0;
		for (Interval iv: intervals) {
			if(iv.end < start) i++;
		}
		intervals.add(i, new Interval(start, end));
		return intervals;
	}

	public static void main(String[] args) {
		InsertInterval service = new InsertInterval();
		List<Interval> intervals = new ArrayList<Interval>();
		intervals.add(new Interval(1,5));
		Interval newInterval = new Interval(0,0);
		List<Interval> result = service.insert(intervals, newInterval);
		System.out.println(result);
	}

}

class Interval {
	int start;
	int end;

	Interval() {
		start = 0;
		end = 0;
	}

	Interval(int s, int e) {
		start = s;
		end = e;
	}

	public String toString() {
		return "[" + start + "-" + end + "]";
	}
}