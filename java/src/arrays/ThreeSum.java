package arrays;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

/**
 * Given an array S of n integers, are there elements a, b, c in S such that 
 * a + b + c = 0 
 * Find all unique triplets in the array which gives the sum of zero.
 * Note: Elements in a triplet (a,b,c) must be in non-descending order. 
 * (ie, a <= b <= c) 
 * The solution set must not contain duplicate triplets. For example,
 * given array S = {-1 0 1 2 -1 -4}, 
 * A solution set is: (-1, 0, 1) (-1, -1, 2)
 *
 */
public class ThreeSum {
	public List<List<Integer>> threeSum(int[] nums) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		Arrays.sort(nums);
		for (int i = 0; i < nums.length - 1; i++) {
			if (i != 0 && nums[i] == nums[i - 1])
				continue;
			int left = i + 1;
			int right = nums.length - 1;
			while (left < right) {
				if (nums[left] + nums[right] > -1 * nums[i]) {
					right--;
				} else if (nums[left] + nums[right] < -1 * nums[i]) {
					left++;
				} else {
					List<Integer> list = new ArrayList<Integer>();
					list.add(nums[i]);
					list.add(nums[left]);
					list.add(nums[right]);
					result.add(list);
					left++;
					while (left < nums.length && nums[left] == nums[left - 1])
						left++;
					right--;
					while (right > 0 && nums[right] == nums[right + 1])
						right--;
				}
			}
		}
		return result;
	}

	public static void main(String[] args) {
		int[] nums = { -2, 0, 0, 2, 2 };
		ThreeSum service = new ThreeSum();
		System.out.println(service.threeSum(nums));
	}
}