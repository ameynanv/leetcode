package arrays;

import java.util.TreeSet;

// https://leetcode.com/problems/contains-duplicate-iii/
public class ContainsDuplicateIII {
    public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        if (k <= 0) return false;
        TreeSet<Integer> set = new TreeSet<Integer>();
        for (int i = 0; i <= k && i < nums.length; i++) {
            Integer max = set.ceiling(nums[i]);
            Integer min = set.floor(nums[i]);
            if (max != null && (max - nums[i]) <= t &&  (max - nums[i]) >= 0) return true;
            if (min != null && (nums[i] - min) <= t &&  (nums[i] - min) >= 0) return true;
            set.add(nums[i]);
        }
        if (nums.length < k) return false;
        for (int i = 1; i < nums.length - k; i++) {
            set.remove(nums[i - 1]);
            Integer max = set.ceiling(nums[i + k]);
            Integer min = set.floor(nums[i + k]);
            if (max != null && (max - nums[i + k]) <= t &&  (max - nums[i + k]) >= 0) return true;
            if (min != null && (nums[i + k] - min) <= t &&  (nums[i + k] - min) >= 0) return true;
            set.add(nums[i + k]);
        }
        return false;
    }
}
