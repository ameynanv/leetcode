package arrays;

public class NumberOccurringOddNumberOfTimes {
	public int oddNumber(int[] nums) {
		int ans = 0;
		for (int i = 0; i < nums.length; i++) {
			ans = ans^nums[i];
		}
		return ans;
	}
}
