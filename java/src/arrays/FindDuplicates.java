package arrays;

// http://www.geeksforgeeks.org/find-duplicates-in-on-time-and-constant-extra-space/
public class FindDuplicates {
    
    public void findDuplicates(int[] nums) {
        int zeroDuplicate = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                zeroDuplicate++;
            }
            if (nums[Math.abs(nums[i])] < 0) {
                System.out.println(Math.abs(nums[i]));                    
            } else {
                nums[Math.abs(nums[i])] *= -1;    
            }
        }
        if (zeroDuplicate > 1) System.out.println(0);
    }
    
    public static void main(String[] args) {
        int[] nums = {0,0,4,4,2};
        FindDuplicates service = new FindDuplicates();
        service.findDuplicates(nums);
    }
}