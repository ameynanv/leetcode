package arrays;

import java.util.List;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.PriorityQueue;
import java.util.Comparator;

public class SkylineProblem {

	// The geometric information of each building is represented by a triplet of
	// integers [Li, Ri, Hi], where Li and Ri are the x coordinates of the left
	// and right edge of the ith building, respectively, and Hi is its height.
	public List<int[]> getSkyline(int[][] buildings) {

		TreeMap<Integer, Integer> hProfile = new TreeMap<Integer, Integer>();
		PriorityQueue<Event> minPQ = new PriorityQueue<Event>(1,
				new EventComparator());
		PriorityQueue<Integer> maxPQ = new PriorityQueue<Integer>(1,
				new MaxPQIntegerComparator());
		
		for (int i = 0; i < buildings.length; i++) {
			minPQ.add(new Event(buildings[i][0], true, buildings[i][2]));
			minPQ.add(new Event(buildings[i][1], false, buildings[i][2]));
		}
		hProfile.put(0,0);
		
		while (!minPQ.isEmpty()) {
			Event e = minPQ.remove();
			if (e.isStartEvent) {
				maxPQ.add(e.height);
			} else {
				maxPQ.remove(e.height);
			}
			hProfile.put(e.x, maxPQ.peek() == null ? 0 : maxPQ.peek());
		}
		System.out.println(hProfile);
		List<int[]> result = new ArrayList<int[]>();
		int prevHeight = 0;
		for (Integer x: hProfile.keySet()) {
			if (hProfile.get(x) != prevHeight) {
				prevHeight = hProfile.get(x);
				int[] arr = new int[2];
				arr[0] = x; arr[1] = prevHeight;
				result.add(arr);
			}
		}
		return result;
	}

	private class EventComparator implements Comparator<Event> {
		public int compare(Event o1, Event o2) {
			if (o1.x > o2.x)
				return +1;
			if (o1.x < o2.x)
				return -1;
			else
				return Integer.compare(o1.height, o2.height);
		}
	}

	private class MaxPQIntegerComparator implements Comparator<Integer> {
		public int compare(Integer o1, Integer o2) {
			return -1 * Integer.compare(o1, o2);
		}
	}

	private class Event {
		int x;
		boolean isStartEvent;
		int height;

		Event(int x, boolean isStartEvent, int height) {
			this.x = x;
			this.isStartEvent = isStartEvent;
			this.height = height;
		}
	}

	public static void main(String[] args) {
		int[][] buildings = {{1,5,3}, {2,4,5},{3,6,4},{7,9,3},{8,10,2}};
		//int[][] buildings = {{1,5,2}, {1,5,4}};
		SkylineProblem service = new SkylineProblem();
		List<int[]> result = service.getSkyline(buildings);
		for (int[] arr: result) {
			System.out.print(arr[0] +"=>" + arr[1]);
		}
		
	}
}
