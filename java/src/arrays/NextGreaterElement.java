package arrays;

import java.util.Stack;

public class NextGreaterElement {
	public void nextGreaterElement(int[] nums) {
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(nums[0]);
		for (int i = 1; i < nums.length; i++) {
			if (nums[i] > stack.peek()) {
				System.out.println(stack.pop() + "=> " + nums[i]);
				while (!stack.isEmpty()) {
					if (nums[i] > stack.peek()) {
						System.out.println(stack.pop() + "=> " + nums[i]);		
					} else {
						break;
					}
				}
			} else {
				stack.push(nums[i]);
			}
		}
		System.out.println(nums[nums.length - 1] + "=> -1");
		while(!stack.isEmpty()) {
			System.out.println(stack.pop() + "=> -1");
		}
	}
}
