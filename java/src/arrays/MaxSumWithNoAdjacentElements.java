package arrays;

//http://www.geeksforgeeks.org/maximum-sum-such-that-no-two-elements-are-adjacent/
public class MaxSumWithNoAdjacentElements {
	
	public int maxSumWithNoAdj(int[] nums) {
		if (nums.length == 0) return 0;
		if (nums.length == 1) return nums[0];
		
		int Result_I = 0;
		int Result_I_1 = nums[1] > nums[0] ? nums[1] : nums[0];
		int Result_I_2 = nums[0];
		
		for (int i = 2; i < nums.length; i++) {
			Result_I = maxSum(nums, i, Result_I_1, Result_I_2);
			Result_I_2 = Result_I_1;
			Result_I_1 = Result_I;
		}
		return Result_I;
	}
	
	private int maxSum(int[] nums, int i, int Result_I_1, int Result_I_2) {
		int sumIncl = nums[i] + Result_I_2;
		int sumExcl = Result_I_1;
		if (sumIncl > sumExcl) {
			return sumIncl;
		}
		else { 
			return sumExcl;
		}
	}
}
