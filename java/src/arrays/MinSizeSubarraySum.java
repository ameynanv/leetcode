package arrays;

public class MinSizeSubarraySum {
    public int minSubArrayLen(int s, int[] nums) {
        if (nums.length == 0) return 0;
        int left = 0;
        int right = left;
        int minLength = Integer.MAX_VALUE;
        int sum = 0;
        while (right < nums.length) {
            sum += nums[right++];            
            while (sum < s && right < nums.length) {
                sum += nums[right++]; 
            }
            while (sum >= s) {
                minLength = Math.min(minLength, right-left);
                sum -= nums[left];
                left++;
            }
        }
        return (minLength < nums.length) ? minLength : 0;
    }
    
	public static void main(String[] args) {
		MinSizeSubarraySum service = new MinSizeSubarraySum();
		int[] nums = {2,3,1,2,4,3};
		int target = 7;
		System.out.println(service.minSubArrayLen(target, nums));

	}

}
