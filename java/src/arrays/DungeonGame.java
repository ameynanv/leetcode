package arrays;

public class DungeonGame {

	public int calculateMinimumHP(int[][] dungeon) {
		int H = dungeon.length;
		if (H == 0) return 0;
		int W = dungeon[0].length;
		if (W == 0) return 0;
		
		int[][] Health = new int[H][W];
		for (int i = H - 1; i >= 0; i--) {
			for (int j = W - 1; j >= 0; j--) {
				if (i == (H-1) && j == (W-1)) {
					Health[i][j] = 1;
				} else if (i == (H-1)) {
					Health[i][j] = (Health[i][j+1] - dungeon[i][j+1]) < 1 ? 
							  1 :
							  Health[i][j+1] - dungeon[i][j+1];
				} else if (j == (W-1)) {
					Health[i][j] = (Health[i+1][j] - dungeon[i+1][j]) < 1 ? 
							  1 :
							  Health[i+1][j] - dungeon[i+1][j];
				} else {
					int rightStepCost = (Health[i][j+1] - dungeon[i][j+1]) < 1 ? 
							  				1 :
					  						Health[i][j+1] - dungeon[i][j+1];
					int downStepCost = (Health[i+1][j] - dungeon[i+1][j]) < 1 ? 
							  				1 :
						  					Health[i+1][j] - dungeon[i+1][j];
					Health[i][j] = Math.min(rightStepCost, downStepCost);
				}
					
			}
		}
		return (Health[0][0] - dungeon[0][0]) < 1 ? 
  					1 :
					Health[0][0] - dungeon[0][0];
    }

	public static void main(String[] args) {
		int[][] puzzle = {{1,-3,3},{0,-2,0},{-3,-3,-3}};
		//int[][] puzzle = {{-2,-3,3},{-5,-10,1},{10,30,-5}};
		//int[][] puzzle = {{-10}};
		//int[][] puzzle = {{-1, 1}};
		DungeonGame service = new DungeonGame();
		System.out.println(service.calculateMinimumHP(puzzle));
		
	}
}
