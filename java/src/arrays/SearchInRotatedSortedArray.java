package arrays;

/**
 * 
 * https://leetcode.com/problems/search-in-rotated-sorted-array/
 *
 */
public class SearchInRotatedSortedArray {
    public int search(int[] nums, int target) {
        if (nums.length < 1) return -1;
        return search(nums, 0, nums.length - 1, target);
    }
    
    private int search(int[] nums, int start, int end, int target) {
        if (start > end) return -1;
        int mid = (start + end) / 2;
        if (nums[mid] == target) return mid;
        if (nums[start] == target) return start;
        if (nums[end] == target) return end;
        if (target < nums[mid]) {
        	if (target > nums[start]) {
        		return search(nums, start, mid-1, target);
        	} else {
        		if (nums[mid] < nums[end])
        			return search(nums, start, mid-1, target);
        		else
        			return search(nums, mid + 1, end, target);	
        	}
        } else {
        	if (target < nums[end]) {
        		return search(nums, mid + 1, end, target);
        	} else {
        		if (nums[mid] < nums[end])
        			return search(nums, start, mid-1, target);
        		else
        			return search(nums, mid + 1, end, target);	
        	}
        }
    }
    
    public static void main(String[] args) {
    	//int[] nums = {4,5,6,7,0,1,2};
    	int[] nums = {4,5,6,7,8,1,2,3};
    			
    	SearchInRotatedSortedArray service = new SearchInRotatedSortedArray();
    	System.out.println(service.search(nums, 8));

    }
}
