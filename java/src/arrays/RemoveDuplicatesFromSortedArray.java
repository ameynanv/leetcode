package arrays;

public class RemoveDuplicatesFromSortedArray {
    public int removeDuplicates(int[] nums) {
        if (nums.length <= 1) return nums.length;
        int read = 1;
        int write = 1;
        while(read < nums.length) {
            if (nums[read] == nums[write-1]) read++;
            else nums[write++] = nums[read++];
        }
        return write;
    }
}
