package arrays;

import java.util.ArrayDeque;

public class SlidingWindowMaximum {
    
    public int[] maxSlidingWindow(int[] nums, int k) {
        if (nums.length == 0) return new int[0];
        int resultLength = (nums.length - k + 1) > 0 ? (nums.length - k + 1) : 1;
        int[] result = new int[resultLength];
        ArrayDeque<Integer> deque = new ArrayDeque<Integer>();
        for (int i = 0; i < k && i < nums.length; i++) {
            removeStaleElementsFromFront(i, k, deque);
            removeLesserElementsFromBack(i, deque, nums);
            deque.addLast(i);
        }
        result[0] = nums[deque.peek()];
        for (int j = 1; (j + k - 1) < nums.length; j++) {
            removeStaleElementsFromFront(j+k-1, k, deque);
            removeLesserElementsFromBack(j+k-1, deque, nums);
            deque.addLast(j+k-1);
            result[j] = nums[deque.peek()];
        }
        return result;
    }
    
    private void removeStaleElementsFromFront(int index, int k, ArrayDeque<Integer> deque) {
        while (!deque.isEmpty() && deque.peek() < (index - k + 1)) {
            deque.remove();
        }
    }
    
    private void removeLesserElementsFromBack(int index, ArrayDeque<Integer> deque, int[] nums) {
        while (!deque.isEmpty() && nums[deque.peekLast()] < nums[index]) {
            deque.removeLast();
        }
    }
    
    public static void main(String[] args) {
        int[] nums = {1,3,-1,-3,5,3,6,7};
        //int[] nums = {1,3};
        //int[] nums = {7,2,4};
        SlidingWindowMaximum service = new SlidingWindowMaximum();
        int[] max = service.maxSlidingWindow(nums, 3);
        for (int i = 0; i < max.length; i++)
            System.out.print(max[i] + " ");
        System.out.println("");
    }
}