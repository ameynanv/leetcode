package arrays;
import java.util.List;
import java.util.ArrayList;

public class TugOfWar {
	
	public List<List<Integer>> tugOfWar(int[] nums) {
		int sum = 0;
		for (int i = 0; i < nums.length; i++)
			sum += nums[i];
		
		// Iterate over all the combinations and find the sum closest of sum/2;
		List<List<Integer>> combinations = combinations(nums.length, nums.length/2);
		double minAbsDiff = Double.MAX_VALUE;
		List<Integer> minCombination = new ArrayList<Integer>();
		
		for (List<Integer> list: combinations) {
			int currSum = 0;
			for (int i: list) 
				currSum += nums[i];
			double absDiff = Math.abs(currSum - sum/2.0);
			if (absDiff < minAbsDiff) {
				minCombination = list;
				minAbsDiff = absDiff;
			}
		}
		
		
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		List<Integer> firstList = new ArrayList<Integer>();
		List<Integer> secondList = new ArrayList<Integer>();
		for (int i = 0; i < minCombination.size(); i++) {
			int index = minCombination.get(i);
			firstList.add(nums[index]);
		}
		for (int i = 0; i < nums.length; i++) {
			if (!minCombination.contains(i)) 
				secondList.add(nums[i]);
		}
		result.add(firstList);
		result.add(secondList);
		return result;
	}
	
	
	// spits out all the combinations of (0 - n-1) numbers and k slots
	private List<List<Integer>> combinations(int n, int k) {
		if (k == 0 || n == k) {
			List<List<Integer>> result = new ArrayList<List<Integer>>();
			List<Integer> list = new ArrayList<Integer>();
			for (int i = 0; i < k ; i++) {
				list.add(i);
			}
			result.add(list);
			return result;
		} else {
			// C(n,k) = C(n-1,k-1) + C(n-1,k)
			List<List<Integer>> result = combinations(n-1,k-1);
			for(List<Integer> list: result)
				list.add(n-1);
			result.addAll(combinations(n-1,k));
			return result;
		}
	}
	
	public static void main(String[] args) {
		TugOfWar service = new TugOfWar();
		int[] nums = {23, 45, -34, 12, 0, 98, -99, 4, 189, -1, 4};
		List<List<Integer>> result = service.tugOfWar(nums);
		System.out.println(result.get(0));
		System.out.println(result.get(1));
		
	}
}
