package arrays;

import java.util.Arrays;

public class ThreeSumClosest {

    private int twoSumClosest(int[] nums, int start, int target) {
        int end = nums.length - 1;
        int min = Integer.MAX_VALUE;
        int closestSum = Integer.MAX_VALUE;
        while(start < end) {
            if (min > Math.abs(target - (nums[start] + nums[end]))) {
                min = Math.abs(target - (nums[start] + nums[end]));
                closestSum = (nums[start] + nums[end]);
            }
            if ((nums[start] + nums[end]) < target) 
                start++;
            else {
                end--;
            }
        }
        return closestSum;
    }
    
    
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);
        int min = Integer.MAX_VALUE;
        int closestSum = Integer.MAX_VALUE;
        for (int i = 0; i < nums.length-2 ; i++) {
            int twoClosestSum = twoSumClosest(nums, i+1, target - nums[i]);
            if (min > Math.abs(target - (nums[i] + twoClosestSum))) {
                min = Math.abs(target - (nums[i] + twoClosestSum));
                closestSum = (nums[i] + twoClosestSum);
            }
        }
        return closestSum;
    }
    
	public static void main(String[] args) {
		ThreeSumClosest service = new ThreeSumClosest();
		int[] nums = {-1, 2, 1, -4};
		int target = 1;
		System.out.println(service.threeSumClosest(nums, target));
	}

}
