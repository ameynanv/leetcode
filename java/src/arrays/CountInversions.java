package arrays;

public class CountInversions {
	
	public int countInversions(int[] nums) {
		int count = sortAndCount(nums, 0, nums.length - 1);
		return count;
	}
	
	private int sortAndCount(int[] nums, int start, int end) {
		if (start == end) return 0;
		int mid = (start + end) >> 1;
		// split into two arrays 0 .. mid  and mid+1 .. end
		int leftCount = sortAndCount(nums, start, mid);
		int rightCount = sortAndCount(nums, mid+1, end);
		int mergeCount = merge(nums, start, mid, end);
		return leftCount+rightCount+mergeCount;
	}
	
	// Left Array nums[start] .. nums[mid]
	// Right Array nums[mid+1] .. nums[end]
	// Total elements = end - start + 1
	private int merge(int[] nums, int start, int mid, int end) {
		int N = end - start + 1;
		int[] temp = new int[N];
		int k = 0;
		int left = start;
		int right = mid + 1;
		int inversionCount = 0;
		while (k < N) {
			if (left < mid + 1 && right < end + 1) {
				if (nums[left] <= nums[right]) {
					temp[k++] = nums[left++];
				} else {
					temp[k++] = nums[right++];
					inversionCount += (mid - left + 1);
				}
			} else if (left < mid + 1) {
				temp[k++] = nums[left++];
			} else {
				temp[k++] = nums[right++];
			}
		}
		int j = start;
		for (int i = 0; i < N; i++) {
			nums[j++] = temp[i];
		}
		return inversionCount;
	}
	
	
	
}
