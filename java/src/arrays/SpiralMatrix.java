package arrays;
import java.util.List;
import java.util.ArrayList;

public class SpiralMatrix {
	public List<Integer> spiralOrder(int[][] matrix) {
        int H = matrix.length;
        if (H == 0) return new ArrayList<Integer>();
        int W = matrix[0].length;
        if (W == 0) return new ArrayList<Integer>();
        
        List<Integer> result = new ArrayList<Integer>();
        int X = 0, Y = 0;
        while(H > 0 && W > 0) {
            result.addAll(boundaryIterate(matrix, X,Y, W, H));
            X = X + 1;
            Y = Y + 1;
            W = W - 2;
            H = H - 2;
        }
        return result;
    }
    
    private List<Integer> boundaryIterate(int[][] matrix, int X, int Y, int W, int H) {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < W; i++) 
            list.add(matrix[X][Y+i]);
        for (int i = 1; i < H; i++) 
            list.add(matrix[X+i][Y+W-1]);
        if (H != 1) {
            for (int i = 1; i < W; i++)             
                list.add(matrix[X+H-1][Y+W-1-i]);
        }
        if (W != 1) {
            for (int i = 1; i < H-1; i++)             
                list.add(matrix[X+H-1-i][Y]);
        }
        return list;
    }
    
    public static void main(String[] args) {
    	SpiralMatrix service = new SpiralMatrix();
    	int[][] nums = {{1,2,3},{8,9,4},{7,6,5}};
    	System.out.println(service.spiralOrder(nums));
    }
}
