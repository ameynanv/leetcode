package arrays;

public class FirstMissingPositive {
    public int firstMissingPositive(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            while(nums[i] > 0 && nums[i] <= nums.length && nums[nums[i] - 1] != nums[i] ) {
                swap(nums, i, nums[i] - 1);
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != i+1) return (i+1);
        }
        return (nums.length+1);
    }
    
    private void swap(int[] nums, int a, int b) {
        int temp = nums[a];
        nums[a] = nums[b];
        nums[b] = temp;
    }
    
    public static void main(String[] args) {
    	int[] nums = {2,-3,-9,1,3,8,10000};
    	FirstMissingPositive service = new FirstMissingPositive();
    	System.out.println(service.firstMissingPositive(nums));
    }
}
