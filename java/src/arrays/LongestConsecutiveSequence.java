package arrays;

import java.util.HashMap;

// Given an unsorted array of integers, find the length of the longest consecutive elements sequence.
// For example,
// Given [100, 4, 200, 1, 3, 2],
// The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.
//
// Your algorithm should run in O(n) complexity.
// 
// https://leetcode.com/problems/longest-consecutive-sequence/
// 
public class LongestConsecutiveSequence {
	public int longestConsecutive(int[] nums) {
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		int maxSum = 0;
		for (int i = 0; i < nums.length; i++) {
			if (!map.containsKey(nums[i])) {
				int left = map.containsKey(nums[i] - 1) ? map.get(nums[i] - 1) : 0;
				int right = map.containsKey(nums[i] + 1) ? map.get(nums[i] + 1) : 0;
				int sum = left + right + 1;
				map.put(nums[i], sum);
				if (sum > maxSum) maxSum = sum;
				
				// This is the key statement. Update the ends of the consecutive sequence with 
				// length of the sequence.
				map.put(nums[i] - left, sum);
				map.put(nums[i] + right, sum);
			}
		}
		return maxSum;
	}
}
