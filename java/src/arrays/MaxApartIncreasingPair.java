package arrays;

// http://www.geeksforgeeks.org/given-an-array-arr-find-the-maximum-j-i-such-that-arrj-arri/
public class MaxApartIncreasingPair {

	public int maxDistanceOfIncreasingPair(int[] nums) {

		int[] minArray = new int[nums.length];
		int[] maxArray = new int[nums.length];

		int minNumber = Integer.MAX_VALUE;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] < minNumber) {
				minNumber = nums[i];
			}
			minArray[i] = minNumber;
		}

		int maxNumber = Integer.MIN_VALUE;
		for (int i = nums.length - 1; i >= 0; i--) {
			if (nums[i] > maxNumber) {
				maxNumber = nums[i];
			}
			maxArray[i] = maxNumber;
		}

		int maxDistance = 0;
		int i = 0, j = 0;
		while (i < nums.length && j < nums.length) {
			if (minArray[i] < maxArray[j]) {
				int distance = j - i;
				if (distance > maxDistance)
					maxDistance = distance;
				j++;
			} else {
				i++;
			}
		}
		return maxDistance;
	}
}
