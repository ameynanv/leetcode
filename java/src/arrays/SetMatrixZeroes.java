package arrays;

public class SetMatrixZeroes {
	public void setZeroes(int[][] matrix) {
		int M = matrix.length;
		int N = matrix[0].length;

		// check if any zeros are present in the first row
		boolean zeroInFirstRow = false;
		for (int c = 0; c < N; c++) {
			if (matrix[0][c] == 0)
				zeroInFirstRow = true;
		}

		// check if any zeros are present in the first column
		boolean zeroInFirstCol = false;
		for (int r = 0; r < M; r++) {
			if (matrix[r][0] == 0)
				zeroInFirstCol = true;
		}

		// We will use first col and first row to record the zeros
		for (int r = 1; r < M; r++) {
			for (int c = 1; c < N; c++) {
				if (matrix[r][c] == 0) {
					matrix[r][0] = 0;
					matrix[0][c] = 0;
				}
			}
		}

		// Set elements to zero
		for (int r = 1; r < M; r++) {
			for (int c = 1; c < N; c++) {
				if (matrix[r][0] == 0 || matrix[0][c] == 0) {
					matrix[r][c] = 0;
				}
			}
		}
		if (zeroInFirstRow) {
			for (int c = 0; c < N; c++)
				matrix[0][c] = 0;
		}
		if (zeroInFirstCol) {
			for (int r = 0; r < M; r++)
				matrix[r][0] = 0;
		}
	}
}
