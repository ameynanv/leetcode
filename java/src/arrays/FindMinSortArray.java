package arrays;

// http://www.geeksforgeeks.org/minimum-length-unsorted-subarray-sorting-which-makes-the-complete-array-sorted/
public class FindMinSortArray {
	
	// Method prints out the min and max indices of array to be sorted
	public void findMinSortArray(int[] nums) {
		// find s : index where first mismatch is happening from left -> right
		int s = 0;
		int temp = nums[s];
		while (s < nums.length && temp <= nums[s]) {
			temp = nums[s];
			s++;
		}
		s--;
		
		// find e : index where first mismatch is happening from right -> left
		int e = nums.length - 1;
		temp = nums[e];
		while (e >= 0 && temp >= nums[e]) {
			temp = nums[e];
			e--;
		}
		e++;
		
		// find min and max of the subarray nums[s] till nums[e]
		int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
		for (int i = s; i <= e; i++) {
			if (nums[i] < min) min = nums[i];
			if (nums[i] > max) max = nums[i];
		}
		
		// search for possible insertion of min in the array nums[0] to nums[s]
		while(s >= 0 && min < nums[s]) {
			s--;
		}
		s++;
		
		// search for possible insertion of max in the array nums[e] to nums[end]
		while(e < nums.length && max > nums[e]) {
			e++;
		}
		e--;		
		
		System.out.println("(" + s + "," + e + ")");
	}
	
	public static void main(String[] args) {
		int[] nums = {0, 12, 20, 30, 25, 40, 32, 31, 35, 50, 60};
		FindMinSortArray service = new FindMinSortArray();
		service.findMinSortArray(nums);
		
		int[] nums2 = {0, 1, 15, 25, 6, 7, 30, 40, 50};
		service.findMinSortArray(nums2);
		
	}
}
