package arrays;

public class BuyAndSellStockWithCooldown {
    
	public int maxProfit(int[] prices) {
        if (prices.length < 2) return 0;
        int[] buy = new int[prices.length];
        int[] sell = new int[prices.length];
        for (int i = 0; i < prices.length; i++) {
        	buy[i] = Math.max((i-1 < 0 ? -prices[i] : buy[i-1]), (i-2 < 0 ? -prices[i] : sell[i-2] - prices[i]));
        	sell[i] = Math.max((i-1 < 0 ? 0 : sell[i-1]), (i-1 < 0 ? -prices[i] : buy[i-1] + prices[i]));
        }
        return Math.max(buy[prices.length-1], sell[prices.length-1]);
    }
    
	public static void main(String[] args) {
		BuyAndSellStockWithCooldown service = new BuyAndSellStockWithCooldown();
		int[] prices = {1,2,3,0,2};
		System.out.println(service.maxProfit(prices));
		int[] prices2 = {1,2,3};
		System.out.println(service.maxProfit(prices2));
		int[] prices3 = {5,2,1};
		System.out.println(service.maxProfit(prices3));
	}

}
