package arrays;

public class MajorityElement {
    public int majorityElement(int[] nums) {
        int number = nums[0];
        int counter = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == number) {
                counter++;
            } else {
                if (counter == 1) {
                    number = nums[i];
                } else {
                    counter--;
                }
            }
        }
        return number;
    }
}
