package arrays;

public class RangeSum2D {
	public static void main(String[] args) {
		int[][] matrix = {{3,0,1,4,2},{5,6,3,2,1},{1,2,0,1,5},{4,1,0,1,7},{1,0,3,0,5}};
		NumMatrix numMatrix = new NumMatrix(matrix);
		System.out.println(numMatrix.sumRegion(2,1,4,3));
		System.out.println(numMatrix.sumRegion(1,1,2,2));
		System.out.println(numMatrix.sumRegion(1,2,2,4));
	}
}

class NumMatrix {

    private int[][] sum;
    public NumMatrix(int[][] matrix) {
        int H = matrix.length;
        if (H == 0) return;
        int W = matrix[0].length;
        if (W == 0) return;
        sum = new int[H][W];
        for (int r = 0; r < H; r++) {
            for (int c = 0; c < W; c++) {
                sum[r][c] = (isValid(r,c-1) ? sum[r][c-1] : 0) + matrix[r][c];
            }
        }
        for (int c = 0; c < W; c++) {
            for (int r = 0; r < H; r++) {
                sum[r][c] = (isValid(r-1,c) ? sum[r-1][c] : 0) + sum[r][c];
            }
        }        
    }
    
    private boolean isValid(int r,int c) {
        if (r < 0 || c < 0 || r >= sum.length || c >= sum[0].length) return false;
        return true;
    }

    public int sumRegion(int row1, int col1, int row2, int col2) {
        int commonSum = isValid(row1-1, col1-1) ? sum[row1-1][col1-1] : 0;
        int topSum = isValid(row1-1, col2) ? sum[row1-1][col2] : 0;
        int leftSum = isValid(row2,  col1-1) ? sum[row2][col1-1] : 0;
        return sum[row2][col2] - (leftSum+topSum-commonSum);
    }
    
}