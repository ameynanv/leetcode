package dynamicProgramming;

import java.util.Stack;
/**
 * https://leetcode.com/problems/maximal-rectangle/
 * 
 */
 public class MaximalRectangle {
    public int maximalRectangle(char[][] matrix) {
        int R = matrix.length;
        if (R == 0) return 0;
        int C = matrix[0].length;
        
        int maxValue = Integer.MIN_VALUE;
        
        int[] temp = new int[C];
        for (int i = 0; i < C; i++) 
            temp[i] = 0;
        
        for (int r = 0; r < R; r++) {
            // copy OR merge the row to temp array
            for (int i = 0; i < C; i++) {
                if (matrix[r][i] == '0') temp[i] = 0;
                else temp[i] += 1;
            }
            
            // now calculate the max rectangular area of the histogram
            int value = largestRectangleArea(temp);
            if (value > maxValue) maxValue = value;
        }
        
        return maxValue;
    }
    
    
    public int largestRectangleArea(int[] height) {
        if (height.length == 0) return 0;
		Stack<Integer> valueStack = new Stack<Integer>();
		Stack<Integer> indexStack = new Stack<Integer>();
		
		int maxArea = Integer.MIN_VALUE;
		int i;
		for (i = 0; i < height.length; i++) {
			if (valueStack.isEmpty()) {
				valueStack.push(height[i]);
				indexStack.push(i);
			} else if (valueStack.peek() <= height[i]){
				valueStack.push(height[i]);
				indexStack.push(i);				
			} else {
				int valueTop;
				while (!valueStack.isEmpty() && valueStack.peek() > height[i]) {
					valueTop = valueStack.pop();
					indexStack.pop();
					int area;
					if (valueStack.isEmpty()) {
						area = valueTop * (i);
					} else {
						area = valueTop * (i - indexStack.peek() - 1);	
					}
					if (area > maxArea) {
						maxArea = area;
					}
				}
				valueStack.push(height[i]);
				indexStack.push(i);				
			}
		}
		
		while (!valueStack.isEmpty()) {
			int valueTop = valueStack.pop();
			indexStack.pop();
			int area;
			if (valueStack.isEmpty()) {
				area = valueTop * (i);
			} else {
				area = valueTop * (i - indexStack.peek() - 1);	
			}
			if (area > maxArea) {
				maxArea = area;
			}
		}
		
		return maxArea;
    }
    
    public static void main(String[] args) {
        char[][] matrix = {
            {'0', '0', '0', '1', '1'},
            {'0', '1', '1', '0', '1'},
            {'0', '1', '1', '1', '1'},
            {'0', '0', '1', '1', '1'},
        };
        MaximalRectangle service = new MaximalRectangle();
        System.out.println(service.maximalRectangle(matrix));
    }
}