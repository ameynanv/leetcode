package dynamicProgramming;

/**
 * The longest Increasing Subsequence (LIS) problem is to find the length of the
 * longest subsequence of a given sequence such that all elements of the
 * subsequence are sorted in increasing order. For example, length of LIS for 
 * {10, 22, 9, 33, 21, 50, 41, 60, 80 } is 6 and 
 * LIS is {10, 22, 33, 50, 60, 80}.
 *
 */
public class LongestIncreasingSubsequenceI {
	int[] F;
	int N;

	int longestIncrSubsequenceLength(int[] num) {
		N = num.length;

		// F[i] will be the length of longestIncrSubsequence including num[i]
		F = new int[N + 1];
		init();

		for (int i = 0; i < N; i++) {
			// to find F[i], search F[j] 0 <= j < i such that num[j] < num[i] &&
			// F[j] + 1 > F[i]
			for (int j = i - 1; j >= 0; j--) {
				if (num[j] < num[i] && (F[j] + 1) > F[i]) {
					F[i] = F[j] + 1;
				}
			}
		}

		// Now find max F[i] for 0 <= i < n
		int max = 1;
		for (int i = 0; i < N; i++) {
			if (F[i] > max) {
				max = F[i];
			}
		}
		return max;
	}

	private void init() {
		for (int i = 0; i < N; i++) {
			F[i] = 1;
		}
	}
}
