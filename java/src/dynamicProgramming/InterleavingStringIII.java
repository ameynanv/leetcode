package dynamicProgramming;

// Trying bottom up approach
public class InterleavingStringIII {

	public boolean isInterleave(String s1, String s2, String s3) {
		int L1 = s1.length();
		int L2 = s2.length();
		int L3 = s3.length();
		boolean[][] F = new boolean[L1 + 1][L2 + 1];
		if (L1 + L2 != L3)
			return false;
		if (L1 == 0 && L2 == 0 && L3 == 0)
			return true;
		F[0][0] = true;
		
		for (int i = 0; i < s1.length(); i++) {
			F[i+1][0] = F[i][0] && s1.charAt(i) == s3.charAt(i);
		}
		for (int i = 0; i < s2.length(); i++) {
			F[0][i+1] = F[0][i] && s2.charAt(i) == s3.charAt(i);
		}

		for (int i1 = 0; i1 < s1.length(); i1++) {
			for (int i2 = 0; i2 < s2.length(); i2++) {
				F[i1+1][i2+1] = (F[i1][i2+1] && s1.charAt(i1) == s3.charAt(i2+i1+1)) ||
						        (F[i1+1][i2] && s2.charAt(i2) == s3.charAt(i2+i1+1)); 
			}
		}
		return F[L1][L2];
	}
}
