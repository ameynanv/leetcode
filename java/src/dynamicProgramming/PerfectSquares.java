package dynamicProgramming;

public class PerfectSquares {
	public int numSquares(int n) {
        if (n == 0) return 0;
        int[] F = new int[n+1];
        F[0] = 0;
        for (int i = 1; i < n+1; i++) {
            int minNumbers = Integer.MAX_VALUE;
            int s = 1;
            while (s*s <= i) {
                minNumbers = Math.min(minNumbers, 1+ F[i-s*s]);
                s++;
            }
            F[i] = minNumbers;
        }
        return F[n];
    }
	
	public static void main(String[] args) {
		PerfectSquares service = new PerfectSquares();
		System.out.println(service.numSquares(12));

	}

}
