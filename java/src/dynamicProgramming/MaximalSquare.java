package dynamicProgramming;

/**
 * Given a 2D binary matrix filled with 0's and 1's, find the largest square
 * containing all 1's and return its area.
 * https://leetcode.com/problems/maximal-square/
 */
public class MaximalSquare {

	public int maximalSquare(char[][] matrix) {
        int H = matrix.length;
        if (H == 0) return 0;
        int W = matrix[0].length;
        if (W == 0) return 0;
        
        int[][] L = new int[H][W];
        int[][] B = new int[H][W];
        int[][] F = new int[H][W];
        int max = Integer.MIN_VALUE;
        
        for (int i = H - 1; i >= 0; i--) {
            for (int j = 0; j < W; j++) {
                if (matrix[i][j] == '1') {
                    L[i][j] = j > 0 ? L[i][j-1] + 1 : 1;
                    B[i][j] = i < (H - 1) ? B[i+1][j] + 1 : 1;
                } else {
                    L[i][j] = 0;
                    B[i][j] = 0;                    
                }
                int K = Math.min(L[i][j], B[i][j]);
                F[i][j] = (j > 0 && i < (H - 1)) ? Math.min(K, F[i+1][j-1] + 1) : Math.min(K, 1);
                max = Math.max(F[i][j], max);
            }
        }
        return max*max;
    }

	public static void main(String[] args) {
		char[][] matrix = {{'1','0','1','0','0'}, 
				           {'0','1','1','1','0'},
				           {'0','1','1','1','1'},
				           {'0','0','1','1','0'}};
		MaximalSquare service = new MaximalSquare();
		System.out.println(service.maximalSquare(matrix));
	}
}
