package dynamicProgramming;

public class DistinctSubsequences {
	
	int[][] F; 	// F[i][j] stores the num of distinct subsequences for
	           	// s(0 .. j) and t(0 .. i)
				// so final solution is F[t.length()][s.length()]
	
	public int numDistinct(String s, String t) {
		if (s.length() == 0) return 0;
		F = new int[t.length() + 1][s.length() + 1];
		for (int c = 0; c < s.length() + 1; c++)
			F[0][c] = 1;
		
		for (int r = 1; r < t.length() + 1; r++) {
			for (int c = 1; c < s.length() + 1; c++) {
				if (s.charAt(c-1) == t.charAt(r-1)) {
					F[r][c] = F[r-1][c-1] + F[r][c-1];
				} else {
					F[r][c] = F[r][c-1];
				}
			}
		}
		return F[t.length()][s.length()];
	}
	
	public static void main(String[] args) {
		DistinctSubsequences service = new DistinctSubsequences();
		System.out.println(service.numDistinct("", ""));
	}
}
