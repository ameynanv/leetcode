package dynamicProgramming;

/**
 * https://leetcode.com/problems/decode-ways/
 */
public class DecodeWays {
	
	int[] F; // F[i] stores numDecodings of s(i .. end)
			 // So final solution will be F[0];
	public int numDecodings(String s) {
		int L = s.length();
		if (L == 0) return 0;
		F = new int[s.length() + 1]; 
		F[L] = 1;
		F[L-1] = s.charAt(L-1) != '0' ? 1 : 0;
		
		for (int i = L-2; i >= 0 ; i--) {
			if (s.charAt(i) == '0') continue;
			String substring = s.substring(i, i+2);
			Integer num = Integer.parseInt(substring);
			F[i] = (num > 0 && num <= 26) ? F[i+1] + F[i+2] : F[i+1]; 
		}
		return F[0];
	}
	
	public static void main(String[] args) {
		DecodeWays service = new DecodeWays();
		System.out.println(service.numDecodings("02"));
	}
}
