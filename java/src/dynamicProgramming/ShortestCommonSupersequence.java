package dynamicProgramming;

public class ShortestCommonSupersequence {

	public String SCS(String t1, String t2) {
		String[][] F = new String[t1.length()+1][t2.length()+1];
		
		for (int i = 0; i < t1.length(); i++)
			for (int j = 0; j < t2.length(); j++)
				F[i][j] = null;
		
		return findSCS(t1, t2, 0, 0, F);
	}
	
	private String findSCS(String t1, String t2, int s1, int s2, String[][] F) {
		if (F[s1][s2] != null) return F[s1][s2];
		if (s1 == t1.length() || s2 == t2.length()) {
			F[s1][s2] =  s1 == t1.length() ? t2.substring(s2, t2.length()) : t1.substring(s1, t1.length());
			return F[s1][s2];
		}
		if (t1.charAt(s1) == t2.charAt(s2)) {
			F[s1][s2] = t1.charAt(s1) + "" + findSCS(t1,t2, s1+1, s2+1, F);
			return F[s1][s2];
		} else {
			String one = t1.charAt(s1) + "" + findSCS(t1,t2, s1+1, s2, F);
			String two = t2.charAt(s2) + "" + findSCS(t1,t2, s1, s2+1, F);
			F[s1][s2] = two.length() < one.length() ? two : one;
			return F[s1][s2];
		}
	}
	
	public static void main(String[] args) {
		ShortestCommonSupersequence service = new ShortestCommonSupersequence();
		System.out.println(service.SCS("geek", "eke"));
		System.out.println(service.SCS("AGGTAB", "GXTXAYB"));

	}

}
