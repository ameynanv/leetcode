package dynamicProgramming;
import java.util.TreeSet;

public class LongestIncreasingSubsequenceIII {

	public int lengthOfLIS(int[] nums) {
        TreeSet<Integer> tree = new TreeSet<Integer>();
        for (int i = 0; i < nums.length; i++) {
            Integer ceil = tree.ceiling(nums[i]);
            if (ceil != null) 
                tree.remove(ceil);
            tree.add(nums[i]);
        }
        return tree.size();
    }
}
