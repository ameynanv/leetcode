package dynamicProgramming;

import java.util.ArrayList;
import java.util.List;

public class GenerateParentheses {
	ArrayList<ArrayList<String>> solutions;
	int N;

	public List<String> generateParenthesis(int n) {
		N = n;
		solutions = new ArrayList<ArrayList<String>>(N + 1);
		init();
		return solveProblem(n);
	}

	private void init() {
		for (int i = 0; i < N + 1; i++) {
			solutions.add(new ArrayList<String>());
		}
		ArrayList<String> t0 = new ArrayList<String>();
		t0.add("");
		solutions.set(0, t0);
		ArrayList<String> t1 = new ArrayList<String>();
		t1.add("()");
		solutions.set(1, t1);
	}

	ArrayList<String> solveProblem(int m) {
		if (solutions.get(m).size() > 0)
			return solutions.get(m);
		ArrayList<String> result = new ArrayList<String>();
		for (int i = 0; i < m; i++) {
			ArrayList<String> innerParenthesis = solveProblem(i);
			ArrayList<String> outerParenthesis = solveProblem(m - i - 1);
			for (int r = 0; r < innerParenthesis.size(); r++) {
				for (int c = 0; c < outerParenthesis.size(); c++) {
					result.add("(" + innerParenthesis.get(r) + ")"
							+ outerParenthesis.get(c));
				}
			}
		}
		solutions.set(m, result);
		return result;
	}
}
