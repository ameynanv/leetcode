package dynamicProgramming;

public class CoinChange {

	int N, M;
	int[] F;

	// Returns the count of ways we can sum S[0...M-1] coins to get sum n
	public int numberOfWaysOfChange(int[] S, int n) {
		N = n;
		M = S.length;
		F = new int[N + 1];
		F[0] = 1;

		for (int i = 0; i < M; i++) {
			for (int j = S[i]; j <= N; j++) {
				F[j] += F[j - S[i]];
			}
		}
		return F[N];
	}
}
