package dynamicProgramming;

import java.util.List;
import java.util.ArrayList;

public class PalindromePartitioningII {
    
    @SuppressWarnings("unchecked")
	public List<List<String>> partition(String s) {
        int L = s.length();
        // P[i][j] stores if s.substring(i, j+1) is palidrome or not
        boolean[][] P = new boolean[L][L];
        init_P(s, P);
        
        // F[i] stores number of possible palindrom partitioning for s.substring(0, i)
        // F[j] = for i = 0 to j-1 result.addAll(   P[i][j-1] ? (F[i]+s.substring(i,j)) : none )
        List<List<String>>[] F = new ArrayList[L+1];
        F[0] = new ArrayList<List<String>>();
        for (int j = 1; j < L+1; j++) {
            F[j] = new ArrayList<List<String>>();
            for (int i = 0; i < j; i++) {
                if (P[i][j-1]) {
                    for (List<String> list: F[i]) {
                        List<String> clone = new ArrayList<String>(list);
                        clone.add(s.substring(i,j));
                        F[j].add(clone);
                    }
                    if (F[i].size() == 0) {
                    	List<String> list = new ArrayList<String>();
                    	list.add(s.substring(i,j));
                    	F[j].add(list);
                    }
                } 
            }
        }
        return F[L];
    }
    
    private void init_P(String s, boolean[][] P) {
        int L = s.length();
        for (int i = 0; i < L; i++)
            P[i][i] = true;
            
        // Find P for all S sized sequences
        for (int S = 2; S <= L; S++) {            
            for (int i = 0; i <= L - S; i++) {
                if (S == 2)
                    P[i][i+S-1] = (s.charAt(i) == s.charAt(i+S-1));
                else
                    P[i][i+S-1] = (s.charAt(i) == s.charAt(i+S-1)) ? P[i+1][i+S-2] : false;
            }
        }
    }
    
    public static void main(String[] args) {
        PalindromePartitioningII service = new PalindromePartitioningII();
        //System.out.println(service.partition("aba"));
        System.out.println(service.partition("abbab"));
        // [["a","b","b","a","b"],["a","b","bab"],["a","bb","a","b"],["abba","b"]]
    }
}