package dynamicProgramming;

public class BurstBalloons {
	public int maxCoins(int[] nums) {
        int[] A = new int[nums.length + 2];
        int[][] F = new int[nums.length+2][nums.length+2];
        for (int i = 0; i < nums.length+2; i++)
        	for (int j = 0; j < nums.length+2; j++)
        		F[i][i] = 0;
        		
        A[0] = 1;
        A[nums.length + 1] = 1;
        for (int i = 0; i < nums.length; i++)
            A[i+1] = nums[i];
        return findMaxCoins(F, 0, A.length-1, A);
    }
    
    private int findMaxCoins(int[][] F, int left, int right, int[] A) {
    	if (F[left][right] != 0) return F[left][right];
        int result = Integer.MIN_VALUE;
        if (right-left == 1) return 0;
        for (int i = left+1; i < right; i++) {
            int leftProduct = findMaxCoins(F, left, i, A);
            int rightProduct = findMaxCoins(F, i, right, A);
            result = Math.max(result, leftProduct+rightProduct+A[left]*A[i]*A[right]);
        }
        F[left][right] = result;
        return result;
    }
    
    public static void main(String[] args) {
    	BurstBalloons service = new BurstBalloons();
    	int[] nums = {3,1,5,8};
    	System.out.println(service.maxCoins(nums));
    }
}
