package dynamicProgramming;

import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;

//Ugly numbers are numbers whose only prime factors are 2, 3 or 5. The sequence
//1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15
//shows the first 11 ugly numbers. By convention, 1 is included.
//Write a program to find and print the 150th ugly number.
public class UglyNumbers {
	private PriorityQueue<Integer> pq = new PriorityQueue<Integer>(1,
			new Comparator<Integer>() {
				@Override
				public int compare(Integer o1, Integer o2) {
					if (o1 < o2) return -1;
					if (o1 > o2) return 1;
					return 0;
				}
			});
	
	private HashSet<Integer> set;
	private int count;
	private int N;
	
	public int getNthUglyNumber(int n) {
		set = new HashSet<Integer>();
		count = 0;
		N = n;
		pq.add(1);
		set.add(1);
		int ans = 0;
		while (count < N) {
			ans = pq.remove();
			if (!set.contains(ans * 2)) {
				pq.add(ans * 2);
				set.add(ans * 2);
			}
			if (!set.contains(ans * 3)) {
				pq.add(ans * 3);
				set.add(ans * 3);
			}
			if (!set.contains(ans * 5)) {
				pq.add(ans * 5);
				set.add(ans * 5);
			}
			count++;
		}
		return ans;
	}
}
