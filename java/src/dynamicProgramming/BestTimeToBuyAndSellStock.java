package dynamicProgramming;

public class BestTimeToBuyAndSellStock {
	public int maxProfit(int[] prices) {
		if (prices.length == 0)
			return 0;
		int maxProfit = 0;
		int currentProfit = 0;
		for (int i = 1; i < prices.length; i++) {
			if (currentProfit < 0) {
				currentProfit = prices[i] - prices[i - 1];
			} else {
				currentProfit += prices[i] - prices[i - 1];
			}
			if (currentProfit > maxProfit) {
				maxProfit = currentProfit;
			}
		}
		return maxProfit;
	}
}
