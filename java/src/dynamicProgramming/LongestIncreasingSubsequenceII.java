package dynamicProgramming;

import java.util.TreeSet;

public class LongestIncreasingSubsequenceII {
	int N;

	int longestIncrSubsequenceLength(int[] num) {
		N = num.length;
		TreeSet<Integer> treeSet = new TreeSet<Integer>();
		for (int i = 0; i < N; i++) {
			Integer successor = treeSet.ceiling(num[i]);
			if (successor != null) {
				treeSet.remove(successor);
			}
			treeSet.add(num[i]);
		}

		return treeSet.size();
	}
}
