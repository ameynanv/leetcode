package dynamicProgramming;

public class MaximumSumRectangleIn2DMatrix {
	
	public class SubArray1D {
		int sum;
		int start;
		int end;
		public SubArray1D(int start, int end, int sum) {
			this.start = start;
			this.end = end;
			this.sum = sum;
		}
	}
	
	public class SubArray2D {
		int sum;
		int topLeftRow;
		int topLeftColumn;
		int bottomRightRow;
		int bottomRightColumn;

		public SubArray2D(int topLeftRow, int topLeftColumn, int bottomRightRow, int bottomRightColumn, int sum) {
			this.topLeftRow = topLeftRow;
			this.topLeftColumn = topLeftColumn;
			this.bottomRightRow = bottomRightRow;
			this.bottomRightColumn = bottomRightColumn;
			this.sum = sum;
		}
	}
	
	
	public SubArray2D findMaxContSubMatrix(int[][] matrix) {
		int R = matrix.length;
		int C = matrix[0].length;
		int[] tempArray = new int[R];

		int maxSum = Integer.MIN_VALUE;
		int maxTopLeftRow = 0;
		int maxTopLeftColumn = 0;
		int maxBottomRightRow = 0;
		int maxBottomRightColumn = 0;

		int curSum = matrix[0][0];
		int curTopLeftRow = 0;
		int curTopLeftColumn = 0;
		int curBottomRightRow = 0;
		int curBottomRightColumn = 0;
		
		for (int i = 0; i < C; i++) {
			// Copy first column to tempArray
			for (int r = 0; r < R; r++) {
				tempArray[r] = 0;
			}
			for (int j = i; j < C; j++) {
				// Copy first column to tempArray
				for (int r = 0; r < R; r++) {
					tempArray[r] += matrix[r][j];
				}
				SubArray1D result = findMaxContSubArray(tempArray, 0, tempArray.length - 1);
				curSum = result.sum;
				curTopLeftRow = result.start;
				curBottomRightRow = result.end;
				curTopLeftColumn = i;
				curBottomRightColumn = j;
				
				if (curSum > maxSum) {
					maxSum = curSum;
					maxTopLeftRow = curTopLeftRow;
					maxBottomRightRow = curBottomRightRow;
					maxTopLeftColumn = curTopLeftColumn;
					maxBottomRightColumn = curBottomRightColumn;	
				}
				
			}
		}
		return new SubArray2D(maxTopLeftRow, maxTopLeftColumn, maxBottomRightRow, maxBottomRightColumn, maxSum);
	}
	
	
	public SubArray2D findMinContSubMatrix(int[][] matrix) {
		int R = matrix.length;
		int C = matrix[0].length;
		int[] tempArray = new int[R];

		int minSum = Integer.MAX_VALUE;
		int minTopLeftRow = 0;
		int minTopLeftColumn = 0;
		int minBottomRightRow = 0;
		int minBottomRightColumn = 0;

		int curSum = matrix[0][0];
		int curTopLeftRow = 0;
		int curTopLeftColumn = 0;
		int curBottomRightRow = 0;
		int curBottomRightColumn = 0;
		
		for (int i = 0; i < C; i++) {
			// Copy first column to tempArray
			for (int r = 0; r < R; r++) {
				tempArray[r] = 0;
			}
			for (int j = i; j < C; j++) {
				// Copy first column to tempArray
				for (int r = 0; r < R; r++) {
					tempArray[r] += matrix[r][j];
				}
				SubArray1D result = findMinContSubArray(tempArray, 0, tempArray.length - 1);
				curSum = result.sum;
				curTopLeftRow = result.start;
				curBottomRightRow = result.end;
				curTopLeftColumn = i;
				curBottomRightColumn = j;
				
				if (curSum < minSum) {
					minSum = curSum;
					minTopLeftRow = curTopLeftRow;
					minBottomRightRow = curBottomRightRow;
					minTopLeftColumn = curTopLeftColumn;
					minBottomRightColumn = curBottomRightColumn;	
				}
				
			}
		}
		return new SubArray2D(minTopLeftRow, minTopLeftColumn, minBottomRightRow, minBottomRightColumn, minSum);
	}
	
	
	// finds max continuous sub array in nums[start] till nums[end] 
	public SubArray1D findMaxContSubArray(int[] nums, int start, int end) {
		if (start >= 0 && end < nums.length) {
			int maxSum = Integer.MIN_VALUE;
			int maxStart = start;
			int maxEnd = start;
			
			int currentStart = start;
			int currentEnd = start;
			
			int sum = nums[start];
			for (int i = start + 1; i <= end; i++) {
				if (sum < 0) {
					sum = nums[i];
					currentStart = i;
					currentEnd = i;
				} else {
					sum += nums[i];
					currentEnd = i;
				}
				if (sum > maxSum) {
					maxSum = sum;
					maxStart = currentStart;
					maxEnd = currentEnd;
				}
			}
			return new SubArray1D(maxStart, maxEnd, maxSum);
		}
		return null;
	}
	
	// finds min continuous sub array in nums[start] till nums[end] 
	public SubArray1D findMinContSubArray(int[] nums, int start, int end) {
		if (start >= 0 && end < nums.length) {
			int minSum = Integer.MAX_VALUE;
			int minStart = start;
			int minEnd = start;
			
			int currentStart = start;
			int currentEnd = start;
			
			int sum = nums[start];
			for (int i = start + 1; i <= end; i++) {
				if (sum > 0) {
					sum = nums[i];
					currentStart = i;
					currentEnd = i;
				} else {
					sum += nums[i];
					currentEnd = i;
				}
				if (sum < minSum) {
					minSum = sum;
					minStart = currentStart;
					minEnd = currentEnd;
				}
			}
			return new SubArray1D(minStart, minEnd, minSum);
		}
		return null;
	}
	
	public static void main(String[] args) {
		MaximumSumRectangleIn2DMatrix service = new MaximumSumRectangleIn2DMatrix();
		int[] nums = {-8,-2,1,-5,-6,20,25,-100,30};
		SubArray1D result = service.findMaxContSubArray(nums, 0, nums.length - 1);
		System.out.println("MAX SUM : (" + result.start + "," + result.end + ") = " + result.sum);
		result = service.findMinContSubArray(nums, 0, nums.length - 1);
		System.out.println("MIN SUM : (" + result.start + "," + result.end + ") = " + result.sum);
		
		int[][] matrix = {{ 2, 1,-3,-4, 5},
		                { 0, 6, 3, 4, 1},
			        	{ 2,-2,-1, 4,-5},
			        	{-3, 3, 1, 0, 3}};
		SubArray2D matResult = service.findMaxContSubMatrix(matrix);
		System.out.println("MAX SUM : (" + matResult.topLeftRow + "," + matResult.topLeftColumn + ") to (" + matResult.bottomRightRow + "," + matResult.bottomRightColumn + ")= " + matResult.sum);

		matResult = service.findMinContSubMatrix(matrix);
		System.out.println("MIN SUM : (" + matResult.topLeftRow + "," + matResult.topLeftColumn + ") to (" + matResult.bottomRightRow + "," + matResult.bottomRightColumn + ")= " + matResult.sum);
		
	}
}
