package dynamicProgramming;

/**
 * http://www.geeksforgeeks.org/dynamic-programming-set-17-palindrome-
 * partitioning/
 * 
 * Refer to PalindronePartitioningIII
 * This is O(n^3) that one is O(n^2)
 */
public class PalindromePartitioning {
	
	// F[i][j] = findMinCut(str, i, j);
	int[][] F;
	
	public int minCuts(String str) {
		F = new int[str.length() + 1][str.length() + 1];
		for (int r = 0; r < str.length() + 1; r++)
			for (int c = 0; c < str.length() + 1; c++)
				F[r][c] = -1;
		return findMinCut(str, 0, str.length() - 1);
	}

	private int findMinCut(String str, int start, int end) {
		if (F[start][end] != -1) return F[start][end];
		if (start >= end) {
			F[start][end] = 0;
			return 0;
		}
		if (isPalindrome(str, start, end)) {
			F[start][end] = 0;
			return 0;
		}
		
		int minValue = Integer.MAX_VALUE;
		for (int i = start; i < end; i++) {
			int val = findMinCut(str, start, i) + findMinCut(str, i+1, end) + 1;
			if (val < minValue) 
				minValue = val;
		}
		F[start][end] = minValue;
		return minValue;
	}
	
	private boolean isPalindrome(String str, int start, int end) {
		int i = start;
		int j = end;
		while (i < j) {
			if (str.charAt(i++) != str.charAt(j--)) return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		String str = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		PalindromePartitioning service = new PalindromePartitioning();
		System.out.println(service.minCuts(str));
	}
}
