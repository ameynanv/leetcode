package dynamicProgramming;

public class HouseRobberII {
	public int rob(int[] nums) {
        if (nums.length == 0) return 0;
        if (nums.length == 1) return nums[0];
        int val1 = findMax(nums);
        for (int i = 0; i < nums.length/2; i++) {
            int tmp = nums[i];
            nums[i] = nums[nums.length-1-i];
            nums[nums.length-1-i] = tmp;
        }
        int val2 = findMax(nums);    
        return Math.max(val1, val2);
    }
    
    private int findMax(int[] nums) {
        int[] F = new int[nums.length + 1];
        F[0] = 0;
        for (int i = 1; i < nums.length; i++)
            F[i] = Math.max(F[i-1], nums[i-1] + ((i - 2 > 0) ? F[i-2] : 0));        
        return F[nums.length - 1];
    }
    
    public static void main(String[] args) {
    	HouseRobberII service = new HouseRobberII();
    	int[] nums = {2,1,4}; 
    	System.out.println(service.rob(nums));
    }
    
}
