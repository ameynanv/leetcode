package dynamicProgramming;

import java.util.ArrayDeque;

public class LongestValidParentheses {
    public int longestValidParentheses(String s) {
    	ArrayDeque<Integer> stack = new ArrayDeque<Integer>();
    	int maxCount = 0;
		int left = -1;
    	for (int i = 0; i < s.length(); i++) {
    		if (s.charAt(i) == '(') {
    			stack.push(i);
    		} else {
    			if (stack.isEmpty()) left = i;
    			else {
    				stack.pop();
    				if (stack.isEmpty()) maxCount = Math.max(maxCount,  i - left);
    				else  maxCount = Math.max(maxCount,  i - stack.peek());
    			}
    		}
    	}
        return maxCount;
    }
    
    public static void main(String[] args) {
    	LongestValidParentheses service = new LongestValidParentheses();
    	System.out.println(service.longestValidParentheses("()(())"));
    }
}
