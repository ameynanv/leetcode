package dynamicProgramming;

// DP Version
public class PalindromePartitioningIII {
    
    int[] F;
    boolean[][] P;
    public int minCut(String s) {
        F = new int[s.length()];
        P = new boolean[s.length()][s.length()];
        init_F();
        init_P(s);
        for (int i = 0; i < s.length(); i++) {
            F[i] = Integer.MAX_VALUE;
            if (P[0][i]) F[i] = 0;
            else {
                for (int j = 0; j < i; j++) {
                    if (P[j+1][i] && (F[j] + 1) < F[i])
                        F[i] = F[j] + 1;
                }
            }
        }
        return F[s.length() - 1];
    }
    
    private void init_F() {
        F[0] = 0;
        for (int i = 1; i < F.length ; i++) {
            F[i] = Integer.MAX_VALUE;
        }
    }
    
    private void init_P(String s) {
        for (int i = 0; i < s.length(); i++)
            P[i][i] = true;
    
        // Find P for all L sized sequences
        for (int L = 2; L <= s.length(); L++) {            
            for (int i = 0; i < s.length()-L+1; i++) {
                if (L == 2) 
                    P[i][i+1] = (s.charAt(i) == s.charAt(i+L-1));
                else 
                    P[i][i+L-1] = (s.charAt(i) == s.charAt(i+L-1)) && P[i+1][i+L-2];
            }
        }
    }

    public static void main(String[] args) {
        PalindromePartitioningIII service = new PalindromePartitioningIII();
        System.out.println(service.minCut("coder"));
        //System.out.println(service.minCut("abba"));
    }
    
}