package dynamicProgramming;

public class EggDroppingPuzzle {
	/* Function to get minimum number of trails needed in worst
	  case with n eggs and k floors */
	
	int[][] F;
	int N,K;
	public int eggDrop(int n, int k) {
		N = n;
		K = k;
		init();
		return findMinTrials(n, k);
	}
	
	private void init() {
		F = new int[N+1][K+1];
		for (int r = 0; r < N+1; r++) {
			for (int c = 0; c < K+1; c++) {
				if (r == 1 || c == 0 || c == 1) {
					F[r][c] = c;
				} else {
					F[r][c] = -1;
				}
			}
		}
	}
	
	private int findMinTrials(int n, int k) {
		if (F[n][k] != -1) return F[n][k];
		int count = 0;
		int minCount = k;
		for (int c = 1; c <= k; c++) {
			int countWhenEggBroke = findMinTrials(n-1, c-1);
			int countWhenEggIntact = findMinTrials(n, k-c);
			count = countWhenEggBroke > countWhenEggIntact ? countWhenEggBroke : countWhenEggIntact;
			if (count < minCount) {
				minCount = count;
			}
		}
		F[n][k] = 1 + minCount;
		return F[n][k];
	}
}
