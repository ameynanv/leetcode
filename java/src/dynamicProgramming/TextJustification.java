package dynamicProgramming;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/text-justification/
 * http://www.geeksforgeeks.org/dynamic-programming-set-18-word-wrap/
 * 
 * This is a greedy approach
 */
public class TextJustification {
	
	public List<String> fullJustify(String[] words, int maxWidth) {
		ArrayList<String> result = new ArrayList<String>();
		if (words.length == 0 || maxWidth == 0) {
			result.add("");
			return result;
		}

		// This list stores which words are included in every line.
		ArrayList<ArrayList<Integer>> output = new ArrayList<ArrayList<Integer>>();

		// This array stores how much extra spaces should be added after every
		// word.
		// ES[j] means ES[j] spaces should be added before words[j].
		int[] ES = new int[words.length + 1];

		int i = 0;
		while (i < words.length) {
			ArrayList<Integer> wordsInLine = new ArrayList<Integer>();
			int currentLineWidth = -1;
			int startWordIndex = i;
			while (i < words.length	&& (currentLineWidth + 1 + words[i].length()) <= maxWidth) {
				if (currentLineWidth == -1) {
					wordsInLine.add(i);
					startWordIndex = i;
					currentLineWidth = words[i].length();
				} else {
					wordsInLine.add(i);
					ES[i] = 1;
					currentLineWidth += words[i].length() + 1;
				}
				i++;
			}
			int extraSpace = maxWidth - currentLineWidth;

			while (startWordIndex < i - 1 && extraSpace > 0) {
				for (int j = startWordIndex + 1; extraSpace > 0 && j < i; j++) {
					ES[j] += 1;
					extraSpace--;
				}
			}
			if (startWordIndex == i - 1) {
				ES[i] += extraSpace;
			}
			output.add(wordsInLine);
		}

		// Reconstruct final text using ES and output
		for (int j = 0; j < output.size(); j++) {
			String line = "";
			for (Integer m : output.get(j)) {
				// add word
				line = line + words[m];

                if (j == output.size() - 1) {
                    if (m < words.length - 1) {
                        line = line + " ";
                    } else {
                        while (line.length() < maxWidth) {
                            line = line + " ";    
                        }
                    }
                } else {
    				// add extra spaces
    				for (int k = 0; k < ES[m + 1]; k++)
    					line = line + " ";
                }

			}
			result.add(line);
		}
        		
		
		return result;
	}

	public static void main(String[] args) {
//		String[] words = { "THIS", "IS", "AN", "EXAMPLE", "OF", "TEXT",
//				"JUSTIFICATION" };
		String[] words = {"Listen","to","many,","speak","to","a","few."};
		TextJustification service = new TextJustification();
		System.out.println(service.fullJustify(words, 6));
	}
}
