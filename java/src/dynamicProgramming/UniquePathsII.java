package dynamicProgramming;

public class UniquePathsII {
	public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int H = obstacleGrid.length;
        if (H == 0) return 0;
        int W = obstacleGrid[0].length;
        if (W == 0) return 0;
        if (obstacleGrid[0][0] == 1 || obstacleGrid[H-1][W-1] == 1) return 0;
        if (H == 1 && W == 1) return 1;
        
        int[][] F = new int[H][W];
        F[H-1][W-1] = 1;
        for (int i = H-2; i >= 0; i--)
            F[i][W-1] = (obstacleGrid[i][W-1] != 1 && F[i+1][W-1] == 1) ? 1 : 0;
        for (int i = W-2; i >= 0; i--) 
            F[H-1][i] = (obstacleGrid[H-1][i] != 1 && F[H-1][i+1] == 1) ? 1 : 0;
        F[H-1][W-1] = 0;
        
        for (int r = H-2; r >= 0; r--) {
            for (int c = W-2; c >= 0; c--) {
                F[r][c] = (obstacleGrid[r][c] != 1) ? (F[r+1][c] +  F[r][c+1]) : 0;
            }
        }
        return F[0][0];
    }
	
	public static void main(String[] args) {
		UniquePathsII service = new UniquePathsII();
		int[][] nums = {{0,0,0},{0,1,0},{0,0,0}};
		System.out.println(service.uniquePathsWithObstacles(nums));

	}

}
