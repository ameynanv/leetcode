package dynamicProgramming;
import java.util.List;
import java.util.ArrayList;

public class Triangle {
	public int minimumTotal(List<List<Integer>> triangle) {
		int H = triangle.size();
		if (H == 0)
			return 0;
		for (int i = H - 2; i >= 0; i--) {
			List<Integer> current = triangle.get(i);
			List<Integer> next = triangle.get(i + 1);
			for (int j = 0; j < current.size(); j++) {
				int minAdjNext = 0;
				if (j + 1 < next.size()) {
					minAdjNext = Math.min(next.get(j), next.get(j + 1));
				}
				current.set(j, current.get(j) + minAdjNext);
			}
		}
		return triangle.get(0).get(0);
	}

	public static void main(String[] args) {
		Triangle service = new Triangle();
		List<List<Integer>> listOfList= new ArrayList<List<Integer>>();
		List<Integer> list0 = new ArrayList<Integer>();
		list0.add(1);
		List<Integer> list1 = new ArrayList<Integer>();
		list1.add(-1);list1.add(2);
		listOfList.add(list0);
		listOfList.add(list1);
		System.out.println(service.minimumTotal(listOfList));
	}

}
