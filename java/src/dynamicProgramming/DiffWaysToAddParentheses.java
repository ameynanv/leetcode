package dynamicProgramming;

import java.util.List;
import java.util.ArrayList;

public class DiffWaysToAddParentheses {
	
	List<Integer>[][] F;
    @SuppressWarnings("unchecked")
	public List<Integer> diffWaysToCompute(String input) {
    	F = new List[input.length()+1][input.length()+1];
    	for (int i = 0; i < input.length()+1; i++)
    		for (int j = 0; j < input.length()+1; j++)
    			F[i][j] = null;
    	
    	return compute(input, 0, input.length());
    }
    
    private List<Integer> compute(String input, int start, int end) {
    	if (F[start][end] != null) return F[start][end];
    	
    	String subset = input.substring(start, end);
    	List<Integer> result = new ArrayList<Integer>();
    	if (subset.split("[*\\-+]").length == 1) {
    		result.add(Integer.parseInt(subset));
    		F[start][end] = result;
    		return result;
    	}
    	for (int i = start ; i < end; i++) {
    		if (input.charAt(i) == '*' || input.charAt(i) == '-' || input.charAt(i) == '+') {
	    		List<Integer> leftResult = compute(input, start, i);
	    		List<Integer> rightResult = compute(input, i+1, end);
	    		for (Integer left: leftResult) {
	    			for (Integer right: rightResult) {
	    				if (input.charAt(i) == '*') result.add(left*right);
	    				if (input.charAt(i) == '-') result.add(left-right);
	    				if (input.charAt(i) == '+') result.add(left+right);
	    			}
	    		}
    		}
    	}
    	F[start][end] = result;
        return result;
    }
	
	public static void main(String[] args) {
		DiffWaysToAddParentheses service = new DiffWaysToAddParentheses();
		System.out.println(service.diffWaysToCompute("2-1-1"));
		System.out.println(service.diffWaysToCompute("2*3-4*5"));
	}
}
