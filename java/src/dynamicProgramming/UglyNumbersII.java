package dynamicProgramming;


//Ugly numbers are numbers whose only prime factors are 2, 3 or 5. The sequence
//1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15
//shows the first 11 ugly numbers. By convention, 1 is included.
//Write a program to find and print the 150 th ugly number.
public class UglyNumbersII {

	public int getNthUglyNumber(int n) {
		int[] uglyNumbers = new int[n+1];
		int pow2 = 1;
		int pow3 = 1;
		int pow5 = 1;
		
		uglyNumbers[1] = 1;
		
		for (int i = 1; i < n ; i++) {
			uglyNumbers[i+1] = min(uglyNumbers[pow2] * 2,  uglyNumbers[pow3] * 3, uglyNumbers[pow5] * 5);
			if (uglyNumbers[i+1] == (uglyNumbers[pow2] * 2)) {
				pow2++;
			}
			if (uglyNumbers[i+1] == (uglyNumbers[pow3] * 3)) {
				pow3++;
			}			
			if (uglyNumbers[i+1] == (uglyNumbers[pow5] * 5)) {
				pow5++;
			}			
		}
		
		return uglyNumbers[n];
	}
	
	private int min(int a, int b, int c) {
		if (a <= b && a <= c) return a;
		if (b <= a && b <= c) return b;
		else return c;
		
	}
	
	public static void main(String[] args) {
		UglyNumbersII service = new UglyNumbersII();
		System.out.println(service.getNthUglyNumber(10));
	}
}
