package dynamicProgramming;

//
// http://www.geeksforgeeks.org/dynamic-programming-set-13-cutting-a-rod/
//
public class CuttingRod {
	
	int[] F;
	public int maxValue(int[] prices) {
		F = new int[prices.length + 1];
		F[1] = prices[0];
		return findMaxValue(prices, prices.length);
	}
	
	private int findMaxValue(int[] prices, int n) {
		if (F[n] != 0) return F[n];
		int maxValue = prices[n-1];
		for (int i = 1; i < n; i++) {
			int value = findMaxValue(prices, i) + findMaxValue(prices, n - i);
			if (value > maxValue)
				maxValue = value;
		}
		F[n] = maxValue;
		return maxValue;
	}
	
	public static void main(String[] args) {
		int[] prices = {3, 5, 8, 9, 10,17,17, 20};
		CuttingRod service = new CuttingRod();
		System.out.println(service.maxValue(prices));
	}
	
}
