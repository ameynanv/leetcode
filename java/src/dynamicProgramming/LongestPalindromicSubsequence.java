package dynamicProgramming;

/**
 * http://www.geeksforgeeks.org/dynamic-programming-set-12-longest-palindromic-
 * subsequence/
 *
 */
public class LongestPalindromicSubsequence {
	
	public int longestPalidromeLength(String str) {
		return lps(str, 0, str.length() -1);
	}
	
	private int lps(String str, int start, int end) {
		if (start > end) return 0;
		if (start == end) return 1;
		if (str.charAt(start) == str.charAt(end)) {
			return 2 + lps(str, start+1, end-1);
		} else {
			int case1 = lps(str, start + 1, end);
			int case2 = lps(str, start, end - 1);
			return case1 > case2 ? case1 : case2;
		}
	}
	
	public static void main(String[] args) {
		String str = "BBABCBCAB";
		LongestPalindromicSubsequence service = new LongestPalindromicSubsequence();
		System.out.println(service.longestPalidromeLength(str));
	}
}
