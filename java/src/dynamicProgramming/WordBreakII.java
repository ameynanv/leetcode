package dynamicProgramming;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;


public class WordBreakII {
	List<String>[][] F;
	int L;
	
    @SuppressWarnings("unchecked")
	public List<String> wordBreak(String s, Set<String> wordDict) {
    	L = s.length();
    	F = new List[L][L];
    	wordBreak(s, wordDict, 0, L-1);
    	return F[0][L-1];
    }
    
    private List<String> wordBreak(String s, Set<String> wordDict, int i, int j) {
    	if (F[i][j] != null) return F[i][j];
    	if (i > j) { F[i][j] = new ArrayList<String>(); return F[i][j]; }
    	if (i == j) {
    		if (wordDict.contains(s.substring(i, j+1))) {
    			List<String> result = new ArrayList<String>();
    			result.add(s.substring(i, j+1));
    			F[i][j] = result; 
    			return F[i][j];
    		} else {
    			F[i][j] = new ArrayList<String>(); 
    			return F[i][j];
    		}
    	}
		List<String> result = new ArrayList<String>();
		if (wordDict.contains(s.substring(i, j+1))) {
			result.add(s.substring(i, j+1));
		}
    	for (int k = i; k < j; k++) {
    		String left = wordDict.contains(s.substring(i, k+1)) ? s.substring(i, k+1) : null;
    		if (left != null) {
    			List<String> right = wordBreak(s, wordDict, k+1, j);
				for (String rs: right) {
					result.add(left + " " + rs);
				}
    		}
    	}
    	F[i][j] = result;
    	return F[i][j];
    }
    
    
    
	public static void main(String[] args) {
		WordBreakII service = new WordBreakII();
		Set<String> dict = new HashSet<String>();
		//String[] arr = {"a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"}; 
		String[] arr = {"a", "b", "c"};
		for (int i = 0; i < arr.length; i++)
			dict.add(arr[i]);
		//String target = "baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		String target = "abc";
		System.out.println(service.wordBreak(target, dict));
		//System.out.println(service.wordBreak("abc", dict));

	}
}
