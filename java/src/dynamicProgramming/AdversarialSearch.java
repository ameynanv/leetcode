package dynamicProgramming;

//
// http://www.geeksforgeeks.org/dynamic-programming-set-31-optimal-strategy-for-a-game/
//
// http://www.careercup.com/question?id=15422849
//
public class AdversarialSearch {
	public int maximizeGameValue(int[] nums) {
		return  maximize(nums, 0, nums.length - 1);
	}
	
	private int maximize(int[] nums, int start, int end) {
		if (start > end) return 0;
		int vStart = nums[start] + minimize(nums, start+1, end);
		int vEnd = nums[end] + minimize(nums, start, end-1);
		return vStart > vEnd ? vStart : vEnd;
	}
	
	private int minimize(int[] nums, int start, int end) {
		if (start > end) return 0;
		int vStart = maximize(nums, start+1, end);
		int vEnd = maximize(nums, start, end - 1);
		return vStart < vEnd ? vStart: vEnd;
	}
	
}
