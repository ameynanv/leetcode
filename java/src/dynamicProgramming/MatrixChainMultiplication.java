package dynamicProgramming;

//Given a sequence of matrices, find the most efficient way to multiply these matrices together. 
//The problem is not actually to perform the multiplications, 
//but merely to decide in which order to perform the multiplications.
public class MatrixChainMultiplication {

	int F[][];
	int N;

	// mat_0 = p[0] x p[1]
	// mat_i = p[i] x p[i+1]
	// mat_N-1 = p[N-1] x p[N]
	// there are total N mat from mat_0 to mat_N-1
	public int MatrixChainOrder(int p[]) {
		N = p.length - 1;
		F = new int[N + 1][N + 1];
		init();

		return findOptimalCalc(p, 0, N - 1);
	}

	private void init() {
		for (int r = 0; r < N; r++) {
			for (int c = 0; c < N; c++) {
				if (r == c) {
					F[r][c] = 0;
				} else {
					F[r][c] = -1;
				}
			}
		}
	}

	private int findOptimalCalc(int p[], int i, int j) {
		if (F[i][j] != -1)
			return F[i][j];

		int minCalc = Integer.MAX_VALUE;
		int calc = 0;
		for (int k = i; k < j; k++) {
			calc = findOptimalCalc(p, i, k) + findOptimalCalc(p, k + 1, j)
					+ p[i] * p[j + 1] * p[k + 1];
			if (calc < minCalc) {
				minCalc = calc;
			}
		}
		F[i][j] = minCalc;
		return minCalc;
	}
}
