package dynamicProgramming;

public class BestTimeToBuyAndSellStockII {
    public int maxProfit(int[] prices) {
        // Edge Cases
        if (prices.length < 2) return 0;
        
        int profit = 0;
        for (int i = 0; (i+1) < prices.length ; i++) {
            int diff = prices[i+1] - prices[i];
            if (diff > 0) profit += diff;
        }
        
        return profit;
    }
}
