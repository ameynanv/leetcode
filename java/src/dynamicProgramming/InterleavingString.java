package dynamicProgramming;

public class InterleavingString {

    public boolean isInterleave(String s1, String s2, String s3) {
    	int L1 = s1.length();
    	int L2 = s2.length();
    	int L3 = s3.length();
    	if (L1 + L2 != L3) return false;
    	if (L1 == 0 && L2 == 0 && L3 == 0) return true;
		boolean result = false;
		if (L1 > 0 && s1.charAt(0) == s3.charAt(0)) {
			String s1_trunc = s1.substring(1, L1);
			String s3_trunc = s3.substring(1, L3);
			result = result || isInterleave(s1_trunc, s2, s3_trunc);
		}
		if (L2 > 0  && s2.charAt(0) == s3.charAt(0)) {
			String s2_trunc = s2.substring(1, L2);
			String s3_trunc = s3.substring(1, L3);
			result = result || isInterleave(s1, s2_trunc, s3_trunc);
		}
		return result;	   
    }
}
