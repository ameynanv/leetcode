package dynamicProgramming;

public class InterleavingStringII {

	//
	// For input: 
	//  s1 = "ab", s2 = "def" :  s3: "abdef"
	// The output solution array F looks like:
	//  	1	f	d	e	 
	//		b	0	0	1	 
	//  	a	0	0	1	
	//
	// Explaination:
	// 	F["ab"]["def"] = F[2][3] = F["b"]["def"] || F["ab"]["ef"] = F[1][3] || F[2][2]
	//  F[i][j] = F[i-1][j] || F[i][j-1]
	//  where :
	// 		F[i][j] tell you if s1[(s1.length - i) .. end] and s2[(s2.length - j) .. end] 
	//      matches with s3[(s1.length + s2.length - (i+j)) .. end]
	// 
	
	//
	// For input: 
	//  s1 = "ab", s2 = "def" :  s3: "adbef"
	// The output solution array F looks like:
	//		1	1	1	0	 
	//		0	0	1	1	 
	//		0	0	0	1
	// 
		
	
	int[][] F;

	public boolean isInterleave(String s1, String s2, String s3) {
		int L1 = s1.length();
		int L2 = s2.length();
		int L3 = s3.length();
		F = new int[L1 + 1][L2 + 1];

		if (L1 + L2 != L3)
			return false;
		if (L1 == 0 && L2 == 0 && L3 == 0)
			return true;
		
		F[0][0] = 1;
		return findSolution(s1, s2, s3);
	}
	
	private boolean findSolution(String s1, String s2, String s3) {
		int L1 = s1.length();
		int L2 = s2.length();
		int L3 = s3.length();
		if (F[L1][L2] != 0) return (F[L1][L2] == 1);
		
		if (L1 + L2 != L3) {
			F[L1][L2] = -1;
			return false;
		}
		boolean result = false;
		if (L1 > 0 && s1.charAt(0) == s3.charAt(0)) {
			String s1_trunc = s1.substring(1, L1);
			String s3_trunc = s3.substring(1, L3);
			result = result || findSolution(s1_trunc, s2, s3_trunc);
		}
		if (L2 > 0 && s2.charAt(0) == s3.charAt(0)) {
			String s2_trunc = s2.substring(1, L2);
			String s3_trunc = s3.substring(1, L3);
			result = result || findSolution(s1, s2_trunc, s3_trunc);
		}
		
		F[L1][L2] = result ? 1 : -1;
		return result;		
	}
}
