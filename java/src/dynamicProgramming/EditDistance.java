package dynamicProgramming;

public class EditDistance {

	int S, T;
	int[][] F; // F[i][j] will store the editDistance of string source(0 .. i-1)
				// to convert it to target(0 .. j-1)

	public int editDistance(String source, String target) {
		S = source.length();
		T = target.length();
		init();
		return edit(source, target, S, T);
	}
	
	// This method returns the edit distance to convert source(0 .. m-1) to
	// target(0 .. n-1)
	private int edit(String source, String target, int m, int n) {
		if (F[m][n] != -1) return F[m][n];
		if (source.charAt(m-1) == target.charAt(n-1)) {
			F[m][n] = edit(source, target, m-1, n-1);
		} else {
			int o1 = 1 + edit(source, target, m-1, n); // delete char from source
			int o2 = 1 + edit(source, target, m, n-1); // insert char into source
			int o3 = 1 + edit(source, target, m-1, n-1); // replace char from source
			if (o1 <= o2 && o2 <= o3) F[m][n] = o1;
			else if (o1 <= o3 && o3 <= o2) F[m][n] = o1;
			else if (o2 <= o3 && o3 <= o1) F[m][n] = o2;
			else if (o2 <= o1 && o1 <= o3) F[m][n] = o2;
			else if (o3 <= o1 && o1 <= o2) F[m][n] = o3;
			else F[m][n] = o3;
		}
		return F[m][n];
	}

	private void init() {
		F = new int[S + 1][T + 1];
		for (int i = 0; i < S+1; i++) {
			for (int j = 0; j < T+1; j++) {
				if (i == 0 && j == 0) {
					F[i][j] = 0;
				} else if (i == 0 && j != 0) {
					F[0][j] = j;
				} else if (i != 0 && j == 0) {
					F[i][0] = i;
				} else {
					F[i][j] = -1;
				}
			}
		}
	}
}
