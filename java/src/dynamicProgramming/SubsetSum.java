package dynamicProgramming;

public class SubsetSum {
	
	public boolean isSubsetSum(int[] nums) {
		int sum = 0;
		for (int i = 0; i < nums.length; i++)
			sum += nums[i];
		if (sum%2 != 0) return false;
		return isSubsetSum(nums, nums.length, sum/2);
	}
	
	private boolean isSubsetSum(int[] nums, int n, int target) {
		if (target == 0) return true;
		if (n == 0 && target != 0) return false;
		return isSubsetSum(nums, n - 1, target) || isSubsetSum(nums, n - 1, target - nums[n-1]);
	}
	
	public static void main(String[] args) {
		int[] nums = {-1,5,10,6};
		SubsetSum service = new SubsetSum();
		System.out.println(service.isSubsetSum(nums));
	}
}
