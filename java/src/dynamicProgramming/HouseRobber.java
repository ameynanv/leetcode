package dynamicProgramming;

public class HouseRobber {
	int[] F; // F[i] stores the optimum solution for nums[0 .. i]

	// So final ans is F[nums.length - 1]
	public int rob(int[] nums) {
		F = new int[nums.length];
		if (nums.length == 0)
			return 0;
		F[0] = nums[0];
		if (nums.length == 1)
			return F[0];

		F[1] = Math.max(nums[0], nums[1]);
		if (nums.length == 2)
			return F[1];

		for (int i = 2; i < nums.length; i++) {
			F[i] = Math.max(F[i - 1], nums[i] + F[i - 2]);
		}
		return F[nums.length - 1];
	}
}
