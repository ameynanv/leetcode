package dynamicProgramming;

// https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/
public class BestTimeToBuyAndSellStockIII {
    
    public int maxProfit(int[] prices) {
        if (prices.length < 2) return 0;
        // F[i] stores max Profit by making single transaction from prices[0] to prices[i]
        int[] F = new int[prices.length];
        
        int max = 0;
        int sum = 0;
        for (int i = 1; i < prices.length; i++) {
            if (sum < 0) {
                sum = (prices[i] - prices[i-1]);
            } else {
                sum += (prices[i] - prices[i-1]);    
            }
            if (max < sum) {
                max = sum;
            }
            F[i] = max;
        }
        
        // Now start from right to left and calculate max profit
        int maxTotalProfit = 0;
        int maxRightToLeft = 0;
        sum = 0;
        for (int i = prices.length - 2; i >= 0; i--) {
            if (sum < 0) {
                sum = prices[i+1] - prices[i];
            } else {
                sum += prices[i+1] - prices[i];
            }
            if (maxRightToLeft < sum) 
                maxRightToLeft = sum;
            
            if (i >= 1) {
                if (maxTotalProfit < (maxRightToLeft + F[i-1]))
                    maxTotalProfit = maxRightToLeft + F[i-1];
            } else {
                if (maxTotalProfit < maxRightToLeft)
                    maxTotalProfit = maxRightToLeft;                
            }
        }
        
        return maxTotalProfit;
    }
    
    public static void main(String[] args) {
        //int[] prices = {1,2,4,3,5,7,2,4,9,0,12};
        int[] prices = {5,1};
        BestTimeToBuyAndSellStockIII service = new BestTimeToBuyAndSellStockIII();
        System.out.println(service.maxProfit(prices));
    }
}