package dynamicProgramming;

public class EditDistanceII {
	int[][] F; // F[i][j] = minDistance of word1(i .. end) and word2(j .. end);

	public int minDistance(String word1, String word2) {
		if (word1.length() == 0 && word2.length() == 0)
			return 0;
		if (word1.length() == 0 && word2.length() != 0)
			return word2.length();
		if (word1.length() != 0 && word2.length() == 0)
			return word1.length();
		F = new int[word1.length() + 1][word2.length() + 1];
		for (int r = 0; r < word1.length() + 1; r++)
			for (int c = 0; c < word2.length() + 1; c++)
				F[r][c] = -1;
		return editDistance(word1, word2, 0, 0);
	}

	public int editDistance(String word1, String word2, int i, int j) {
		if (F[i][j] != -1) return F[i][j];
		
		int L1 = word1.length();
		int L2 = word2.length();
		if (i == L1 && j == L2) {
			F[i][j] = 0;
			return F[i][j];
		} 
		if (i < L1 && j == L2) {
			F[i][j] = L1 - i;
			return F[i][j];
		} 
		if (j < L2 && i == L1) {
			F[i][j] = L2 - j;
			return F[i][j];
		}

		if (word1.charAt(i) == word2.charAt(j)) {
			F[i][j] = editDistance(word1, word2, i + 1, j + 1);
			return F[i][j];
		} else {
			int replaceDistance = 1 + editDistance(word1, word2, i + 1, j + 1);
			int insertDistance = 1 + editDistance(word1, word2, i, j + 1);
			int deleteDistance = 1 + editDistance(word1, word2, i + 1, j);
			F[i][j] = min(replaceDistance, insertDistance, deleteDistance);
			return F[i][j];
		}
	}

	private int min(int a, int b, int c) {
		if (a <= b && a <= c)
			return a;
		if (b <= a && b <= c)
			return b;
		return c;
	}
	
	public static void main(String[] args) {
		EditDistanceII service = new EditDistanceII();
		System.out.println(service.minDistance("ab", "a"));
	}
	
	
}
