package dynamicProgramming;

/**
 * LCS Problem Statement: Given two sequences, find the length of longest
 * subsequence present in both of them. A subsequence is a sequence that appears
 * in the same relative order, but not necessarily contiguous.
 * 
 * LCS for input Sequences "ACDGH" and "AEDFHR" is "ADH" of length 3.
 * LCS for input Sequences "AGGTAB" and "GXTXAYB" is "GTAB" of length 4.
 *
 */
public class LongestCommonSubsequenceI {

	int M, N;
	int[][] F; // F[i][j] = longestCommonSubsequence of num1(0 .. i-1) and
				// num2(0 .. j-1)

	public int longestCommonSubsequence(int[] num1, int[] num2) {
		M = num1.length;
		N = num2.length;
		if (M == 0 || N == 0)
			return 0;
		init();
		return lcs(num1, num2, M, N);
	}

	private void init() {
		F = new int[M + 1][N + 1];
		for (int i = 0; i < M + 1; i++) {
			for (int j = 0; j < N + 1; j++) {
				if (i == 0 || j == 0) {
					F[i][j] = 0;
				} else {
					F[i][j] = -1;
				}
			}
		}

	}

	// This method finds longestCommonSubsequence for num1(0 .. n1-1) and num2(0
	// .. n2-1)
	private int lcs(int[] num1, int[] num2, int n1, int n2) {
		if (F[n1][n2] != -1)
			return F[n1][n2];
		if (num1[n1 - 1] == num2[n2 - 1]) {
			F[n1][n2] = 1 + lcs(num1, num2, n1 - 1, n2 - 1);
		} else {
			int l1 = lcs(num1, num2, n1 - 1, n2);
			int l2 = lcs(num1, num2, n1, n2 - 1);
			if (l1 > l2) {
				F[n1][n2] = l1;
			} else {
				F[n1][n2] = l2;
			}
		}
		return F[n1][n2];
	}

}
