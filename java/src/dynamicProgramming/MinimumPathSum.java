package dynamicProgramming;

public class MinimumPathSum {
	public int minPathSum(int[][] grid) {
        if (grid.length == 0) return 0;
        int m = grid.length;
        int n = grid[0].length;
        for (int r = m-2; r >= 0; r--) {
            grid[r][n-1] = grid[r+1][n-1] + grid[r][n-1];
        }
        for (int c = n-2; c >= 0; c--) {
            grid[m-1][c] = grid[m-1][c+1] + grid[m-1][c];
        }
        for (int r = m-2; r >= 0; r--) {
            for (int c = n-2; c >= 0; c--) {    
                grid[r][c] += (grid[r+1][c] < grid[r][c+1]) ? grid[r+1][c] : grid[r][c+1];
            }
        }
        return grid[0][0];
    }
}
