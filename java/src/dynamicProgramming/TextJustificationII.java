package dynamicProgramming;

import java.util.List;
import java.util.ArrayList;
import java.util.ArrayDeque;

/**
 * http://www.geeksforgeeks.org/dynamic-programming-set-18-word-wrap/
 * 
 * This is a dynamic programming approach
 */
public class TextJustificationII {

	public List<String> fullJustify(String[] words, int maxWidth) {
		// C[j] = { C[i-1] + LC(i,j) } 1 <= i <= j
		// C[j] = cost of arranging words from from 0 to j
		// LC(i,j) = cost of arranging words from i to j on a new single line
		
		int[] C = new int[words.length + 1];
		int[][] LC = new int[words.length][words.length];

		for (int i = 0; i < words.length; i++) {
			for (int j = i; j < words.length; j++) {
				int len = words[i].length();
				for (int k = i + 1; k <= j; k++) 
					len += (1 + words[k].length());
				if (len > maxWidth)
					LC[i][j] = Integer.MAX_VALUE;
				else
					LC[i][j] = (maxWidth - len) * (maxWidth - len) * (maxWidth - len); 
			}
		}
		
		C[0] = LC[0][0];
		int[] p = new int[words.length];
		int minCost = Integer.MAX_VALUE;
		for (int j = 1; j < words.length; j++) {
			minCost = Integer.MAX_VALUE;
			for (int i = 1; i <= j; i++) {
				if (C[i-1] < Integer.MAX_VALUE && LC[i][j] < Integer.MAX_VALUE) {
					int cost = C[i-1] + LC[i][j];
					if (cost < minCost) {
						minCost = cost;
						p[j] = i;
					}
				} 
			}
			C[j] = minCost;
		}
		
				
		System.out.println("Cost : " + minCost);
		return printSolution(p, words);
	}
	
	private List<String> printSolution(int[] p, String[] words) {
		ArrayDeque<String> deque = new ArrayDeque<String>();
		int previousLast = p.length - 1;
		while (previousLast >= 0) {
			//System.out.println("Line Contails words from : "+ p[previousLast] + " till " + previousLast);
			StringBuilder builder = new StringBuilder();
			for (int i = p[previousLast]; i <= previousLast; i++)
				builder.append(words[i] + " ");
			deque.addFirst(builder.toString());
			previousLast = p[previousLast] - 1;
		}
		
		List<String> result = new ArrayList<String>();
		for (String str: deque)
			result.add(str);
		return result;
	}

	public static void main(String[] args) {
		String words[] = {"aaa","bb","cc","ddddd"};
		TextJustificationII service = new TextJustificationII();
		List<String> result = service.fullJustify(words, 6);
		for (String str: result)
			System.out.println(str);
	}
}
