package bit;

public class ReverseBitsUnsigned {
	 // you need treat n as an unsigned value
    public int reverseBits(int n) {
        int out = 0;
        int k = 0;
        while (k < 32) {
            if ((n & 0x1) != 0) {
                out = (out | 0x1);
            }
            if (k < 31) {
                n = n >>> 0x1;
                out = out << 0x1;
            }
            k++;
        }
        return out;
    }
}
