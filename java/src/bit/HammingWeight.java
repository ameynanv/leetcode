package bit;

/**
 * https://leetcode.com/problems/number-of-1-bits/
 */
public class HammingWeight {
    // you need to treat n as an unsigned value
    public int hammingWeight(int n) {
       int k = n;
       int mask = 1;
       int count = 0;
       while (k != 0) {
           count = count + (k & mask);
           k = k >>> 1;
       }
       return count; 
    }
}
