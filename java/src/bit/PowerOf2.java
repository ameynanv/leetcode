package bit;

public class PowerOf2 {
	public boolean isPowerOfTwo(int n) {
		if (n <= 0)
			return false;
		if (n == 1)
			return true;
		if (n % 2 != 0)
			return false;
		return (countNumberOf1s(n) == 1);
	}

	private int countNumberOf1s(int n) {
		int mask = 1;
		int count = 0;
		while (n > 0) {
			count = count + (n & mask);
			n = n >> 1;
		}
		return count;
	}
}
