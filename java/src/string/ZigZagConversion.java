package string;

/**
 * Convert to zig zag pattern  https://leetcode.com/problems/zigzag-conversion/ 
 *  ABCDEFGHIJKLMNOP , 4 => 
 *  
 *  A     G     M
 *  B   F H   L N
 *  C E   I K   O
 *  D     J     P
 *  
 *  Output is  AGMBFHLNCEIKODJP
 *             
 */
public class ZigZagConversion {
	public String convert(String s, int numRows) {
        if (numRows <= 0) return s;
        StringBuffer[] mat = new StringBuffer[numRows];
        for (int i = 0; i < numRows; i++)
            mat[i] = new StringBuffer();
        
        char[] str = s.toCharArray();
        int k = 0;
        while(k < str.length) {
            for (int i = 0; k < str.length && i < numRows; i++)
                mat[i].append(str[k++]);
            for (int i = numRows - 2; k < str.length && i >= 1; i--)
                mat[i].append(str[k++]);
        }
        
        StringBuffer result = new StringBuffer();
        for (StringBuffer buf: mat) 
            result.append(buf);
        
        return result.toString();
    }
	
	public static void main(String[] args) {
		ZigZagConversion service = new ZigZagConversion();
		System.out.println(service.convert("ABCDEFGHIJKLMNOP", 4));

	}

}
