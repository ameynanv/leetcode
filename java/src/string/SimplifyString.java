package string;
import java.util.ArrayDeque;

public class SimplifyString {
	
	public String simplifyPath(String path) {
        ArrayDeque<String> deque = new ArrayDeque<String>();
        String[] strs = path.split("/");
        for (String str: strs) {
            if (str.length() == 0) continue;
            if (str.equalsIgnoreCase(".")) continue;
            if (str.equalsIgnoreCase("..")) {
                if (!deque.isEmpty()) 
                    deque.removeLast();
            } else {
                deque.addLast(str);
            }
        }
        StringBuffer buffer = new StringBuffer();
        if (deque.size() == 0) return "/";
        for (String str: deque) {
            buffer.append("/");
            buffer.append(str);
        }
        return buffer.toString();
    }
	
	public static void main(String[] args) {
		SimplifyString service = new SimplifyString();
		System.out.println(service.simplifyPath("/../aa/bb/../cc/..///dd"));
	}

}
