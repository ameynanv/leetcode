package string;

public class ValidPalindrome {
    
    public boolean isPalindrome(String s) {
        s = s.toLowerCase();
        if (s.length() == 0) return true;
        int left = 0;
        int right = s.length() - 1;
        while(left < right) {
            while(left < s.length() && !isValidAlphaNumChar(s, left))
                left++;
            while(right >= 0 && !isValidAlphaNumChar(s, right))
                right--;
            if (left >= right) break;
            if (s.charAt(left) != s.charAt(right)) return false;
            left++;
            right--;
        }
        return true;
    }
    
    private boolean isValidAlphaNumChar(String s, int index) {
        String a = s.charAt(index) + "";
        return a.matches("[a-z0-9]");
    }
    
    public static void main(String[] args) {
        String str = " A man, a plan, a canal: Panama :.";
        ValidPalindrome service = new ValidPalindrome();
        System.out.println(service.isPalindrome(str));
    }
    
}