package string;

public class KMPStringSearch {

	public int strStr(String text, String pattern) {
		if (pattern.length() == 0) return 0;
        int[][] dfa = new int[256][pattern.length()];
        char[] P = pattern.toCharArray();
        buildDFA(dfa, P);
        
        char[] T = text.toCharArray();
        int i,j;
        for (i = 0, j = 0; i < T.length && j < P.length; i++)
        	j = dfa[T[i]][j];
        if (j == P.length) return i - P.length;
        else return -1;
    }
	
	private void buildDFA(int[][] dfa, char[] P) {
		dfa[P[0]][0] = 1;
		for (int X = 0, j = 1; j < P.length; j++) {
			for (int i = 0; i < 256; i++) 
				dfa[i][j] = dfa[i][X];
			dfa[P[j]][j] = j+1;
			X = dfa[P[j]][X];
		}
	}
	
	public static void main(String[] args) {
		KMPStringSearch service = new KMPStringSearch();
		System.out.println(service.strStr("mississippi", "issipi"));
	}

}
