package string;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

public class SubstringWithConcatenationAllWords {

	public List<Integer> findSubstring(String s, String[] words) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		int L = words[0].length();
		int T = words.length;
		
		HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
		for (String str: words) {
			addStrToMap(targetMap, str);
		}
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		int left = 0;
		while ((left + L) < s.length()) {
			if (targetMap.containsKey(s.substring(left, left + L))) {
				int count = 0;
				int right = left;
				map.clear();
				while (count < T && (right + L) < s.length()) {
					String key = s.substring(right, right + L);
					if (targetMap.containsKey(key)) {
						addStrToMap(map, key);
						if (map.get(key) <= targetMap.get(key)) {
							right = right + L;
							count++;
						} else {
							break;
						}
					} else {
						break;
					}
				}
				if (count == T) {
					result.add(left);
				}
			}
			left++;
		}
		return result;
	}

	private void addStrToMap(HashMap<String, Integer> map, String key) {
		if (map.containsKey(key)) {
			map.put(key, map.get(key) + 1);
		} else {
			map.put(key, 1);	
		}
	}
	public static void main(String[] args) {
		SubstringWithConcatenationAllWords service = new SubstringWithConcatenationAllWords();
		String[] words = {"foo", "bar"};
		String s = "barfoothefoobarman";
		System.out.println(service.findSubstring(s,words));
	}

}
