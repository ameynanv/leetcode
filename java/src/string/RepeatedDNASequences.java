package string;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * All DNA is composed of a series of nucleotides abbreviated as A, C, G, and T,
 * for example: "ACGAATTCCG". When studying DNA, it is sometimes useful to
 * identify repeated sequences within the DNA.
 * 
 * Write a function to find all the 10-letter-long sequences (substrings) that
 * occur more than once in a DNA molecule.
 * 
 * For example, Given s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT",
 * 
 * Return: ["AAAAACCCCC", "CCCCCAAAAA"].
 *
 */
public class RepeatedDNASequences {

	public List<String> findRepeatedDnaSequences(String s) {
		List<String> result = new ArrayList<String>();
		if (s.length() <= 10)
			return result;
		HashSet<Integer> single = new HashSet<Integer>();
		HashSet<Integer> moreThanOne = new HashSet<Integer>();
		HashMap<Character, Integer> map = new HashMap<Character, Integer>();
		map.put('A', 0);
		map.put('C', 1);
		map.put('G', 2);
		map.put('T', 3);
		int hash = 0;
		for (int i = 0; i < 10; i++) {
			hash = hash << 2;
			hash = hash | map.get(s.charAt(i));
		}
		single.add(hash);
		for (int i = 10; i < s.length(); i++) {
			hash = hash << 2;
			hash = hash | map.get(s.charAt(i));
			hash &= 0xfffff;
			if (!single.add(hash) && moreThanOne.add(hash))
				result.add(s.substring(i - 9, i + 1));
		}
		return result;
	}

	public static void main(String[] args) {
		RepeatedDNASequences service = new RepeatedDNASequences();
		System.out.println(service.findRepeatedDnaSequences("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"));
	}

}
