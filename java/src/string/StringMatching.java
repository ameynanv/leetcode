package string;

public class StringMatching {
	
	public int strStr(String haystack, String needle) {
        if (needle.length() > haystack.length()) return -1;
        int i,j;
        for (i = 0; i < haystack.length(); i++) {
        	for (j = 0; j < needle.length() && (i+j) < haystack.length(); j++) {
        		if (needle.charAt(j) != haystack.charAt(i+j)) {
        			j = 0;
        			i += j;
        			break;
        		}
        	}
        	if (j == needle.length()) return i;
        }
        return -1;
    }
	
	public static void main(String[] args) {
		StringMatching service = new StringMatching();
		System.out.println(service.strStr("mississippi", "issipi"));
	}
}
