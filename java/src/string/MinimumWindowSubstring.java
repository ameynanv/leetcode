package string;


/**
 * https://leetcode.com/problems/minimum-window-substring/
 * 
 * Given a string S and a string T, find the minimum window in S which will
 * contain all the characters in T in complexity O(n).
 * 
 * For example, S = "ADOBECODEBANC" T = "ABC" Minimum window is "BANC".
 */
public class MinimumWindowSubstring {
	
	public String minWindow(String s, String t) {
		int minRange = Integer.MAX_VALUE;
		int minStart = 0, minEnd = 0;
		
		int Lt = t.length();
		char[] S = s.toCharArray();
		char[] T = t.toCharArray();
		int[] tHist = new int[128];
		for (int i = 0; i < T.length; i++)
			tHist[T[i]] += 1;
		
		int[] temp = new int[128];
		int start = 0;
		while(start < S.length && tHist[S[start]] == 0) {
			start++;
		}
		
		int counter = 0;
		for (int i = start; i < S.length; i++) {
			if (tHist[S[i]] > 0 ) {
				if (tHist[S[i]] - temp[S[i]] > 0) {
					counter++;	
				}				
				temp[S[i]] += 1;
			}
			while (counter == Lt) {
				int range = i - start + 1;
				if (range < minRange) {
					minRange = range;
					minStart = start;
					minEnd = i;
				}
				temp[S[start]] -= 1;
				if (temp[S[start]] < tHist[S[start]])
					counter -= 1;
				int j = start + 1;
				while(j < i && tHist[S[j]] == 0) {
					j++;
				}
				start = j;
			}
		}
		
		return minRange < Integer.MAX_VALUE ? s.substring(minStart, minEnd+1) : "";
	}
	
	
	public static void main(String[] args) {
		//String source = "ADOBECODEBANC";
		//String target = "ABC";
		String source = "BBA";
		String target = "AB";
		MinimumWindowSubstring service = new MinimumWindowSubstring();
		System.out.println(service.minWindow(source, target));
	}
}
