package string;

/**
 * Given a string S, find the longest palindromic substring in S. You may assume
 * that the maximum length of S is 1000, and there exists one unique longest
 * palindromic substring.
 *
 */
public class LongestPalindromicSubstring {

	
	public String longestPalindrome(String str) {
		String result = "";
		char[] s = str.toCharArray();
		for (int i = 0; i < s.length; i++) {
			String even = findPalindrome(str, i, i);
			String odd = findPalindrome(str, i, i+1);
			if (even.length() > odd.length()) {
				result = result.length() < even.length() ? even : result;
			} else {
				result = result.length() < odd.length() ? odd : result;
			}
		}
		return result;
	}
	
	private String findPalindrome(String str, int left, int right) {
		char[] s = str.toCharArray();
		while(left >= 0 && right < s.length && s[left] == s[right]) {
			left--;
			right++;
		}
		return str.substring(left+1, right); 
	}

	public static void main(String[] args) {
		String s = "BBAABCDCBAPWAB";
		LongestPalindromicSubstring service = new LongestPalindromicSubstring();
		System.out.println(service.longestPalindrome(s));
	}
}

