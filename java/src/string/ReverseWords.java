package string;

public class ReverseWords {
    
    public String reverseWords(String s) {
        String[] strs = s.split("\\s+");
        StringBuilder builder = new StringBuilder();
        for (int i = strs.length-1 ; i >= 0; i--) {
            builder.append(" ");    
            builder.append(strs[i]);    
        }
        String result = builder.toString();
        return result.trim();
    }
    
    public static void main(String[] args) {
        String str = "the   sky  is   blue   ";
        ReverseWords service = new ReverseWords();
        System.out.println(service.reverseWords(str));
    }
    
    
}