package string;

public class LongestSubstringWORepeat {
    
    public int lengthOfLongestSubstring(String s) {
        int[] charMap = new int[256];
        for (int i = 0; i < 256; i++) {
            charMap[i] = -1;
        }
        char[] string = s.toCharArray();
        int max = 0;
        int left = 0;
        for (int i = 0; i < string.length; i++) {
        	if (charMap[string[i]] != -1) {
        		if (charMap[string[i]] < left) {
        		} else {
        			left = charMap[string[i]] + 1;
        		}
        	}
        	charMap[string[i]] = i;
        	if (max < (i - left + 1))
        		max = (i - left + 1);
        }
        return max;
    }
    
    public static void main(String[] args) {
        LongestSubstringWORepeat service = new LongestSubstringWORepeat();
        System.out.println(service.lengthOfLongestSubstring("bbbbb")); // 1
        System.out.println(service.lengthOfLongestSubstring("abcabcdabc")); // 4
        System.out.println(service.lengthOfLongestSubstring("abpqbcdabc")); // 6
        System.out.println(service.lengthOfLongestSubstring("c")); // 1
        System.out.println(service.lengthOfLongestSubstring("ca")); // 2
        System.out.println(service.lengthOfLongestSubstring("")); // 0
    }
}