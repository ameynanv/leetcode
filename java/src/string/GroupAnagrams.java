package string;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class GroupAnagrams {
    public List<List<String>> groupAnagrams(String[] strs) {
        HashMap<String, List<String>> map = new HashMap<String, List<String>>();
        Arrays.sort(strs);
        for (int i = 0; i < strs.length; i++) {
            String key = sortedStr(strs[i]);
            List<String> list = map.containsKey(key) ? map.get(key) : 
                                                       new ArrayList<String>();
            list.add(strs[i]);
            map.put(key, list);
        }
        List<List<String>> result = new ArrayList<List<String>>();
        for (String key: map.keySet()) {
            result.add(map.get(key));
        }
        return result;
    }
    
    private String sortedStr(String str) {
        char[] c = str.toCharArray();
        Arrays.sort(c);
        return String.valueOf(c);
    }
    
	public static void main(String[] args) {
		GroupAnagrams service = new GroupAnagrams();
		String[] strs = {"eat", "tea", "tan", "ate", "nat", "bat"};
		System.out.println(service.groupAnagrams(strs));

	}

}
