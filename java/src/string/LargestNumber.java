package string;
import java.util.Arrays;
import java.util.Comparator;


public class LargestNumber {
    
	@SuppressWarnings("unused")
	private class MyComparator implements Comparator<Integer> {
		public int compare(Integer o1, Integer o2) {
			char[] c1 = o1.toString().toCharArray();
			char[] c2 = o2.toString().toCharArray();
			int rev = -1;
			int i = 0;
			while(i < c1.length && i < c2.length) { 
				if (c1[i] > c2[i]) return +1*rev;
				if (c1[i] < c2[i]) return -1*rev;
				i++;
			}
			if (i == c1.length && i == c2.length) return 0;
			
			if (i == c1.length) {
				while(i < c2.length) {
					if (c2[i] < c1[0]) return +1*rev;
					if (c2[i] > c1[0]) return -1*rev;
					i++;
				}
				int j = 0;
				while(j < c1.length - 1) {
					if (c1[j] < c1[j+1]) return +1*rev;
					if (c1[j] > c1[j+1]) return -1*rev;
					j++;
				}
				return 0;
			} else {
				//i == c2.length
				while(i < c1.length) {
					if (c1[i] < c2[0]) return -1*rev;
					if (c1[i] > c2[0]) return +1*rev;
					i++;
				}
				int j = 0;
				while(j < c2.length - 1) {
					if (c2[j] < c2[j+1]) return -1*rev;
					if (c2[j] > c2[j+1]) return +1*rev;
					j++;
				}
				return 0;
			}
		}
	}
    
	private class MyComparator2 implements Comparator<Integer> {
		public int compare(Integer o1, Integer o2) {
			String st12 = o1.toString() + o2.toString();
			String st21 = o2.toString() + o1.toString();
			return -1*st12.compareTo(st21);
		}
	}
	
    public String largestNumber(int[] nums) {
    	Integer[] numbers = new Integer[nums.length];
    	for (int i = 0; i < nums.length; i++)
    			numbers[i] = nums[i];
    	Arrays.sort(numbers, new MyComparator2());
    	StringBuffer buffer = new StringBuffer();
    	for (int i = 0; i < numbers.length; i++) {
    		System.out.print(numbers[i] + " ");
    		buffer.append(numbers[i] + "");
    	}
    	System.out.println(" ");
    	if (buffer.charAt(0) == '0') return "0";
        return buffer.toString();
    }
    
    
    public static void main(String[] args) {
    	LargestNumber service = new LargestNumber();
    	int[] nums = {0,0};
    	System.out.println(service.largestNumber(nums));
    	int[] nums2 = {12,121};
    	System.out.println(service.largestNumber(nums2));
    }
}