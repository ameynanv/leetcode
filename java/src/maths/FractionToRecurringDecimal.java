package maths;

import java.util.HashMap;

public class FractionToRecurringDecimal {

    public String fractionToDecimal(int numerator, int denominator) {
        if (denominator == 0) return "NaN";
        if (numerator == 0) return "0";
        
        boolean negativeNum = ((numerator < 0) ^ (denominator < 0));
        long lnum = Math.abs((long)numerator);
        long ldenom = Math.abs((long)denominator);
        
        HashMap<Long, Integer> map = new HashMap<Long, Integer>();
        StringBuffer buffer = new StringBuffer();
        if (negativeNum) buffer.append("-");
        buffer.append(lnum/ldenom);
        lnum %= ldenom;
        if (lnum == 0) 
        	return buffer.toString();
        
        
        buffer.append(".");
        map.put(lnum, buffer.length());
        while(lnum != 0) {
        	lnum *= 10;
        	buffer.append(lnum/ldenom);
        	lnum %= ldenom;
        	
        	if (!map.containsKey(lnum)) {
        		map.put(lnum, buffer.length());
        	} else {
        		int startIndex = map.get(lnum);
        		buffer.insert(startIndex, "(");
        		buffer.append(")");
        		break;
        	}
        }
        return buffer.toString();
    }
    
	public static void main(String[] args) {
		FractionToRecurringDecimal service = new FractionToRecurringDecimal();
		System.out.println(service.fractionToDecimal(44, 3));
		System.out.println(service.fractionToDecimal(50, -8));
		System.out.println(service.fractionToDecimal(4, 2));
		System.out.println(service.fractionToDecimal(-50, -8));
		System.out.println(service.fractionToDecimal(-50, 8));
		System.out.println(service.fractionToDecimal(0, -8));
		System.out.println(service.fractionToDecimal(1, 6));
		System.out.println(service.fractionToDecimal(-1, -2147483648)); // "0.0000000004656612873077392578125"
	}
	

}
