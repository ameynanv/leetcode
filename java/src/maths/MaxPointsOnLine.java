package maths;

import java.util.HashMap;
import java.util.Map;

// https://leetcode.com/problems/max-points-on-a-line/
public class MaxPointsOnLine {
    
    public int maxPoints(Point[] points) {
        if (points.length < 1) return 0;
        if (points.length == 1) return 1;
        int maxPoints = 0;
        for (int i = 0; i < points.length-1; i++) {
            Point p1 = points[i];
            HashMap<Line, Integer> map = new HashMap<Line, Integer>();
            for (int j = i+1; j < points.length; j++) {
                Point p2 = points[j];
                Line l;
                // find equation of line
                if (p1.x == p2.x && p1.y != p2.y) {
                    l = new Line(0,0,0);
                    if (map.containsKey(l)) {
                        map.put(l, map.get(l) + 1);
                    } else {
                        map.put(l, 2);
                    }
                } else if (p1.x != p2.x || p1.y != p2.y) {
                    int x1,x2,y1,y2;
                    if (p1.x > p2.x) {
                        x1 = p1.x; y1 = p1.y;
                        x2 = p2.x; y2 = p2.y;
                    } else {
                        x1 = p2.x; y1 = p2.y;
                        x2 = p1.x; y2 = p1.y;
                    }
                    int xDiff = x1 - x2;
                    int yDiff = y1 - y2;
                    int g = gcd(yDiff, xDiff);
                    double C = p1.y - (yDiff)*1.0*p1.x/(xDiff*1.0);
                    l = new Line(xDiff/g, yDiff/g, C);
                    if (map.containsKey(l)) {
                        map.put(l, map.get(l) + 1);
                    } else {
                        map.put(l, 2);
                    }
                }
            }
            for (int j = i+1; j < points.length; j++) {
                Point p2 = points[j];
                if (p1.x == p2.x && p1.y == p2.y) {
                    if (map.keySet().isEmpty()) {
                        map.put(new Line(0,0,0), 1);
                    }
                    for (Line l: map.keySet()) {
                        map.put(l, map.get(l) + 1);
                    } 
                }
            }
            for (Map.Entry<Line, Integer> entry: map.entrySet()) {
                if (maxPoints < entry.getValue())
                    maxPoints = entry.getValue();
            }
            System.out.println(map);
        }
        return maxPoints;
    }
    
    private int gcd(int a, int b) {
        if (b == 0) return a;
        else return gcd(b, a%b);
    }
    
    private class Line {
        int xDiff;
        int yDiff;
        double C;
        public Line(int xDiff, int yDiff, double C) {
            this.xDiff = xDiff;
            this.yDiff = yDiff;
            this.C = C;
        }
        
        public String toString() {
            return "(X:" + xDiff + ",Y:" + yDiff +  ",C:" + C + ")";
        }
        // TODO
        
        public boolean equals(Object o) {
            if (o instanceof Line) {
                Line otherLine = (Line)o;
                if (otherLine.xDiff != this.xDiff) return false;
                if (otherLine.yDiff != this.yDiff) return false;
                if (otherLine.C  != this.C) return false;
                return true;
            } else {
                return false;
            }
            
        }
        
        public int hashCode() {
            return xDiff + 31 * yDiff + 31 * 31 * Double.valueOf(C).hashCode();
        }
    }
    
    public static void main(String[] args) {
        //[[0,0],[1,1],[1,-1]]
        Point p1 = new Point(0,0);
        Point p2 = new Point(1,0);
        //Point p3 = new Point(1,-1);
        //Point p3 = new Point(3,3);
        //Point p4 = new Point(5,4);
        //Point p5 = new Point(-5,-4);
        //Point p6 = new Point(-10,-8);
        //Point[] points = {p1, p2, p3, p4, p5, p6};
        Point[] points = {p1, p2};
        MaxPointsOnLine service = new MaxPointsOnLine();
        System.out.println(service.maxPoints(points));
        
    }
}



class Point {
    int x;
    int y;
    Point() { x = 0; y = 0; }
    Point(int a, int b) { x = a; y = b; }
}