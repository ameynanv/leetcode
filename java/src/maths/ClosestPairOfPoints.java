package maths;
import java.util.Arrays;
import java.util.Comparator;

public class ClosestPairOfPoints {

	public double minDistance(Point[] points) {
		Arrays.sort(points, new XYComparator());
		return minDistance(points, 0, points.length-1);
	}
	
	private double minDistance(Point[] points, int start, int end) {
		if (end <= start) return Integer.MAX_VALUE;
		int mid = (start + end) >> 1;
		double leftMinDistance = minDistance(points, start, mid);
		double rightMinDistance = minDistance(points, mid+1, end);
		double minDistance = Math.min(leftMinDistance, rightMinDistance);
		
		int left = mid;
		int right = mid;
		while(left > start && distance(points[left], points[mid]) < minDistance)
			left--;
		while(right < end && distance(points[right], points[mid]) < minDistance)
			right++;
		
		Point[] dup = new Point[right-left+1];
		for (int i = left; i <= right; i++) {
			dup[i-left] = new Point(points[i].x, points[i].y);
		}
		Arrays.sort(dup, new YXComparator());
		for (int i = 0; i < dup.length; i++) {
			for (int j = 1; j < 7; j++) {
				if (i+j < dup.length) {
					double d = distance(points[i], points[i+j]);
					minDistance = Math.min(minDistance, d);
				}
			}
		}
		
		return minDistance;
	}
	
	private double distance(Point p1, Point p2) {
		return Math.sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y));
	}
	
	private class XYComparator implements Comparator<Point> {
		public int compare(Point o1, Point o2) {
			if (o1.x < o2.x) return -1;
			if (o1.x > o2.x) return +1;
			
			// o1.x == o2.x
			if (o1.y < o2.y) return -1;
			if (o1.y > o2.y) return +1;
			return 0;
		}
	}
	
	private class YXComparator implements Comparator<Point> {
		public int compare(Point o1, Point o2) {
			if (o1.y < o2.y) return -1;
			if (o1.y > o2.y) return +1;
			
			// o1.x == o2.x
			if (o1.x < o2.x) return -1;
			if (o1.x > o2.x) return +1;

			return 0;
		}
	}
	
	public static void main(String[] args) {
		ClosestPairOfPoints service = new ClosestPairOfPoints();
		Point[] points = {new Point(2,3),new Point(12,30),new Point(40,50),new Point(5,1),new Point(12,10), new Point(3,4)};
		System.out.println(service.minDistance(points));
	}
	
}
