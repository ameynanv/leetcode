package maths;

public class AddDigits {
    public int addDigits(int num) {
        return (num - 9 * ((num - 1) / 9));
    }
    
    public static void main(String[] args) {
        AddDigits service = new AddDigits();
        System.out.println(service.addDigits(38));
    }
}