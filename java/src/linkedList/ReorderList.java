package linkedList;

public class ReorderList {
    public void reorderList(ListNode head) {
        if (head == null || head.next == null) return;
        // Find middle Element
        ListNode slow = head;
        ListNode fast = head;
        while (fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        
        // break the list and revese list form middle to end
        ListNode middle = slow.next;
        slow.next = null;
        ListNode revHead = reverseList(middle);
        
        // insert node from rev list between two nodes of org list
        ListNode node = head;
        ListNode rev = revHead;
        while(node != null && rev != null) {
            ListNode tmp = node.next;
            node.next = rev;
            rev = rev.next;
            node.next.next = tmp;
            node = node.next.next;
        }
    }
    
    private ListNode reverseList(ListNode node) {
        if (node.next == null) return node;
        ListNode prev = node;
        ListNode curr = node.next;
        prev.next = null;
        while(curr != null) {
            ListNode temp = curr.next;
            curr.next = prev;
            prev = curr;
            curr = temp;
        }
        return prev;
    }
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
