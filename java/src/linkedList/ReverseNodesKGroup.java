package linkedList;


/**
 * Given a linked list, reverse the nodes of a linked list k at a time and
 * return its modified list.
 * 
 * If the number of nodes is not a multiple of k then left-out nodes in the end
 * should remain as it is.
 * For example,
 * Given this linked list: 1->2->3->4->5
 * For k = 2, you should return: 2->1->4->3->5
 * For k = 3, you should return: 3->2->1->4->5
 */
public class ReverseNodesKGroup {

	public ListNode reverseKGroup(ListNode head, int k) {
		if (k < 1) return head;
		
		ListNode dummy = new ListNode(0);
		ListNode previous = dummy;
		ListNode temp = head;
		while (temp != null) {
			ListNode h = temp;
			int i = 0;
			for ( ; i < k - 1 && temp != null; i++) {
				temp = temp.next;
			}
			if (i == (k - 1) && temp != null) {
				ListNode temp2 = temp.next;
				temp.next = null;
				ListNode[] rList = reverseLinkedList(h);
				previous.next = rList[0];
				previous = rList[1];
				temp = temp2;
			} else {
				previous.next = h;
			}
		}
		return dummy.next;
	}
	
	// Returns [head, tail] of reversed LinkedList
	public ListNode[] reverseLinkedList(ListNode head) {
		ListNode[] result = new ListNode[2];
		if (head.next == null) {
			result[0] = head;
			result[1] = head;
			return result;
		}
		
		ListNode prev = head;
		ListNode current = head.next;
		prev.next = null;
		while(current != null) {
			ListNode temp = current.next;
			current.next = prev;
			prev = current;
			current = temp;
		}
		result[0] = prev;
		result[1] = head;
		return result;
	}
	
	private void printList(ListNode head) {
		ListNode t = head;
		while (t != null) {
			System.out.print(t.val + " ");
			t = t.next;
		}
		System.out.println("");
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		head.next =  new ListNode(2);
		head.next.next =  new ListNode(3);
		head.next.next.next =  new ListNode(4);
		head.next.next.next.next =  new ListNode(5);
		
		ReverseNodesKGroup service = new ReverseNodesKGroup();
		ListNode rev = service.reverseKGroup(null, 6);
		
		service.printList(rev);
	}
}
