package linkedList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class PeekingIteratorTest {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);list.add(2);list.add(3);list.add(4);
		Iterator<Integer> it = list.iterator();
		PeekingIterator pit = new PeekingIterator(it);
		System.out.println(pit.hasNext());
		System.out.println(pit.peek() + " " + pit.next() + " " + pit.peek());
	}
}

class PeekingIterator implements Iterator<Integer> {

	private Integer next;
    private Iterator<Integer> it;
	public PeekingIterator(Iterator<Integer> iterator) {
		if(iterator == null) return;
		
	    // initialize any member here.
	    it = iterator;
	    next = it.hasNext() ? it.next() : null;
	}

    // Returns the next element in the iteration without advancing the iterator.
	public Integer peek() {
		if (next == null) throw new NoSuchElementException();
        return next;
	}

	// hasNext() and next() should behave the same as in the Iterator interface.
	// Override them if needed.
	@Override
	public Integer next() {
	    if (next == null) throw new NoSuchElementException();
	    Integer result = next;
	    next = it.hasNext() ? it.next() : null;
	    return result;
	}

	@Override
	public boolean hasNext() {
	    return next != null;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}
}