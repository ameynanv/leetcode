package linkedList;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * http://www.careercup.com/question?id=16759664
 *
 * You have k lists of sorted integers. Find the smallest range that includes at
 * least one number from each of the k lists.
 * 
 * For example, List 1: [4, 10, 15, 24, 26] List 2: [0, 9, 12, 20] List 3: [5,
 * 18, 22, 30]
 * 
 * The smallest range here would be [20, 24] as it contains 24 from list 1, 20
 * from list 2, and 22 from list 3.
 */
public class SmallestRangeForKLists {

	public int[] smallestRange(ListNode[] lists) {
		int[] range = new int[2];
		PriorityQueue<ListNode> minPQ = new PriorityQueue<ListNode>(
				lists.length, new Comparator<ListNode>() {
					public int compare(ListNode p1, ListNode p2) {
						if (p1.val < p2.val)
							return -1;
						if (p1.val > p2.val)
							return 1;
						else
							return 0;
					}
				});

		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < lists.length; i++) {
			ListNode n = lists[i];
			if (n.val > max)
				max = n.val;
			if (n.val < min)
				min = n.val;
			minPQ.add(n);
		}
		range[0] = min;
		range[1] = max;

		int[] minRange = new int[2];
		int minRangeVal = Integer.MAX_VALUE;
		while (incrementMin(range, minPQ)) {
			if ((range[1] - range[0]) < minRangeVal) {
				minRange[0] = range[0];
				minRange[1] = range[1];
				minRangeVal = range[1] - range[0];
			}
		}
		return minRange;
	}

	private boolean incrementMin(int[] range, PriorityQueue<ListNode> minPQ) {
		ListNode minNode = minPQ.remove();
		ArrayList<ListNode> addBackList = new ArrayList<ListNode>();
		while (minNode.next == null && !minPQ.isEmpty()) {
			addBackList.add(minNode);
			if (!minPQ.isEmpty())
				minNode = minPQ.remove();
		}
		if (minNode.next == null) {
			return false;
		} else {
			minNode = minNode.next;
			if (minNode.val > range[1])
				range[1] = minNode.val;
			minPQ.add(minNode);
		}
		for (ListNode node : addBackList)
			minPQ.add(node);

		range[0] = minPQ.peek().val;
		return true;
	}

	public static void main(String[] args) {
		// List 1: [4, 10, 15, 24, 26] List 2: [0, 9, 12, 20]
		// List 3: [5, 18, 22, 30]
		ListNode[] lists = new ListNode[3];
		lists[0] = new ListNode(4);
		lists[0].next = new ListNode(10);
		lists[0].next.next = new ListNode(15);
		lists[0].next.next.next = new ListNode(24);
		lists[0].next.next.next.next = new ListNode(26);

		lists[1] = new ListNode(0);
		lists[1].next = new ListNode(9);
		lists[1].next.next = new ListNode(12);
		lists[1].next.next.next = new ListNode(20);

		lists[2] = new ListNode(5);
		lists[2].next = new ListNode(18);
		lists[2].next.next = new ListNode(22);
		lists[2].next.next.next = new ListNode(30);
		
		SmallestRangeForKLists service = new SmallestRangeForKLists();
		int[] result = service.smallestRange(lists);
		System.out.println("(" + result[0] +"," + result[1] + ")");

	}
}
