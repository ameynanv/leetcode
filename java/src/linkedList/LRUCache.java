package linkedList;
import java.util.HashMap;

public class LRUCache {
	
	class DLinkNode {
		int key;
		int value;
		DLinkNode next;
		DLinkNode prev;
		
		public DLinkNode(int k, int v) {
			this.key = k; 
			this.value = v;
			this.next = null;
			this.prev = null;
		}
	}
	
	HashMap<Integer, DLinkNode> cache;
	int capacity;
	DLinkNode head;
	DLinkNode tail;
	
	public LRUCache(int capacity) {
		cache = new HashMap<Integer, DLinkNode>();
		this.capacity = capacity;
		head = new DLinkNode(-1,-1); // dummy node
		tail = new DLinkNode(-1,-1); //dummy node
		head.next = tail;
		tail.prev = head;
	}

	public int get(int key) {
		DLinkNode result = cache.get(key);
		if (result == null) return -1;
		else {
			moveToHead(result);
			return result.value;
		}
	}

	public void set(int key, int value) {
		DLinkNode result = cache.get(key);
		if (result == null) {
			if (cache.size() <= capacity - 1) {
				addNode(new DLinkNode(key, value));
			} else {
				removeTailNode();
				addNode(new DLinkNode(key, value));
			}
		} else {
			moveToHead(result);
			result.value = value;
		}
	}

	private void moveToHead(DLinkNode node) {
		DLinkNode prev = node.prev;
		DLinkNode next = node.next;
		prev.next = next;
		next.prev = prev;
		DLinkNode temp = head.next;
		head.next = node;
		node.prev = head;
		node.next = temp;
		temp.prev = node;
	}
	
	private void removeTailNode() {
		if (cache.size() > 0) {
			DLinkNode toBeDel = tail.prev;
			cache.remove(toBeDel.key);
			DLinkNode prev = toBeDel.prev;
			prev.next = tail;
			tail.prev = prev;
			toBeDel.next = null;
			toBeDel.prev = null;
			
		}
	}
	
	private void addNode(DLinkNode node) {
		cache.put(node.key, node);
		DLinkNode temp = head.next;
		head.next = node;
		node.prev = head;
		node.next = temp;
		temp.prev = node;
	}
	
	public static void main(String[] args) {
		LRUCache cache = new LRUCache(2);
		cache.set(1, 1);
		cache.set(2, 2);
		System.out.println(cache.get(1));
		System.out.println(cache.get(2));
		cache.set(3, 3);
		System.out.println(cache.get(3));
		System.out.println(cache.get(1));
	}

}
