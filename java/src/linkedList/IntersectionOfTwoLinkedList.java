package linkedList;

public class IntersectionOfTwoLinkedList {
	
	public ListNode getIntersection(ListNode head1, ListNode head2) {
		int c1 = size(head1);
		int c2 = size(head2);
		ListNode t1 = head1;
		ListNode t2 = head2;
		
		while (c1 != c2) {
			if (c1 > c2) {
				t1 = t1.next;
				c1--;
			}
			else {
				t2 = t2.next;
				c2--;
			}
		}
		while (t1 != t2) {
			t1 = t1.next;
			t2 = t2.next;
		}
		return t1;
	}
	
	// gives the size of the "non cyclic" list
	private int size(ListNode head) {
		int count = 0;
		ListNode t = head;
		while(t != null) {
			count++;
			t = t.next;
		}
		return count;
	}
	
}
