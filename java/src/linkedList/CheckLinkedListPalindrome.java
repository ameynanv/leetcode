package linkedList;

// Check if a single linkedlist is a palindrome or not
public class CheckLinkedListPalindrome {

	// 1) Find midpoint of the linked list.
	// 2) Reverse the right half of the linked list
	// 3) Iterate through the two halfs to check if linked list is palidrome or not
	public boolean isPalindrome(ListNode head) {
		ListNode mid = findMidPoint(head);
		ListNode head2 = reverseLinkedList(mid);
		
		ListNode t1 = head;
		ListNode t2 = head2;
		while(t2 != null) {
			if (t2.val != t1.val) return false;
			t1 = t1.next;
			t2 = t2.next;
		}
		return true;
	}
	
	private ListNode findMidPoint(ListNode head) {
		ListNode slow = head;
		ListNode fast = head;
		boolean midpointReached = false;
		
		while(!midpointReached) {
			if (fast.next == null) midpointReached = true;
			else if (fast.next != null && fast.next.next == null) midpointReached = true;
			else {
				slow = slow.next;
				fast = fast.next.next;
			}
		}
		return slow.next;
	}
	
	private ListNode reverseLinkedList(ListNode head) {
		ListNode prev = null;
		ListNode current = head;
		ListNode next = current.next;
		while (current != null) {
			next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}
		return prev;
	}

}
