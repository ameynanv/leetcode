package linkedList;

/**
 *  https://leetcode.com/problems/palindrome-linked-list/
 *  Use O(n) time and O(1) space
 *  Approach: Find middle and reverse second half. Compare with 1st
 * 
 */
 public class PalindromeLinkedList {
    
    public boolean isPalindrome(ListNode head) {
        if (head == null) return true;
        if (head.next == null) return true;
        if (head.next.next == null) {
            if (head.next.val == head.val) return true;
            return false;
        }
        
        // First find middle element
        ListNode slow = head;
        ListNode fast = head;
        while(fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        
        // slow represent the middle element;
        // now reverse the linked list from slow.next till end
        ListNode list2 = slow.next;
        slow.next = null;
        ListNode rev = reverseLinkedList(list2);
        
        // Compare orinal list and rev list 
        ListNode list1 = head;
        while (rev != null) {
            if (rev.val != list1.val) return false;
            rev = rev.next;
            list1 = list1.next;
        }
        return true;
    }
    
    private ListNode reverseLinkedList(ListNode head) {
        if (head.next == null) return head;
        ListNode prev = head;
        ListNode curr = head.next;
        prev.next = null;
        while(curr != null) {
            ListNode next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }
        return prev;
    }
    
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(1);
        PalindromeLinkedList service = new PalindromeLinkedList();
        System.out.println(service.isPalindrome(head));
    }
}