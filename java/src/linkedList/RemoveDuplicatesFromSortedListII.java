package linkedList;

public class RemoveDuplicatesFromSortedListII {
	
	public ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode dummy = new ListNode(-1);
        ListNode w = dummy;
        ListNode u = head;
        ListNode r = head.next;
        while(r != null) {
            if (u.val != r.val) {
                w.next = u;
                w = w.next;
                u = r;
                r = r.next;
                w.next = null;
            } else {
                int t = u.val;
                u = null;
                while(r != null && r.val == t) {
                    r = r.next;
                }
                if (r != null) {
                    u = r;
                    r = r.next;
                }
            }
        }
        if (u != null) {
            w.next = u;
        }
        return dummy.next;
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
