package linkedList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class MergeKSortedLists {
	private int K;
	private PriorityQueue<ListNode> pq; 
	
    public ListNode mergeKLists(ListNode[] lists) {
    	ListNode resultHead = null;
    	ListNode resultNext = null;
    	
    	K = lists.length;
    	if (K == 0) return null;
    	
    	pq = new PriorityQueue<ListNode>(1, new Comparator<ListNode>() {
    		@Override
    		public int compare(ListNode o1, ListNode o2) {
    			if (o1.val < o2.val) return -1;
    			if (o1.val == o2.val) return 0;
    			if (o1.val > o2.val) return  1;
    			return 0;
    		}
    	});
    	
    	for (int i = 0; i < K; i++) {
    		if (lists[i] != null)
    			pq.add(lists[i]);
    	}
    	
    	while(!pq.isEmpty()) {
    		ListNode minNode = pq.remove();
    		ListNode nextNodeInList = minNode.next;
    		minNode.next = null;
    		if (nextNodeInList != null) {
    			pq.add(nextNodeInList);
    		}
    		if (resultHead == null) {
    			resultHead = minNode;
    			resultNext = minNode;
    		} else {
    			resultNext.next = minNode;
    			resultNext = resultNext.next; 
    		}
    	}
        return resultHead;
    }
}
