package linkedList;

public class CopyListWithRandomPointer {
    public RandomListNode copyRandomList(RandomListNode head) {
    	RandomListNode result = null;
    	// Iterate Over RandomListNode's next pointers
    	// and place the cloned node between himself and random node.
    	RandomListNode node = head;
    	while (node != null) {
    		RandomListNode cloneNode = new RandomListNode(node.label);
    		if (result == null)
    			result = cloneNode;
    		cloneNode.next = node.next;
    		node.next = cloneNode;
    		node = node.next.next;
    	}
    	
    	node = head;
    	while (node != null) {
    		RandomListNode cloneNode = node.next;
    		cloneNode.random = node.random != null ? node.random.next : null;
    		node = node.next.next;
    	}    	
    	node = head;
    	RandomListNode copyNode = result;
    	while (node != null) {
    		node.next = node.next != null ? node.next.next : null;
    		copyNode.next = copyNode.next != null ? copyNode.next.next : null;
    		node = node.next;
    		copyNode = copyNode.next;
    	}
    	
        return result;
    }
    
    public void printRandomNodeList(RandomListNode head) {
    	RandomListNode n = head;
     	while(n != null) {
     		System.out.print("(" + n.label + "," + (n.random != null ? n.random.label : "#") + "," + (n.next != null ? n.next.label : "#") + ")");
     		n = n.next;
     	}
     	System.out.println("");
    }
    
    public static void main(String[] args) {
    	RandomListNode head = new RandomListNode(1);
    	head.next = new RandomListNode(2);
    	head.next.next = new RandomListNode(3);
    	head.next.next.next = new RandomListNode(4);
    	
    	head.random = null;
    	head.next.random = head.next.next;
    	head.next.next.random = head.next.next;
    	head.next.next.next.random = head.next;
    	
    	CopyListWithRandomPointer service = new CopyListWithRandomPointer();
    	RandomListNode clone = service.copyRandomList(head);
    	RandomListNode n = head;
    	while(n != null) {
    		n.label = n.label * -1;
    		n = n.next;
    	}
    	
    	service.printRandomNodeList(head);
    	service.printRandomNodeList(clone);
    }
	
}

class RandomListNode {
	int label;
	RandomListNode next, random;

	RandomListNode(int x) {
		this.label = x;
	}
};
