package linkedList;

public class AddTwoNumbers {
	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode result = new ListNode(-1); // dummyNode
        int val1, val2, carry = 0;
        ListNode head = result;
        while (l1 != null || l2 != null) {
            val1 = (l1 == null) ? 0 : l1.val;
            val2 = (l2 == null) ? 0 : l2.val;
            int sum = val1+val2+carry;
            head.next = new ListNode(sum%10);
            carry = sum / 10;
            if (l1 != null) l1 = l1.next;
            if (l2 != null) l2 = l2.next;
            head = head.next;
        }
        if (carry != 0) {
            head.next = new ListNode(carry);
        }
        return result.next;
    }
}
