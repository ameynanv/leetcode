package graph;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;
public class BreadthFirstPaths {

	private boolean[] marked;
	private int[] edgeTo;
	
	public BreadthFirstPaths(Graph G, int s) {
		marked = new boolean[G.V()];
		edgeTo = new int[G.V()];
		edgeTo[s] = s;
		marked[s] = true;
		bfs(G, s);
	}
	
	private void bfs(Graph G, int s) {
		Queue<Integer> queue = new ArrayDeque<Integer>();
		queue.add(s);
		while(!queue.isEmpty()) {
			int n = queue.remove();
			for (Integer i: G.adj(n)) {
				if (!marked[i]) {
					edgeTo[i] = n;
					marked[i] = true;
					queue.add(i);
				}
			}
		}
	}

	// is there a path from s to v?
	public boolean hasPathTo(int v) {
		return marked[v];
	}

	// path from s to v; null if no such path
	public Iterable<Integer> pathTo(int v) {
		if (!marked[v]) return null;
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(v);
		while(edgeTo[v] != v) {
			stack.push(edgeTo[v]);
			v = edgeTo[v];
		}
		return stack;
	}
}
