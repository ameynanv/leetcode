package graph;

import java.util.ArrayList;
import java.util.List;

//
// http://www.geeksforgeeks.org/given-array-strings-find-strings-can-chained-form-circle/
//
public class ChainStrings {

    public boolean isLoopPossible(List<String> list) {
    	Digraph DG = new Digraph(128);
    	for (String str: list) {
    		int firstChar = str.charAt(0);
    		int lastChar = str.charAt(str.length() - 1);
    		DG.addEdge(firstChar, lastChar);
    	}
    	
    	
    	// Check if every node has same in and out degree
    	Digraph RDG = DG.reverse();
    	for (int i = 0; i < DG.V(); i++) {
    		if (DG.degree(i) != RDG.degree(i)) return false;
    	}
    	
    	// Check that all the non zero components belong to same SCC.
    	KosarajuSCC kscc = new KosarajuSCC(DG);
    	int id = -1;
    	for (int i = 0; i < DG.V(); i++) {
    		if (DG.degree(i) != 0) {
    			if (id == -1) id = kscc.id(i);
    			else if (kscc.id(i) != id) return false;
    		}
    	}
        return true;
    }
    
    public static void main(String[] args) {
    	List<String> list = new ArrayList<String>();
    	list.add("ab");
    	list.add("ba");
    	list.add("ab");
    	list.add("ba");
    	ChainStrings service = new ChainStrings();
    	System.out.println(service.isLoopPossible(list));
    }
}