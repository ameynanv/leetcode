package graph;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Comparator;

public class KruskalMST {
    
    private ArrayList<Edge> mst;
    
    public KruskalMST(EdgeWeightedGraph G) {
        mst = new ArrayList<Edge>();
        PriorityQueue<Edge> minPQ = new PriorityQueue<Edge>(G.E(), new Comparator<Edge>() {
            public int compare(Edge e1, Edge e2) {
                if (e1.weight() < e2.weight()) return -1;
                if (e1.weight() > e2.weight()) return +1;
                else return 0;
            }
        });
        
        for (Edge e: G.edges()) {
            minPQ.add(e);
        }
        
        UnionFind uf = new UnionFind(G.V());
        
        while(!minPQ.isEmpty() && mst.size() < G.V()) {
            Edge e = minPQ.remove();
            int v = e.either();
            int w = e.other(v);
            if (uf.connected(v, w)) continue;
            uf.union(v,w);
            mst.add(e);
        }
    }
    
    
    
     // all of the MST edges
    public Iterable<Edge> edges() {
        return mst;
    }
    
     // weight of MST
    public double weight() {
        double weight = 0.0;
        for (Edge e: mst)
            weight += e.weight();
        return weight;
    }
    
    public static void main(String[] args) {
        EdgeWeightedGraph G = new EdgeWeightedGraph(8);
        G.addEdge(new Edge(4,5,.35));
        G.addEdge(new Edge(4,7,.37));
        G.addEdge(new Edge(5,7,.28));
        G.addEdge(new Edge(0,7,.16));
        G.addEdge(new Edge(1,5,.32));
        G.addEdge(new Edge(0,4,.38));
        G.addEdge(new Edge(2,3,.17));
        G.addEdge(new Edge(1,7,.19));
        G.addEdge(new Edge(0,2,.26));
        G.addEdge(new Edge(1,2,.36));
        G.addEdge(new Edge(1,3,.29));
        G.addEdge(new Edge(2,7,.34));
        G.addEdge(new Edge(6,2,.40));
        G.addEdge(new Edge(3,6,.52));
        G.addEdge(new Edge(6,0,.58));
        G.addEdge(new Edge(6,4,.93));
        KruskalMST mst = new KruskalMST(G);
        System.out.println(mst.edges());
        System.out.println(mst.weight());
    }
}