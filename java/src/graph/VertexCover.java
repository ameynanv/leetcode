package graph;

import java.util.HashSet;

/**
 * http://www.geeksforgeeks.org/vertex-cover-problem-set-1-introduction-approximate-algorithm-2/
 * 
 * Given an undirected graph find its approximate vertex cover
 */
public class VertexCover {
	
	public HashSet<Integer> vertexCover(Graph G) {
		HashSet<Integer> set = new HashSet<Integer>();
		for (int v = 0; v < G.V(); v++) {
			if (set.contains(v)) continue;
			for (Integer e: G.adj(v)) {
				if (!set.contains(e) && !set.contains(v)) {
					set.add(e);
					set.add(v);
				}
			}
				
		}
		return set;
	}
	
	public static void main(String[] args) {
		Graph G = new Graph(7);
	    G.addEdge(0, 1);
	    G.addEdge(0, 2);
	    G.addEdge(1, 3);
	    G.addEdge(3, 4);
	    G.addEdge(4, 5);
	    G.addEdge(5, 6);
	    
	    VertexCover service = new VertexCover();
	    System.out.println(service.vertexCover(G));
	}
}
