package graph;

public class WordSearch {
	public boolean exist(char[][] board, String word) {
		int H = board.length;
		if (H == 0)
			return false;
		int W = board[0].length;
		if (W == 0)
			return false;
		char[] target = word.toCharArray();
		for (int r = 1; r < H; r++) {
			for (int c = 1; c < W; c++) {
				if (board[r][c] == target[0]) {
					boolean[][] visited = new boolean[H][W];
					boolean result = dfs(board, visited, target, r, c, 0);
					if (result)
						return true;
				}
			}
		}
		return false;
	}

	private boolean dfs(char[][] board, boolean[][] visited, char[] target,
			int x, int y, int matchCount) {
		int H = board.length;
		int W = board[0].length;
		if (x < 0 || x >= H)
			return false;
		if (y < 0 || y >= W)
			return false;
		if (visited[x][y])
			return false;
		if (target[matchCount] != board[x][y])
			return false;
		if (matchCount + 1 == target.length)
			return true;
		visited[x][y] = true;
		boolean result = dfs(board, visited, target, x + 1, y, matchCount + 1)
				|| dfs(board, visited, target, x - 1, y, matchCount + 1)
				|| dfs(board, visited, target, x, y + 1, matchCount + 1)
				|| dfs(board, visited, target, x, y - 1, matchCount + 1);
		visited[x][y] = false;
		return result;
	}

	public static void main(String[] args) {
		WordSearch service = new WordSearch();
		char[][] board = {{'C','E'},{'C','S'},{'E','E'}};
		String word = "SEE";
		System.out.println(service.exist(board, word));
	}

}
