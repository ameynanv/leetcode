package graph;
import java.util.ArrayList;
/**
 * http://www.geeksforgeeks.org/eulerian-path-and-circuit/
 * 
 * Eulerian Path is a path in graph that visits every edge exactly once.
 * Eulerian Circuit is an Eulerian Path which starts and ends on the same
 * vertex.
 *
 * Test for Eulerian Cycle: 
 * a) All vertices with non-zero degree are connected. We dont care about 
 * vertices with zero degree because they dont belong to Eulerian Cycle 
 * or Path (we only consider all edges). 
 * b) All vertices have even degree.
 *  
 * Test for Eulerian Path:
 * a) All vertices with non-zero degree are connected. We dont care about 
 * vertices with zero degree because they dont belong to Eulerian Cycle 
 * or Path (we only consider all edges). 
 * b) Either zero or two vertices have odd degree rest everyone has even degree 
 * 
 */
public class EulerianGraph {

	boolean eulerianCycle = false;
	boolean eulerianPath = false;
	
	public EulerianGraph(Graph G) {
		if (!isConnected(G)) return;
		int oddVertex = 0;
		for(int i= 0; i < G.V(); i++) {
			ArrayList<Integer> adj = new ArrayList<Integer>();
			for (Integer v: G.adj(i)) 
				adj.add(v);
			if (adj.size() == 0) continue;
			if (adj.size() % 2 != 0) oddVertex++;
		}
		if (oddVertex == 0) eulerianCycle = true;
		if (oddVertex == 0 || oddVertex == 2) eulerianPath = true;
	}
	
	private boolean isConnected(Graph G) {
		boolean[] visited = new boolean[G.V()];
		int i = 0;
		ArrayList<Integer> adj = new ArrayList<Integer>();
		for (Integer v: G.adj(i)) 
			adj.add(v);
		while (i < G.V() && adj.size() == 0)
			i++;
		if (i == G.V()) return true;
		dfs(G, i, visited);
		for (i = 0; i < G.V(); i++)
			if (adj.size() != 0 && !visited[i]) return false;
		return true;
	}
	
	private void dfs(Graph G, int source, boolean[] visited) {
		visited[source] = true;
		for (Integer v: G.adj(source)) {
			if(!visited[v]) {
				dfs(G, v, visited);
			}
		}
	}
	
	public boolean isEulerianPath() {
		return eulerianPath;
	}
	
	public boolean isEulerianCycle() {
		return eulerianCycle;
	}
	
	public static void main(String[] args) {
		Graph G = new Graph(5);
		G.addEdge(0,1);
		G.addEdge(0,2);
		G.addEdge(0,3);
		G.addEdge(1,2);
		G.addEdge(3,4);
		EulerianGraph service = new EulerianGraph(G);
		System.out.println(service.isEulerianPath());
		System.out.println(service.isEulerianCycle());
		G.addEdge(0,4);
		service = new EulerianGraph(G);
		System.out.println(service.isEulerianPath());
		System.out.println(service.isEulerianCycle());
		
		G = new Graph(5);
		G.addEdge(0,1);
		G.addEdge(0,2);
		G.addEdge(0,3);
		G.addEdge(1,2);
		G.addEdge(3,4);
		G.addEdge(1,3);
		service = new EulerianGraph(G);
		System.out.println(service.isEulerianPath());
		System.out.println(service.isEulerianCycle());
		
	}

}
