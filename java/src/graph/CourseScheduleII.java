package graph;

import java.util.ArrayList;
import java.util.ArrayDeque;

public class CourseScheduleII {

    @SuppressWarnings("unchecked")
	public int[] findOrder(int numCourses, int[][] prerequisites) {
		ArrayList<Integer>[] dep = new ArrayList[numCourses];
		for (int i = 0; i < numCourses; i++)
			dep[i] = new ArrayList<Integer>();
		for (int i = 0; i < prerequisites.length; i++) {
			dep[prerequisites[i][0]].add(prerequisites[i][1]);
		}

        boolean cycle = detectCycle(numCourses, dep);
        if (!cycle) return new int[0];
        return topologicalOrder(numCourses, dep);
    }
    
    private boolean detectCycle(int numCourses, ArrayList<Integer>[] dep) {
    	boolean result = true;
		boolean[] visited = new boolean[numCourses];
		boolean[] onStack = new boolean[numCourses];
		for (int i = 0; i < numCourses; i++) {
			if (!visited[i]) {
				result = result && dfs(i, visited, onStack, dep);
				if (!result) return false;
			}
		}
		return result;	
    }
    
	private boolean dfs(int course, boolean[] visited, boolean[] onStack, ArrayList<Integer>[] dep) {
		boolean result = true;
		visited[course] = true;
		onStack[course] = true;
		for (Integer i: dep[course]) {
			if (!visited[i]) {
				result = result && dfs(i, visited, onStack, dep);
			} else {
				if (onStack[i]) {
					result = false;
					onStack[course]= false;
					return result;
				}
			}
		}
		onStack[course] = false;
		return result;
	}
	
	private int[] topologicalOrder(int numCourses, ArrayList<Integer>[] dep) {
		ArrayDeque<Integer> stack = new ArrayDeque<Integer>();
		boolean[] visited = new boolean[numCourses];
		for (int i = 0; i < numCourses; i++) {
			if (!visited[i]) {
				dfs(i, visited, stack, dep);
			}
		}
		
		int[] result = new int[stack.size()];
		for (int i = 0; i < result.length; i++)
			result[i] = stack.remove();
		return result;
	}
	
	private void dfs(int course, boolean[] visited, ArrayDeque<Integer> stack, ArrayList<Integer>[] dep) {
		visited[course] = true;
		for (Integer i: dep[course]) {
			if (!visited[i])
				dfs(i, visited, stack, dep);
		}
		System.out.println("Course " + course + " DEP " + dep[course]);
		stack.add(course);
	}

    
    
	public static void main(String[] args) {
		CourseScheduleII service = new CourseScheduleII();
		int numCourses = 7;
		int[][] prereq = new int[8][2];
		prereq[0][0] = 0; prereq[0][1] = 3;
		prereq[1][0] = 1; prereq[1][1] = 2;
		prereq[2][0] = 1; prereq[2][1] = 3;
		prereq[3][0] = 2; prereq[3][1] = 3;
		prereq[4][0] = 2; prereq[4][1] = 4;
		prereq[5][0] = 5; prereq[5][1] = 3;
		prereq[6][0] = 5; prereq[6][1] = 6;
		prereq[7][0] = 6; prereq[7][1] = 4;
		int[] order = service.findOrder(numCourses, prereq);
		for (int i = 0; i < order.length; i++)
			System.out.print(order[i] + " ");
		System.out.println("");
	}

}
