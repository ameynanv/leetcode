package graph;

import java.util.ArrayDeque;
import java.util.Queue;

public class BreadthFirstSearch {

	private boolean[] marked;
	private int count;
	
	public BreadthFirstSearch(Graph G, int s) {
		marked = new boolean[G.V()];
		count = 0;
		bfs(G, s);
	}
	
	private void bfs(Graph G, int n) {
		Queue<Integer> queue = new ArrayDeque<Integer>();
		queue.add(n);
		marked[n] = true;
		while(!queue.isEmpty()) {
			int k = queue.remove();
			count++;
			for (Integer i: G.adj(k)) {
				if (!marked[i]) {
					marked[i] = true;
					queue.add(i);
				}
			}
		}
	}

	// is v connected to s?
	public boolean marked(int w) {
		return marked[w];
	}

	// How many vertices are connected to s?
	public int count() {
		return count;
	}

}
