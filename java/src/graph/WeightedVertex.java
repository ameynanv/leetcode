package graph;

public class WeightedVertex implements Comparable<WeightedVertex>{
	private final int val;
    private double weight;
    public WeightedVertex(int val, double weight) {
        this.val = val;
        this.weight = weight;
    }
    
    public int val() {
        return val;
    }
    
    public double weight() {
        return val;
    }
    
    public boolean equals(Object that) {
    	if (that instanceof WeightedVertex) {
            WeightedVertex o = (WeightedVertex) that;
            return (this.val == o.val);
    	} else {
    		return false;
    	}
    }
    
    public int hashCode() {
    	return (31 * (31 + val));
    }

	@Override
	public int compareTo(WeightedVertex o) {
		if (this.weight < o.weight) return -1;
		if (this.weight > o.weight) return +1;
		return 0;
	}
}
