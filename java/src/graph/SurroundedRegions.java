package graph;

public class SurroundedRegions {
	int H;
	int W;

	public void solve(char[][] board) {
		H = board.length;
		if (H == 0)
			return;
		W = board[0].length;
		if (W == 0)
			return;
		UnionFind uf = new UnionFind(H * W + 1);
		for (int r = 0; r < H; r++) {
			for (int c = 0; c < W; c++) {
				connectToNeighbours(r,c,board,uf);
			}
		}
		
		for (int r = 0; r < H; r++) {
			for (int c = 0; c < W; c++) {
				if (!uf.connected(index(r,c), H*W))
					board[r][c] = 'X';
			}
		}
	}

	private void connectToNeighbours(int r, int c, char[][] grid, UnionFind uf) {
		if (r > 0 && grid[r - 1][c] == grid[r][c]) {
			uf.union(index(r, c), index(r - 1, c));
		}
		if (r < H - 1 && grid[r + 1][c] == grid[r][c]) {
			uf.union(index(r, c), index(r + 1, c));
		}
		if (c > 0 && grid[r][c - 1] == grid[r][c]) {
			uf.union(index(r, c), index(r, c - 1));
		}
		if (c < W - 1 && grid[r][c + 1] == grid[r][c]) {
			uf.union(index(r, c), index(r, c + 1));
		}
		
		if (r == 0 || r == H -1 || c == 0 || c == W - 1) {
			if (grid[r][c] == 'O') {
				uf.union(index(r, c), H*W);	
			}
		}
	}

	private int index(int r, int c) {
		return r * W + c;
	}

    public static void main(String[] args) {
    	SurroundedRegions service = new SurroundedRegions();
    	char[][] grid = {{'X','X','X','X'},
    			         {'X','O','O','X'},
    			         {'X','X','O','X'},
    			         {'X','O','X','X'}
    			        };
    	service.solve(grid);
    	for (int r = 0; r < 4; r++) {
			for (int c = 0; c < 4; c++) {
				System.out.print(grid[r][c]);
			}
			System.out.println("");
		}
    }
}
