package graph;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.ArrayList;

public class PrimMST {

    private PriorityQueue<WeightedVertex> minPQ;
    private Edge[] edgeTo;
    private double[] distTo;
    private boolean[] marked;
    
    public PrimMST(EdgeWeightedGraph G) {
        minPQ = new PriorityQueue<WeightedVertex>(G.V(), new Comparator<WeightedVertex>() {
                public int compare(WeightedVertex o1, WeightedVertex o2) {
                    return o1.compareTo(o2);
                }
            }
        );
        edgeTo = new Edge[G.V()];
        distTo = new double[G.V()];
        marked = new boolean[G.V()];
        
        for (int i = 0; i < G.V(); i++)
            distTo[i] = Double.POSITIVE_INFINITY;
        distTo[0] = 0;
        minPQ.add(new WeightedVertex(0, 0.0));
        
        while(!minPQ.isEmpty()) {
            visit(G, (minPQ.remove()).val());
        }
    }
    
    private void visit(EdgeWeightedGraph G, int v) {
        marked[v] = true;
        for (Edge e : G.adj(v))
        {
            int w = e.other(v);
            if (marked[w]) continue; // v-w is ineligible.
            if (e.weight() < distTo[w]) { 
                // Edge e is new best connection from tree to w.
                edgeTo[w] = e; 
                distTo[w] = e.weight(); 
                if (minPQ.contains(new WeightedVertex(w, distTo[w]))) {
                    minPQ.remove(new WeightedVertex(w, distTo[w]));
                    minPQ.add(new WeightedVertex(w, distTo[w]));
                } else {
                    minPQ.add(new WeightedVertex(w, distTo[w]));
                }
            }
        }
    }

    // all of the MST edges
    public Iterable<Edge> edges() {
        ArrayList<Edge> edges = new ArrayList<Edge>();
        for (int i = 1; i < edgeTo.length ; i++)
            edges.add(edgeTo[i]);
        return edges;
    }

    // weight of MST
    public double weight() {
        double weight = 0.0;
        for (int i = 1; i < edgeTo.length ; i++)
            weight += edgeTo[i].weight();
        return weight;
    }
    
    public static void main(String[] args) {

        EdgeWeightedGraph G = new EdgeWeightedGraph(8);
        G.addEdge(new Edge(4,5,.35));
        G.addEdge(new Edge(4,7,.37));
        G.addEdge(new Edge(5,7,.28));
        G.addEdge(new Edge(0,7,.16));
        G.addEdge(new Edge(1,5,.32));
        G.addEdge(new Edge(0,4,.38));
        G.addEdge(new Edge(2,3,.17));
        G.addEdge(new Edge(1,7,.19));
        G.addEdge(new Edge(0,2,.26));
        G.addEdge(new Edge(1,2,.36));
        G.addEdge(new Edge(1,3,.29));
        G.addEdge(new Edge(2,7,.34));
        G.addEdge(new Edge(6,2,.40));
        G.addEdge(new Edge(3,6,.52));
        G.addEdge(new Edge(6,0,.58));
        G.addEdge(new Edge(6,4,.93));
        PrimMST mst = new PrimMST(G);
        System.out.println(mst.edges());
        System.out.println(mst.weight());
        
    }
}