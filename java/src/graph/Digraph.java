package graph;

import java.util.ArrayList;

public class Digraph {
	
	private final int V;
	private int E;
	private ArrayList<Integer>[] adj;
	
	// create a V-vertex digraph with no edges
	@SuppressWarnings("unchecked")
	public Digraph(int V) {
		this.V = V;
		E = 0;
		adj = (ArrayList<Integer>[]) new ArrayList[V];
		for (int i = 0; i < V; i++) {
			adj[i] = new ArrayList<Integer>();
		}
	}

	// number of vertices
	public int V() {
		return V;
	}

	// number of edges
	public int E() {
		return E;
	}

	// add edge v->w to this digraph
	public void addEdge(int v, int w) {
		adj[v].add(w);
		E++;
	}

	// vertices connected to v by edges pointing from v
	public Iterable<Integer> adj(int v) {
		return adj[v];
	}
	
	// out degree of vertex v
	public int degree(int v) {
		return adj[v].size();
	}

	// reverse of this digraph
	public Digraph reverse() {
		Digraph rGraph = new Digraph(V);
		for (int i = 0; i < V; i++) {
			for (Integer k: adj(i))
				rGraph.addEdge(k, i);
		}
		return rGraph;
	}

	// string representation
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < V; i++) {
			builder.append(i + " :\t");
			builder.append(adj[i]);
			builder.append("\n");
		}
		return builder.toString();
		
	}
}
