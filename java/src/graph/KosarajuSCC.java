package graph;

/**
 *	Algorithm Steps: 
 *  1) Reverse the Digraph DG => RDG
 *  2) Find Topological order (Reverse Post) of RDG
 *  3) Run DFS on DG for every element according to Step 2 order 
 *  4) Every component is SCC.
 *
 */
public class KosarajuSCC {

	private boolean[] marked;
	private int[] id;
	private int count = 0;
	
	public KosarajuSCC(Digraph G) {
		marked = new boolean[G.V()];
		id = new int[G.V()];
		Digraph RDF = G.reverse();
		DepthFirstOrder order = new DepthFirstOrder(RDF);
		for (Integer s: order.reversePost()) {
			if (!marked[s]) {
				dfs(G, s);
				count++;
			}
		}
			
	}
	
	private void dfs(Digraph G, int s) {
		marked[s] = true;
		id[s] = count;
		for (Integer v: G.adj(s)) {
			if (!marked[v])
				dfs(G,v);
		}
	}

	// are v and w strongly connected?
	public boolean stronglyConnected(int v, int w) {
		return id[v] == id[w];
	}

	// component identifier for v (between 0 and count()-1)
	public int id(int v) {
		return id[v];
	}

	// number of strong components
	public int count() {
		return count;
	}
}
