package graph;

import java.util.HashSet;

public class FindTriangles {

    public int findNumberOfTriangles(Graph G) {
        int count = 0;
        for (int v = 0; v < G.V(); v++) {
            HashSet<Integer> set = new HashSet<Integer>();
            for (Integer i : G.adj(v)) {
                set.add(i);
            }
            for (Integer i: G.adj(v)) {
                for (Integer j: G.adj(i)) {
                    if (set.contains(j)) {
                        count++;  
                    }
                }
            }
        }
        return count / 6;
    }
    
    public static void main(String[] args) {
        Graph G = new Graph(13);
		G.addEdge(0, 5);
		G.addEdge(0, 1);
		G.addEdge(0, 2);
		G.addEdge(0, 6);
		G.addEdge(3, 5);
		G.addEdge(3, 4);
		G.addEdge(4, 5);
		G.addEdge(4, 6);
		G.addEdge(7, 8);
		G.addEdge(9, 10);
		G.addEdge(9, 11);
		G.addEdge(9, 12);
		G.addEdge(11, 12);
		
		FindTriangles service = new FindTriangles();
		System.out.println(service.findNumberOfTriangles(G));
    }
}