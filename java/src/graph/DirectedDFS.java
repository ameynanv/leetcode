package graph;

public class DirectedDFS {

	private boolean[] marked;

	// find vertices in G that are reachable from s
	public DirectedDFS(Digraph G, int s) {
		marked = new boolean[G.V()];
		marked[s] = true;
		dfs(G, s);
	}

	// find vertices in G that are reachable from sources
	public DirectedDFS(Digraph G, Iterable<Integer> sources) {
		marked = new boolean[G.V()];
		for (Integer i: sources) {
			if (!marked[i]) {
				marked[i] = true;
				dfs(G, i);	
			}
		}
	}

	// is v reachable?
	public boolean marked(int v) {
		return marked[v];
	}

	private void dfs(Digraph G, int n) {
		for (Integer k : G.adj(n)) {
			if (!marked[k]) {
				marked[k] = true;
				dfs(G, k);
			}
		}
	}
}
