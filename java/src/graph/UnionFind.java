package graph;

public class UnionFind {
	
	private int[] id;
	private int[] size;
	private int count;
	
	// initialize N sites with integer names (0 to N-1)
	public UnionFind(int N) {
		id = new int[N];
		size = new int[N];
		count = N;
		for (int i = 0; i < N; i++) {
			id[i] = i;
			size[i] = 1;
		}
		
	}

	// add connection between p and q
	public void union(int p, int q) {
		int rootP = find(p);
		int rootQ = find(q);
		if (rootP == rootQ) return;
		
		if (size[rootP] < size[rootQ]) {
			id[rootP] = rootQ;
			size[rootQ] += size[rootP];
		} else {
			id[rootQ] = rootP;
			size[rootP] += size[rootQ];
		}
		count--;
	}

	// component identifier for p (0 to N-1)
	public int find(int p) {
		while(id[p] != p) {
			id[p] = id[id[p]];
			p = id[p];
		}
		return p;
	}

	// return true if p and q are in the same component
	public boolean connected(int p, int q) {
		return find(p) == find(q);
	}

	// number of components
	public int count() {
		return count;
	}
}
