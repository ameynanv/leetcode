package graph;

import java.util.ArrayDeque;

public class DepthFirstOrder {

	private ArrayDeque<Integer> preOrder;
	private ArrayDeque<Integer> postOrder;
	private ArrayDeque<Integer> reversePostOrder;

	private boolean[] marked;

	public DepthFirstOrder(Digraph G) {
		preOrder = new ArrayDeque<Integer>();
		postOrder = new ArrayDeque<Integer>();
		reversePostOrder = new ArrayDeque<Integer>();

		marked = new boolean[G.V()];
		for (int s = 0; s < G.V(); s++)
			if (!marked[s]) {
				marked[s] = true;
				dfs(G, s);
			}
	}

	private void dfs(Digraph G, int s) {
		preOrder.add(s);
		for (Integer k: G.adj(s)) {
			if (!marked[k]) {
				marked[k] = true;
				dfs(G,k);
			}
		}
		postOrder.add(s);
		reversePostOrder.push(s);
	}

	public Iterable<Integer> pre() {
		return preOrder;
	}

	public Iterable<Integer> post() {
		return postOrder;
	}

	public Iterable<Integer> reversePost() {
		return reversePostOrder;
	}
}
