package graph;

import java.util.Stack;

public class DirectedCycle {

	private boolean[] marked;
	private boolean[] onStack;
	private int[] edgeTo;
	private boolean cycle;
	private Stack<Integer> cycleTrace;

	// cycle-finding constructor
	public DirectedCycle(Digraph G) {
		marked = new boolean[G.V()];
		onStack = new boolean[G.V()];
		edgeTo = new int[G.V()];

		for (int s = 0; s < G.V(); s++) {
			if (!marked[s]) {
				marked[s] = true;
				dfs(G, s);
			}
		}
	}

	private void dfs(Digraph G, int n) {
		onStack[n] = true;
		for (Integer i : G.adj(n)) {
			if (cycle)
				return;
			if (!marked[i]) {
				edgeTo[i] = n;
				marked[i] = true;
				dfs(G, i);
			} else if (onStack[i]) {
				cycle = true;
				cycleTrace = new Stack<Integer>();
				int k = n;
				while (k != i) {
					cycleTrace.push(k);
					k = edgeTo[k];
				}
				cycleTrace.push(i);
				cycleTrace.push(n);
			}
		}
		onStack[n] = false;
	}

	// does G have a directed cycle?
	public boolean hasCycle() {
		return cycle;
	}

	// vertices on a cycle (if one exists)
	public Iterable<Integer> cycle() {
		return cycleTrace;
	}
}
