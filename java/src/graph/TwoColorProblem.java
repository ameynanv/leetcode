package graph;

import java.util.ArrayDeque;

public class TwoColorProblem {

	private boolean[] marked;
	private boolean[] color;
	private boolean result = true;
	
	public TwoColorProblem(Graph G) {
		marked = new boolean[G.V()];
		color = new boolean[G.V()];
		for (int i = 0; i < G.V(); i++) {
			if (!marked[i]) {
				bfs(G, i);
				if (!result) break;
			}
		}
	}
	
	private void bfs(Graph G, int s) {
		ArrayDeque<Integer> queue = new ArrayDeque<Integer>();
		queue.add(s);
		marked[s] = true;
		while (!queue.isEmpty()) {
			s = queue.remove();
			for (Integer i: G.adj(s)) {
				if (!result) return;
				if (!marked[i]) {
					marked[i] = true;
					color[i] = !color[s];	
					queue.add(i);
				} else if (color[i] == color[s]){
					result = false;
				}
			}
		}
	}
	
	public boolean isBipartite() {
		return result;
	}

}
