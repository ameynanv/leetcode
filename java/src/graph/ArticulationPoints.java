package graph;
import java.util.ArrayList;

public class ArticulationPoints {
	private boolean[] visited;
	private int[] disc;
	private int[] low;
	private int[] parent;
	private boolean[] ap;
	private int time;
	
	public ArticulationPoints(Graph G) {
		visited = new boolean[G.V()];
		disc = new int[G.V()];
		low = new int[G.V()];
		parent = new int[G.V()];
		ap = new boolean[G.V()];
		time = 0;
		
		for (int i = 0; i < G.V(); i++)
			if (!visited[i]) {
				dfs(G, i);
				parent[i] = i;
			}
	}
	
	public ArrayList<Integer> getArticulationPoints() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < ap.length; i++)
			if(ap[i]) list.add(i);
		return list;
	}
	
	private void dfs(Graph G, int s) {
		visited[s] = true;
		disc[s] = time++;
		low[s] = disc[s];
		int childCount = 0;
		for (Integer i: G.adj(s)) {
			parent[i] = s;
			childCount++;
			if (!visited[i]) {
				dfs(G, i);
				low[s] = Math.min(low[s], low[i]);
				
				// s can be articulation point in 2 cases
				// 1) s is root and has more than two independent children
				if (parent[s] == s && childCount > 1) {
					ap[s] = true;
				}
				// 2) s is not root and disc[s] <= low[i]
				if (parent[s] != s && disc[s] <= low[i]) {
					ap[s] = true;
				}
			} else if (parent[s] != i) {
				low[s] = Math.min(low[s], disc[i]);
			}
		}
	}
	
	
	
	public static void main(String[] args) {
		Graph G = new Graph(5);
		G.addEdge(3,4);
		G.addEdge(3,0);
		G.addEdge(0,1);
		G.addEdge(0,2);
		G.addEdge(2,1);
		ArticulationPoints ap = new ArticulationPoints(G);
		System.out.println(ap.getArticulationPoints());
	}
}
