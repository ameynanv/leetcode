package graph;

public class NumberOfIslands {
	int H;
	int W;
	
    public int numIslands(char[][] grid) {
    	H = grid.length;
    	W = grid[0].length;
    	UnionFind uf = new UnionFind(H*W + 1);
    	for (int r = 0; r < H; r++) {
    		for (int c = 0; c < W; c++) {
    			if (grid[r][c] == '1') {
    				connectToNeighbours(r,c,grid,uf);
    			} else {
    				uf.union(index(r,c), H*W);
    			}
    		}
    	}
    	return uf.count() - 1;
    }
    
    private void connectToNeighbours(int r, int c, char[][] grid, UnionFind uf) {
    	if (r > 0 && grid[r-1][c] == '1') {
    		uf.union(index(r,c), index(r-1,c));
    	}
    	if (r < H-1 && grid[r+1][c] == '1') {
    		uf.union(index(r,c), index(r+1,c));
    	}
    	if (c > 0 && grid[r][c-1] == '1') {
    		uf.union(index(r,c), index(r,c-1));
    	}
    	if (c < W-1 && grid[r][c+1] == '1') {
    		uf.union(index(r,c), index(r,c+1));
    	}
    }
    
    private int index(int r, int c) {
    	return r*W+c;
    }
    
    class UnionFind {
    	
    	private int[] id;
    	private int[] size;
    	private int count;
    	
    	// initialize N sites with integer names (0 to N-1)
    	public UnionFind(int N) {
    		id = new int[N];
    		size = new int[N];
    		count = N;
    		for (int i = 0; i < N; i++) {
    			id[i] = i;
    			size[i] = 1;
    		}
    		
    	}

    	// add connection between p and q
    	public void union(int p, int q) {
    		int rootP = find(p);
    		int rootQ = find(q);
    		if (rootP == rootQ) return;
    		
    		if (size[rootP] < size[rootQ]) {
    			id[rootP] = rootQ;
    			size[rootQ] += size[rootP];
    		} else {
    			id[rootQ] = rootP;
    			size[rootP] += size[rootQ];
    		}
    		count--;
    	}

    	// component identifier for p (0 to N-1)
    	public int find(int p) {
    		while(id[p] != p) {
    			id[p] = id[id[p]];
    			p = id[p];
    		}
    		return p;
    	}

    	// return true if p and q are in the same component
    	public boolean connected(int p, int q) {
    		return find(p) == find(q);
    	}

    	// number of components
    	public int count() {
    		return count;
    	}
    }
    
    
    public static void main(String[] args) {
    	NumberOfIslands service = new NumberOfIslands();
    	char[][] grid = {{'1','1','0','0','0'},
    			         {'1','1','0','0','0'},
    			         {'0','0','1','0','0'},
    			         {'0','0','0','1','1'}
    			        };
    	System.out.println(service.numIslands(grid));
    }
}
