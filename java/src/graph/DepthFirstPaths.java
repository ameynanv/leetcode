package graph;

import java.util.Stack;

public class DepthFirstPaths {

	private boolean[] marked;
	private int[] edgeTo;

	// find paths in G from source s
	public DepthFirstPaths(Graph G, int s) {
		marked = new boolean[G.V()];
		edgeTo = new int[G.V()];
		edgeTo[s] = s;
		marked[s] = true;
		dfs(G, s);
	}

	private void dfs(Graph G, int n) {
		for (Integer i : G.adj(n)) {
			if (!marked[i]) {
				marked[i] = true;
				edgeTo[i] = n;
				dfs(G, i);
			}
		}
	}

	// is there a path from s to v?
	public boolean hasPathTo(int v) {
		return marked[v];
	}

	// path from s to v; null if no such path
	public Iterable<Integer> pathTo(int v) {
		if (!marked[v])
			return null;
		Stack<Integer> stack = new Stack<Integer>();
		int num = v;
		stack.push(num);
		while (edgeTo[num] != num) {
			num = edgeTo[num];
			stack.push(num);
		}
		return stack;
	}
}
