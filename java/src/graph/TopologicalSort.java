package graph;

public class TopologicalSort {
	private Iterable<Integer> order;
	private boolean isDAG = true;
	
	public TopologicalSort(Digraph G) {
		DirectedCycle dfsCycle = new DirectedCycle(G);
		if(dfsCycle.hasCycle()) isDAG = false;
		DepthFirstOrder dfOrder = new DepthFirstOrder(G);
		order = dfOrder.reversePost();
	}
	public Iterable<Integer> order() {
		if (!isDAG) return null;
		return order;
	}

	public boolean isDAG() {
		return isDAG;
	}
}
