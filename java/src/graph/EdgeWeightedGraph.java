package graph;

import java.util.ArrayList;

public class EdgeWeightedGraph {
    
    private final int V;
    private int E;
    private ArrayList<Edge>[] adj;
    
    @SuppressWarnings("unchecked")
	public EdgeWeightedGraph(int V) {
        this.V = V;
        this.E = 0;
        adj = (ArrayList<Edge>[]) new ArrayList[V];
		for (int v = 0; v < V; v++) 	 // Initialize all lists
			adj[v] = new ArrayList<Edge>(); // to empty.
    }
    
    public int V() { 
        return V; 
    }

    public int E() { 
        return E; 
    }

    public void addEdge(Edge e)
    {
        int v = e.either(), w = e.other(v);
        adj[v].add(e);
        adj[w].add(e);
        E++;
    }

    public Iterable<Edge> adj(int v) { 
        return adj[v]; 
    }

    // Spit out all edges, excluding self loops
    public Iterable<Edge> edges() 
    {
        ArrayList<Edge> b = new ArrayList<Edge>();
        for (int v = 0; v < V; v++) {
            for (Edge e : adj[v]) {
                if (e.other(v) > v) b.add(e);
            }
        }
        return b;
    }
}