package graph;

import java.util.ArrayList;

public class EdgeWeightedDigraph {
	
	private final int V; // number of vertices
	private int E; // number of edges
	private ArrayList<DirectedEdge>[] adj; // adjacency lists

	@SuppressWarnings("unchecked")
	public EdgeWeightedDigraph(int V) {
		this.V = V;
		this.E = 0;
		adj = (ArrayList<DirectedEdge>[]) new ArrayList[V];
		for (int v = 0; v < V; v++)
			adj[v] = new ArrayList<DirectedEdge>();
	}

	public int V() {
		return V;
	}

	public int E() {
		return E;
	}

	public void addEdge(DirectedEdge e) {
		adj[e.from()].add(e);
		E++;
	}

	public Iterable<DirectedEdge> adj(int v) {
		return adj[v];
	}

	public Iterable<DirectedEdge> edges() {
		ArrayList<DirectedEdge> bag = new ArrayList<DirectedEdge>();
		for (int v = 0; v < V; v++)
			for (DirectedEdge e : adj[v])
				bag.add(e);
		return bag;
	}
}
