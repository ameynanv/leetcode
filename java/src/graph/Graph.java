package graph;

import java.util.ArrayList;

// Undirected Graph
public class Graph {

	private final int V;
	private int E;
	private ArrayList<Integer>[] adj;

	// create a V-vertex graph with no edges
	@SuppressWarnings("unchecked")
	public Graph(int V) {
		this.V = V;
		this.E = 0;
		adj = (ArrayList<Integer>[]) new ArrayList[V];
		for (int v = 0; v < V; v++) 	 // Initialize all lists
			adj[v] = new ArrayList<Integer>(); // to empty.
	}

	// number of vertices
	public int V() {
		return V;
	}

	// number of edges
	public int E() {
		return E;
	}

	// add edge v-w to this graph
	public void addEdge(int v, int w) {
		adj[v].add(w);
		adj[w].add(v);
		E++;
	}

	// vertices adjacent to v
	public Iterable<Integer> adj(int v) {
		return adj[v];
	}

	// string representation
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < V; i++) {
			builder.append( i + " :\t");
			builder.append( adj[i]);
			builder.append("\n");
		}
		return builder.toString();
	}
}
