package graph;
import java.util.ArrayList;

/**
 * https://leetcode.com/problems/course-schedule/
 * 
 * There are a total of n courses you have to take, labeled from 0 to n - 1.
 * 
 * Some courses may have prerequisites, for example to take course 0 you have to
 * first take course 1, which is expressed as a pair: [0,1]
 * 
 * Given the total number of courses and a list of prerequisite pairs, is it
 * possible for you to finish all courses?
 * 
 */
public class CourseSchedule {

	@SuppressWarnings("unchecked")
	public boolean canFinish(int numCourses, int[][] prerequisites) {
		boolean result = true;
		boolean[] visited = new boolean[numCourses];
		boolean[] onStack = new boolean[numCourses];
		ArrayList<Integer>[] dep = new ArrayList[numCourses];
		for (int i = 0; i < numCourses; i++)
			dep[i] = new ArrayList<Integer>();
		for (int i = 0; i < prerequisites.length; i++) {
			dep[prerequisites[i][0]].add(prerequisites[i][1]);
		}
		for (int i = 0; i < numCourses; i++) {
			if (!visited[i]) {
				result = result && dfs(i, visited, onStack, dep);
				if (!result) return false;
			}
		}
		return result;
	}
	
	private boolean dfs(int course, boolean[] visited, boolean[] onStack, ArrayList<Integer>[] dep) {
		boolean result = true;
		visited[course] = true;
		onStack[course] = true;
		for (Integer i: dep[course]) {
			if (!visited[i]) {
				result = result && dfs(i, visited, onStack, dep);
			} else {
				if (onStack[i]) {
					result = false;
					onStack[course]= false;
					return result;
				}
			}
		}
		onStack[course] = false;
		return result;
	}

	public static void main(String[] args) {
		CourseSchedule service = new CourseSchedule();
		int numCourses = 2;
		int[][] prereq = new int[0][0];
		System.out.println(service.canFinish(numCourses, prereq)); // true
		
		numCourses = 7;
		prereq = new int[8][2];
		prereq[0][0] = 0; prereq[0][1] = 3;
		prereq[1][0] = 1; prereq[1][1] = 2;
		prereq[2][0] = 1; prereq[2][1] = 3;
		prereq[3][0] = 2; prereq[3][1] = 3;
		prereq[4][0] = 2; prereq[4][1] = 4;
		prereq[5][0] = 5; prereq[5][1] = 3;
		prereq[6][0] = 5; prereq[6][1] = 6;
		prereq[7][0] = 6; prereq[7][1] = 4;
		System.out.println(service.canFinish(numCourses, prereq));

	}

}
