package graph;

public class DepthFirstSearch {

	private boolean[] marked;
	private int count;

	// find vertices connected to a source vertex s
	public DepthFirstSearch(Graph G, int s) {
		marked = new boolean[G.V()];
		count = 0;
		dfs(G, s);
	}

	private void dfs(Graph G, int v) {
		marked[v] = true;
		count++;
		for (Integer n : G.adj(v)) {
			if (!marked[n]) {
				dfs(G, n);
			}
		}
	}

	// is v connected to s?
	public boolean marked(int w) {
		return marked[w];
	}

	// How many vertices are connected to s?
	public int count() {
		return count;
	}
}
