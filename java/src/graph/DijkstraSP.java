package graph;

import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.ArrayDeque;

public class DijkstraSP {

	private DirectedEdge[] edgeTo;
	private double[] distTo;
	private PriorityQueue<WeightedVertex> minPQ;

	public DijkstraSP(EdgeWeightedDigraph G, int s) {
		edgeTo = new DirectedEdge[G.V()];
		distTo = new double[G.V()];
		minPQ = new PriorityQueue<WeightedVertex>(1,
				new Comparator<WeightedVertex>() {
					public int compare(WeightedVertex o1, WeightedVertex o2) {
						return o1.compareTo(o2);
					}
				});
		for (int v = 0; v < G.V(); v++)
			distTo[v] = Double.POSITIVE_INFINITY;
		distTo[s] = 0.0;

		minPQ.add(new WeightedVertex(s, 0.0));
		while (!minPQ.isEmpty())
			relax(G, minPQ.remove().val());

	}

	private void relax(EdgeWeightedDigraph G, int v) {
		for (DirectedEdge e : G.adj(v)) {
			int w = e.to();
			if (distTo[w] > distTo[v] + e.weight()) {
				distTo[w] = distTo[v] + e.weight();
				edgeTo[w] = e;
				if (minPQ.contains(new WeightedVertex(w, distTo[w]))) {
					minPQ.remove(w);
					minPQ.add(new WeightedVertex(w, distTo[w]));
				} else
					minPQ.add(new WeightedVertex(w, distTo[w]));
			}
		}
	}

	public double distTo(int v) {
		return distTo[v];
	}

	public boolean hasPathTo(int v) {
		return distTo[v] < Double.POSITIVE_INFINITY;
	}

	public Iterable<DirectedEdge> pathTo(int v) {
		if (!hasPathTo(v))
			return null;
		
		ArrayDeque<DirectedEdge> path = new ArrayDeque<DirectedEdge>();
		for (DirectedEdge e = edgeTo[v]; e != null; e = edgeTo[e.from()])
			path.push(e);
		return path;
	}

	public static void main(String[] args) {
		EdgeWeightedDigraph G = new EdgeWeightedDigraph(8);
        G.addEdge(new DirectedEdge(4,5,.35));
        G.addEdge(new DirectedEdge(5,3,.35));
        G.addEdge(new DirectedEdge(4,7,.37));
        G.addEdge(new DirectedEdge(5,7,.28));
        G.addEdge(new DirectedEdge(7,5,.28));
        G.addEdge(new DirectedEdge(5,1,.32));
        G.addEdge(new DirectedEdge(0,4,.38));
        G.addEdge(new DirectedEdge(0,2,.26));
        G.addEdge(new DirectedEdge(7,3,.39));
        G.addEdge(new DirectedEdge(1,3,.29));
        G.addEdge(new DirectedEdge(2,7,.34));
        G.addEdge(new DirectedEdge(6,2,.40));
        G.addEdge(new DirectedEdge(3,6,.52));
        G.addEdge(new DirectedEdge(6,0,.58));
        G.addEdge(new DirectedEdge(6,4,.93));

        DijkstraSP service = new DijkstraSP(G, 0);
        for (int t = 0; t < G.V(); t++)
        {
        	System.out.print("0" + " to " + t);
        	System.out.print(String.format(" (%4.2f): ", service.distTo(t)));
        	if (service.hasPathTo(t)) {
        		for (DirectedEdge e : service.pathTo(t)) {
        			System.out.print(e + " ");
        		}
        	}
        	System.out.println("");
        }
	}
}
