package concurrent;

public class ThreadLocalDemo {

	public static void main(String[] args) throws InterruptedException {
		ThreadLocalDemo_ThreadLocal obj = new ThreadLocalDemo_ThreadLocal();
		Thread t1 = new Thread(obj);
		Thread t2 = new Thread(obj);
		t1.start();
		t2.start();
		t1.join();
		t2.join();
	}

}

class ThreadLocalDemo_NotThreadLocal implements Runnable {
	private Integer i;
	
	public void run() {
		i = (int)(Math.random() * 100);
		try {
			Thread.sleep(1000);
		} catch(InterruptedException e) {
			
		}
		System.out.println(i);
	}
}

class ThreadLocalDemo_ThreadLocal implements Runnable {
	private ThreadLocal<Integer> i = new ThreadLocal<Integer>();
	
	public void run() {
		i.set((int)(Math.random() * 100));
		try {
			Thread.sleep(1000);
		} catch(InterruptedException e) {
			
		}
		System.out.println((Integer)i.get());
	}
}
