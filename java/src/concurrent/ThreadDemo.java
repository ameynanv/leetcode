package concurrent;

public class ThreadDemo {
	
	class MyThread extends Thread {
		
		public void run() {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(Thread.currentThread().getName());
		}
	}
	
	class MyRunnable implements Runnable {
		
		public void run() {
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(Thread.currentThread().getName());
		}
	}
	
	public void testThread(int num) {
		Thread t = new Thread(new MyThread(),"MyThread#" + num);
		t.start();
		Thread t2 = new Thread(new MyRunnable(),"MyRunnable#" + num);
		t2.start();
	}
	
	public static void main(String[] args) {
		System.out.println(Thread.currentThread().getName());
		ThreadDemo service = new ThreadDemo();
		for (int i = 0; i < 5; i++)
			service.testThread(i);
	}
}
