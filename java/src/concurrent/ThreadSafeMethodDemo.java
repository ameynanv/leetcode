package concurrent;

public class ThreadSafeMethodDemo {
	
	public static void main(String[] args) {
		AddService addService = new AddService();
		for (int i = 0; i < 10; i++) {
			Thread t = new Thread(new MyRunnable(0, addService));
			t.start();
		}
	}
}

class MyRunnable implements Runnable {
	private Integer i;
	private AddService service;
	
	public MyRunnable(Integer i, AddService service) {
		this.i = i;
		this.service = service;
	}
	
	public void run() {
		System.out.println(service.addInteger(i));
	}
}

class AddService {
	public Integer addInteger(Integer o1) {
		return o1+1;
	}
}
