package interview;
import java.util.List;
import java.util.HashMap;

// Given a list of tree nodes, in no particular order, write a method that finds 
// and returns the root of a binary tree. The method should return null, when the 
// list does not contain all the nodes in the binary tree.
// Example:
//         Input: BCAD
//         Output: A
//         Tree:
//                A
//              /    \
//            B      C
//              \
//               D
public class AmazonInterview {

	public class TreeNode {
	    int val;
	    TreeNode left;
	    TreeNode right;
	    public TreeNode(int val) {
	        this.val = val;
	    }
	    
	    public int hashCode() {
	        return val;
	    }
	}  
	public TreeNode findRootOfTree(List<TreeNode> list) {
	    if (list == null || list.size() == 0) return null;
	    boolean[] nodes = new boolean[list.size()];
	    HashMap<TreeNode, Integer> map = new HashMap<TreeNode, Integer>();
	    int k = 0;
	    for (TreeNode tn: list) {
	        map.put(tn, k++);
	    }
	    // map: B: 0
	    
	    for (TreeNode tn: list) {
	        if (tn.left != null) {
	            if (map.get(tn.left) == null) return null;
	            nodes[map.get(tn.left)] = true;
	        }
	        if (tn.right != null) {
	            if (map.get(tn.right) == null) return null;
	            nodes[map.get(tn.right)] = true;
	        }
	    }
	    // skip above for loop
	    // nodes[0] = false;
	    
	    int cnt = 0;
	    TreeNode result = null;
	    for (int i = 0; i < nodes.length; i++) {
	        if (!nodes[i]) {
	            result = list.get(i);
	            cnt++;
	        }
	    }
	    // cnt = 1, result = B
	    if (cnt != 1) return null;
	    return result;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
