package interview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.

 Given an unsorted array of numbers, find the highest product of any 3 numbers.
 There can be negative numbers and zeroes.

 */
public class AppleInterviewII {

	private class AbsoluteComp implements Comparator<Integer> {
		public int compare(Integer o1, Integer o2) {
			if (Math.abs(o1) > Math.abs(o2))
				return -1;
			if (Math.abs(o1) < Math.abs(o2))
				return 1;
			else
				return 0;
		}
	}

	public int findMax3Product(int[] nums) {
		// if (nums.length < 3) {
		// throw IllegalArgumentException("invalid inp");
		// }
		ArrayList<Integer> arr = new ArrayList<Integer>();
		boolean allNega = true;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] >= 0)
				allNega = false;
			arr.add(nums[i]);
		}

		Collections.sort(arr, new AbsoluteComp());
		System.out.println(arr);
		// nums = 4, 3, -2, 2, 0

		if (allNega) {
			nums[0] = arr.get(arr.size() - 1);
			nums[1] = arr.get(arr.size() - 2);
			nums[2] = arr.get(arr.size() - 3);
			return nums[0] * nums[1] * nums[2];
		} else {
			nums[0] = arr.get(0);
			nums[1] = arr.get(1);
			nums[2] = arr.get(2);
			nums[3] = arr.get(3);
		}

		if (nums[0] * nums[1] * nums[2] >= 0)
			return nums[0] * nums[1] * nums[2];
		else {
			// there can be either three neg numbers or only one nega number.
			// final ans will be combination of nums[0] .. nums[3]
			if (nums[0] * nums[1] * nums[3] >= 0)
				return nums[0] * nums[1] * nums[3];
			else if (nums[0] * nums[2] * nums[3] >= 0)
				return nums[0] * nums[2] * nums[3];
			else
				return nums[1] * nums[2] * nums[3];
		}
	}

	public static void main(String[] args) {
		// int[] nums = {-2,5,3,0,-4}; // 40
		// int[] nums = {-2,5,3,0,0}; // 0
		// int[] nums = {2,-5,3,4,0}; // 24
		// int[] nums = {2,-5,-3,-4,0}; // 40
		int[] nums = { -2, -5, -3, -4, -10 }; // -24 {-2,-3,-4,-5,-10}
		AppleInterviewII service = new AppleInterviewII();
		System.out.println(service.findMax3Product(nums));
	}

}
