package interview;
public class GoogleInterview {

	public int[] increment(int[] nums) throws IllegalArgumentException {
		if (nums.length == 0)
			throw new IllegalArgumentException("Input is empty");

		// handle the case where all digits are 9
		boolean isAll9 = true;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] != 9)
				isAll9 = false;
		}
		if (isAll9) {
			int[] result = new int[nums.length + 1];
			result[0] = 1;
			for (int i = 1; i < result.length; i++)
				result[i] = 0;
			return result;
		}

		// right to left increment the digit and remember to add carry
		int carry = 0;

		nums[nums.length - 1] = nums[nums.length - 1] + 1 + carry;
		if (nums[nums.length - 1] >= 10) {
			nums[nums.length - 1] = nums[nums.length - 1] - 10;
			carry = 1;
		}
		for (int i = nums.length - 2; i >= 0; i--) {
			nums[i] = nums[i] + carry;
			if (nums[i] >= 10) {
				nums[i] = nums[i] - 10;
				carry = 1;
			} else {
				carry = 0;
			}
		}
		return nums;
	}

	public static void main(String[] args) {
		int[] nums = { 0, 0, 1 };
		GoogleInterview gi = new GoogleInterview();
		int[] result = gi.increment(nums);
		for (int i = 0; i < nums.length; i++)
			System.out.print(nums[i] + " ");
		System.out.print("=>");
		for (int i = 0; i < result.length; i++)
			System.out.print(result[i] + " ");
	}
}
