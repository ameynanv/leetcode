package interview;

import java.util.ArrayDeque;

public class AppleInterview {

	public String giveMinPath(String longPath) {
		ArrayDeque<String> stack = new ArrayDeque<String>();
		String[] strings = longPath.split("/");
		for (int i = 0; i < strings.length; i++) {
			if (strings[i].equalsIgnoreCase("..")) {
				if (stack.isEmpty()) {
					throw new IllegalArgumentException("wrong path");
				}
				stack.pop();
			} else if (strings[i].equalsIgnoreCase(".")) {
				continue;
			} else {
				stack.push(strings[i]);
			}
		}
		StringBuilder builder = new StringBuilder();
		while (!stack.isEmpty()) {
			builder.append(stack.removeLast());
			builder.append("/");
		}
		return builder.toString();
	}

	public static void main(String[] args) {
		String longPath = "/usr/bin/../lib/mv";
		AppleInterview service = new AppleInterview();
		System.out.println(service.giveMinPath(longPath));
	}
}
