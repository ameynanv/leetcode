package puzzlers;


class Base1 {
	public static int i = 1;
	
	public void test() {
		System.out.println("Base1.test  " + i );
	}
	
	public void call() {
		test();
	}
}

class Child1 extends Base1 {
	
	public Child1() {
		i = 2;
	}
	
	public void test() {
		System.out.println("Child1.test " + (i + 1) );
	}
}

public class Main {
	public static void main(String[] args) {
		Base1 b = new Child1();
		b.call();
	}
}


