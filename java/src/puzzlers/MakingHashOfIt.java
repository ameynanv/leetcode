package puzzlers;

import java.util.Set;
import java.util.HashSet;
public class MakingHashOfIt {
	private String first, last;

	public MakingHashOfIt(String first, String last) {
		if (first == null || last == null)
			throw new NullPointerException();
		this.first = first;
		this.last = last;
	}

	public boolean equals(MakingHashOfIt o) {
		return first.equals(o.first) && last.equals(o.last);
	}

	public int hashCode() {
		return 31 * first.hashCode() + last.hashCode();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {
		Set s = new HashSet();
		s.add(new MakingHashOfIt("Mickey", "Mouse"));
		System.out.println(s.contains(new MakingHashOfIt("Mickey", "Mouse")));
	}
}
