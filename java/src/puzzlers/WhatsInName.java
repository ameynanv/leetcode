package puzzlers;

import java.util.HashSet;
import java.util.Set;

public class WhatsInName {
	private String first, last;

	public WhatsInName(String first, String last) {
		this.first = first;
		this.last = last;
	}

	public boolean equals(Object o) {
		if (!(o instanceof WhatsInName))
			return false;
		WhatsInName n = (WhatsInName) o;
		return n.first.equals(first) && n.last.equals(last);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {
		Set s = new HashSet();
		s.add(new WhatsInName("Donald", "Duck"));
		System.out.println(s.contains(new WhatsInName("Donald", "Duck")));
	}
}
