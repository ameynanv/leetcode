package puzzlers;
import java.util.Arrays;

public class PassByReference {

	public static void main(String[] args) {
		PassByReference service = new PassByReference();
		Integer k = 5;
		System.out.println("Before : " + k);
		System.out.println("Result " + service.addMe(k));
		System.out.println("After : " + k);
		MyInt m = new MyInt(5);
		service.addMe(m);
		System.out.println(m);
		
		int[] nums = {1,2,3};
		int[] numsOut = service.add1(nums);
		service.print(nums);
		service.print(numsOut);
		
	}
	
	public void print(int[] arr) {
		for(int i = 0; i < arr.length; i++)
			System.out.print(arr[i]);
		System.out.println("");
	}
	public int[] add1(int[] input) {
		int[] dup = Arrays.copyOf(input, input.length);
		for (int i = 0; i < dup.length; i++)
			dup[i]++;
		return dup;
	}
	
	
	public Integer addMe(Integer i) {
		int t = i;
		
		i = new Integer(t);
		return i;
	}
	
	public void addMe(MyInt i) {
		i.add1();
	}
	
	
	
}

class MyInt {
	
	private Integer i;
	
	public MyInt(int i) {
		this.i = i;
	}
	
	public void add1() {
		i = i + 1;
	}
	
	public String toString() {
		return i + "";
	}
	
}

