package puzzlers;

class Base {
	public String name = "Base";

	public void id() {
		System.out.println("Base id");
	}
}

@SuppressWarnings("unused")
class Derived extends Base {
	
	private String name = "Derived";

//	private void id() {
//		System.out.println("Base id");
//	}
}

/**
 * 
 * Private method can't override public, but private field can hide public
 *
 */
public class PrivateMatter {
	public static void main(String[] args) {
		// System.out.println(new Derived().name);
	}
}
