package puzzlers;

public class All_I_Get_Is_Static {

	@SuppressWarnings("static-access")
	public static void main(String args[]) {
		Dog woofer = new Dog();
		Dog nipper = new Basenji();
		woofer.bark();
		nipper.bark();
	}
}

class Dog {
	public static void bark() {
		System.out.print("woof ");
	}
}

class Basenji extends Dog {
	public static void bark() {
	}
}