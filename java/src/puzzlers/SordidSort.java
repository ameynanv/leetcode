package puzzlers;

import java.util.Arrays;
import java.util.Comparator;

public class SordidSort {
	public static void main(String args[]) {
		Integer big = new Integer(2000000000);
		Integer small = new Integer(-2000000000);
		Integer zero = new Integer(0);
		Integer[] arr = new Integer[] { big, small, zero };
		Arrays.sort(arr, new Comparator<Integer>() {
			public int compare(Integer o1, Integer o2) {
				return (o2).intValue() - (o1).intValue();
			}
		});
		System.out.println(Arrays.asList(arr));
	}
}
