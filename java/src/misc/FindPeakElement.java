package misc;

public class FindPeakElement {
    public int findPeakElement(int[] nums) {
        return findPeak(0, nums.length-1, nums);
    }
    
    private int findPeak(int startIndex, int endIndex, int[] nums) {
        if (startIndex >= endIndex) return endIndex;
        if ((endIndex - startIndex) == 1) {
            if (nums[endIndex] > nums[startIndex]) return endIndex;
            return startIndex;
        }
        int mid = (startIndex + endIndex)/2;
        if ((nums[mid - 1] < nums[mid] && nums[mid] > nums[mid+1])) {
            return mid;
        }  else if ((nums[mid - 1] < nums[mid] && nums[mid] < nums[mid+1])) {
           return findPeak(mid+1, endIndex, nums);
        }  else {
           return findPeak(startIndex, mid, nums); 
        }
    }
}
