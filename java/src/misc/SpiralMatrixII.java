package misc;
public class SpiralMatrixII {
	public int[][] generateMatrix(int n) {
		int[][] result = new int[n][n];
		int width = n;
		int startNumber = 1;
		int startR = 0;
		while (width > 0) {
			startNumber = populateOuterEdge(startNumber, startR, startR, width,
					result);
			startR++;
			width -= 2;
		}
		return result;
	}

	private int populateOuterEdge(int startNumber, int startR, int startC,
			int width, int[][] nums) {
		for (int i = 0; i < width; i++) {
			nums[startR][startC + i] = startNumber++;
		}
		for (int i = 1; i < width; i++) {
			nums[startR + i][startC + (width - 1)] = startNumber++;
		}
		for (int i = 1; i < width; i++) {
			nums[startR + (width - 1)][startC + (width - 1) - i] = startNumber++;
		}
		for (int i = 1; i < width - 1; i++) {
			nums[startR + (width - 1) - i][startC] = startNumber++;
		}
		return startNumber;
	}
}
