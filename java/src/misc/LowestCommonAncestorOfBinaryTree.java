package misc;
public class LowestCommonAncestorOfBinaryTree {
	enum Status {
		NONE, BOTH, P, Q
	}

	private class ResultObject {
		public Status status;
		public TreeNode lca;
		public Integer distanceOfLCA;
		
		public ResultObject(Status st, TreeNode tn, Integer d) {
			status = st;
			lca = tn;
			distanceOfLCA = d;
		}
	}
	
	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
		ResultObject obj = LCA(root, p, q, 0);
		return obj.lca;
	}

	private ResultObject LCA(TreeNode root, TreeNode p, TreeNode q, int currentDistance) {
		Status returnStatus = Status.NONE;
		Integer distanceOfLCA = null;
		TreeNode lca = null;
		
		if (root == null) {
			return new ResultObject(Status.NONE, null, null);
		}
		ResultObject leftResultObject = LCA(root.left, p, q, currentDistance + 1);
		ResultObject rightResultObject = LCA(root.right, p, q, currentDistance + 1);
		Status leftStatus = leftResultObject.status;
		Status rightStatus = rightResultObject.status;
		
		
		if (leftStatus == Status.NONE && rightStatus == Status.NONE) {
			if (root.val == p.val) {
				returnStatus = Status.P;
			} else if (root.val == q.val) {
				returnStatus = Status.Q;
			} else {
				returnStatus = Status.NONE;
			}
			distanceOfLCA = null;
			lca = null;
		} else if ((leftStatus == Status.P || leftStatus == Status.Q)
				&& rightStatus == Status.NONE) 
		{
			if ((leftStatus == Status.P && root.val == q.val) ||
				(leftStatus == Status.Q && root.val == p.val)) 
			{
				distanceOfLCA = currentDistance;
				lca = root;
				returnStatus = Status.BOTH;
			} else {
				distanceOfLCA = null;
				lca = null;
				returnStatus = leftStatus;	
			}
		} else if ((rightStatus == Status.P || rightStatus == Status.Q)
				&& leftStatus == Status.NONE) 
		{
			distanceOfLCA = null;
			lca = null;
			if ((rightStatus == Status.P && root.val == q.val) ||
				(rightStatus == Status.Q && root.val == p.val)) 
			{
				distanceOfLCA = currentDistance;
				lca = root;
				returnStatus = Status.BOTH;
			} else {
				distanceOfLCA = null;
				lca = null;
				returnStatus = rightStatus;	
			}
		} else if ((leftStatus == Status.P && rightStatus == Status.Q)
				|| (leftStatus == Status.Q && rightStatus == Status.P)) 
		{
			distanceOfLCA = currentDistance;
			lca = root;
			returnStatus = Status.BOTH;
		} else if (leftStatus == Status.BOTH && rightStatus != Status.BOTH) {
			distanceOfLCA = leftResultObject.distanceOfLCA;
			lca = leftResultObject.lca;
			returnStatus = Status.BOTH;
		} else if (rightStatus == Status.BOTH && leftStatus != Status.BOTH) {
			distanceOfLCA = rightResultObject.distanceOfLCA;
			lca = rightResultObject.lca;
			returnStatus = Status.BOTH;
		} else if (leftStatus == Status.BOTH && rightStatus == Status.BOTH) {
			if (leftResultObject.distanceOfLCA > rightResultObject.distanceOfLCA) {
				distanceOfLCA = leftResultObject.distanceOfLCA;
				lca = leftResultObject.lca;
				returnStatus = Status.BOTH;
			} else {
				distanceOfLCA = rightResultObject.distanceOfLCA;
				lca = rightResultObject.lca;
				returnStatus = Status.BOTH;
			}
		}
		
		return new ResultObject(returnStatus, lca, distanceOfLCA);
	}
}
