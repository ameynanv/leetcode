package misc;
import java.util.ArrayList;
import java.util.List;

public class N_Queens {
	int N;
	List<List<String>> solutions;
	int[] queenPosition;

	public List<List<String>> solveNQueens(int n) {
		N = n;
		solutions = new ArrayList<List<String>>();
		if (n < 1) return solutions;
		queenPosition = new int[N];
		for (int i = 0; i < N; i++)
			queenPosition[i] = -1;
		solveQueen(0);
		return solutions;
	}
	
	public int totalNQueens(int n) {
		List<List<String>> result = solveNQueens(n);
		return result.size();
	}

	private void solveQueen(int row) {
		if (row >= N) {
			solutions.add(printBoard());
			return;
		}
		for (int i = 0; i < N; i++) {
			if (isValid(row, i)) {
				queenPosition[row] = i;
				solveQueen(row + 1);
			}
		}
	}

	// Check if position at row = n is valid w.r.t. queen positions row = 0 to n-1
	private boolean isValid(int n, int position) {
		for (int i = 0; i < n; i++) {
			if (queenPosition[i] == position) return false;
			if ((queenPosition[i] - position) == (i - n)) return false;
			if ((position - queenPosition[i]) == (i - n)) return false;
		}
		return true;
	}

	private List<String> printBoard() {
		List<String> result = new ArrayList<String>();
		for (int r = 0; r < N; r++) {
			StringBuilder builder = new StringBuilder();
			for (int c = 0; c < N; c++) {
				if (c == queenPosition[r]) {
					builder.append("Q");
				} else {
					builder.append(".");
				}
			}
			result.add(builder.toString());
		}
		return result;
	}
}
