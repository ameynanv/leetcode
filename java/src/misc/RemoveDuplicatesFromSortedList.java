package misc;
import java.util.HashSet;
import java.util.Set;

public class RemoveDuplicatesFromSortedList {
	public ListNode deleteDuplicates(ListNode head) {
		Set<Integer> set = new HashSet<Integer>();
		ListNode node = head;
		ListNode prev = head;
		while (node != null) {
			if (set.contains(node.val)) {
				prev.next = node.next;
			} else {
				set.add(node.val);
				prev = node;
			}
			node = node.next;
		}
		return head;
	}
}
