package misc;
public class FindMinimumInRotatedSortedArrayII {
	public int findMin(int[] nums) {
		if (nums.length == 0)
			return 0;
		return searchMin(nums, 0, nums.length - 1);
	}

	private int searchMin(int[] nums, int start, int end) {
		if (start == end)
			return nums[start];
		int center = (start + end) >> 1;
		if (nums[end] < nums[center]) {
			// search right
			start = center + 1;
		} else if (nums[end] == nums[center]) {
			end--;
		} else {
			// search left
			end = center;
		}
		return searchMin(nums, start, end);
	}
}
