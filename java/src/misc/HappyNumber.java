package misc;
import java.util.HashSet;
import java.util.Set;


public class HappyNumber {
    public boolean isHappy(int n) {
        Set<Integer> set = new HashSet<Integer>();
        int t = n;
        while (true) {
            int sum = sumOfSqDigits(t);
            if (sum == 1) return true;
            if (set.contains(sum)) return false;
            set.add(sum);
            t = sum;
        }
    }
    
    private int sumOfSqDigits(int m) {
        int ans = 0;
        int t = m;
        while (t/10 > 0) {
            ans += (t % 10) * (t % 10);
            t = t / 10;
        }
        ans += (t % 10) * (t % 10);
        return ans;
    }
}
