package misc;
public class RemoveElement {
	public int removeElement(int[] nums, int val) {
		 // Edge Cases
        if (nums.length == 0) return 0;
        
        int copyFromIndex = 0;
        int copyToIndex = 0;
        while(copyFromIndex < nums.length) {
            if (nums[copyFromIndex] == val) {
                copyFromIndex++;
            } else {
                nums[copyToIndex] = nums[copyFromIndex];
                copyToIndex++;
                copyFromIndex++;
            }
        }
        return copyToIndex;
	}
}
