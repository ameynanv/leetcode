package misc;

public class DeleteNodeInLinkedList {
	public void deleteNode(ListNode node) {
        // Edge Cases
        if (node == null || node.next == null) return;
        ListNode tmp = node;
        tmp.val = tmp.next.val;
        ListNode t2 = tmp.next.next;
        tmp.next = t2;
    }
}
