package misc;

public class UniqueBinarySearchTrees {
    
    
    public int numTrees(int n) {
    	if (n < 1) return 0;
    	int[] NUMTREES = new int[n+1];
    	for (int i = 0; i < NUMTREES.length; i++) {
    		NUMTREES[i] = 0;	
    	}
        NUMTREES[1] = 1;
        return compute(n, NUMTREES);
    }
    
    private int compute(int n, int[] array) {
        if (array[n] != 0) return array[n];
        int ans = 0;
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                ans = ans + compute(n - 1, array);
            } else if (i == n - 1) {
                ans = ans + compute(n - 1, array);
            } else {
                ans = ans + compute(i, array) * compute(n - i - 1, array);
            }
        }
        array[n] = ans;
        return ans;    	
    }
}
