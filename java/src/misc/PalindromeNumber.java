package misc;

public class PalindromeNumber {
    public boolean isPalindrome(int x) {
        if (x < 0) return false;
        int rev = 0;
        int dup = x;
        while(dup != 0) {
            rev = rev * 10 + dup%10;
            dup = dup/10;
        }
        return (x == rev);
    }
    
    public static void main(String[] args) {
        PalindromeNumber service = new PalindromeNumber();
        System.out.println(service.isPalindrome(12321));
    }
}