package misc;
public class LinkedListCycleII {
	 public ListNode detectCycle(ListNode head) {
	        if (head == null) return head;
	        ListNode pt1 = head;
	        ListNode pt2 = head;
	        while (pt2.next != null && pt2.next.next != null) {
	            pt1 = pt1.next;
	            pt2 = pt2.next.next;
	            if (pt1 == pt2) {
	                pt1 = head;
	                while (pt1 != pt2) {
	                    pt1 = pt1.next;
	                    pt2 = pt2.next;
	                }
	                return pt1;
	            }
	        }
	        return null;
	    }
}
