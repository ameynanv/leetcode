package misc;
public class ExcelSheetColumnNumber {
	public int titleToNumber(String s) {
		if (s == null) return 0;
		int num = 0;
		for (int i = 0; i < s.length(); i++) {
			num = num * 26 + (s.charAt(i) - 64);
		}
		return num;
	}
}
