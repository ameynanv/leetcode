package misc;
public class Search2DMatrixII {
	int M, N;

	public boolean searchMatrix(int[][] matrix, int target) {
		M = matrix.length;
		N = matrix[0].length;
		int r = M - 1;
		int c = 0;

		while (r >= 0 && c < N) {
			if (matrix[r][c] == target)
				return true;
			if (matrix[r][c] < target) {
				c++;
			} else if (matrix[r][c] > target) {
				r--;
			}
		}
		return false;
	}
}
