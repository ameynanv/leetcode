package misc;

public class RotateImage {
    int N = 0;
    public void rotate(int[][] matrix) {
        N = matrix.length;
        if (N == 0 || N == 1) return;
        int width = N;
        for (int i = 0; i < N/2; i++) {
            rotate(i,i,width, matrix);
            width = width - 2;
        }
    }
    
    public void rotate(int startR, int startC, int width, int[][] matrix) {
        for (int i = 0; i < width - 1; i++) {
            int tmp = matrix[startR][startC + i];
            matrix[startR][startC + i] = matrix[width - 1 - i + startR][startC];
            matrix[width - 1 - i + startR][startC] = matrix[width - 1 + startR][startC + width - 1 - i];
            matrix[width - 1 + startR][startC + width - 1 - i] = matrix[startR + i][startC + width - 1];
            matrix[startR + i][startC + width - 1] = tmp;
        }
    }
}
