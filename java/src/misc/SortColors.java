package misc;
public class SortColors {
	public void sortColors(int[] nums) {
		if (nums.length < 2)
			return;
		int lt = 0;
		int gt = nums.length - 1;
		int i = lt;
		while (i <= gt) {
			if (nums[i] < 1) {
				exchg(nums, i, lt);
				lt++;
				i++;
			} else if (nums[i] > 1) {
				exchg(nums, i, gt);
				gt--;
			} else {
				i++;
			}
		}
	}

	private void exchg(int[] nums, int a, int b) {
		int tmp = nums[a];
		nums[a] = nums[b];
		nums[b] = tmp;
	}
}
