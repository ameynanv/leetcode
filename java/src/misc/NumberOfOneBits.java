package misc;
public class NumberOfOneBits {
	public int hammingWeight(int n) {
		int result = 0;
		int mask = 0x1;
		for (int i = 0; i < 32; i++) {
			if ((n & mask) != 0x00) {
				result++;
			}
			mask = mask << 1;
		}
		return result;
	}
}
