package misc;

public class MergeTwoLists {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
    	if (l1 == null && l2 != null) return l2;
    	if (l2 == null && l1 != null) return l1;
    	
        ListNode rHead = null;
        ListNode left = l1;
        ListNode right = l2;
        ListNode r = null;
        
        while(left != null && right != null) {        
            if (left.val < right.val) {
            	if (rHead == null) {
            		rHead = left;
            		r = rHead;
            	} else {
            		r.next = left;
            		r = r.next;
            	}
                left = left.next;
            } else {
            	if (rHead == null) {
            		rHead = right;
            		r = rHead;
            	} else {
            		r.next = right;
            		r = r.next;
            	}
                right = right.next;
            }
        } 
        if (left != null) r.next = left;
        if (right != null) r.next = right;

        return rHead;
    }
}
