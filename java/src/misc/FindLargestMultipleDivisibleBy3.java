package misc;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Arrays;


public class FindLargestMultipleDivisibleBy3 {
	private int[] convertListToArray(LinkedList<Integer> list) {
		int[] arr = new int[list.size()];
		Iterator<Integer> it = list.iterator();
		int k = 0;
		while(it.hasNext()) {
			arr[k++] = it.next();
		}
		return arr;
	}
	
	private int convertListToNumber(LinkedList<Integer> list) {
		int[] arr = convertListToArray(list);
		Arrays.sort(arr);
		int num = 0;
		for (int i = arr.length - 1; i >= 0; i--) {
			num = num * 10 + arr[i];
		}
		return num;
	}
  
	public int findLargestMultipleDivisibleBy3(int[] arr) {
		LinkedList<Integer> finalArray = new LinkedList<Integer>();
		LinkedList<Integer> rem1 = new LinkedList<Integer>();
		LinkedList<Integer> rem2 = new LinkedList<Integer>();
		Arrays.sort(arr);
		for (int i = arr.length - 1; i >= 0; i--) {
			if (arr[i] % 3 == 0)
				finalArray.add(arr[i]);
			if (arr[i] % 3 == 1) {
				rem1.add(arr[i]);
				if (rem1.size() == 3) {
					finalArray.addAll(rem1);
					rem1.clear();
				}
			}
			if (arr[i] % 3 == 2) {
				rem2.add(arr[i]);
				if (rem2.size() == 3) {
					finalArray.addAll(rem2);
					rem2.clear();
				}
			}
		}

		if (rem1.size() != 0 && rem2.size() != 0) {
			if (rem1.size() == rem2.size()) {
				finalArray.addAll(rem1);
				finalArray.addAll(rem2);
			} else {
				finalArray.add(rem1.getFirst());
				finalArray.add(rem2.getFirst());
			}
		}
		return convertListToNumber(finalArray);
	}
	
	public static void main(String[] args) {
		FindLargestMultipleDivisibleBy3 service = new FindLargestMultipleDivisibleBy3();
		int[] nums = {8,9,0,2,2,2};
		System.out.println(service.findLargestMultipleDivisibleBy3(nums));
	}
}
