package misc;

public class ClimbingStairs {
    int[] F;
    public int climbStairs(int n) {
    	if (n == 0) return 0;
    	if (n == 1) return 1;
    	if (n == 2) return 2;
        F = new int[n+1];
        for (int i = 0; i < n+1; i++) {
            F[i] = -1;
        }
        F[0] = 0;
        F[1] = 1;
        F[2] = 2;
        return countStairs(n);
    }
    
    private int countStairs(int n) {
        if (F[n] != -1) return F[n];
        F[n] = countStairs(n-1) + countStairs(n-2);
        return F[n];
    }
}
