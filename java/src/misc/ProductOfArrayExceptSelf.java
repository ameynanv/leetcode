package misc;
public class ProductOfArrayExceptSelf {
	public int[] productExceptSelf(int[] nums) {
		int[] output = new int[nums.length];
		int t = 1;
		output[0] = nums[0];
		for (int i = 1; i < nums.length - 1; i++) {
			output[i] = output[i - 1] * nums[i];

		}
		output[nums.length - 1] = output[nums.length - 2];
		t = nums[nums.length - 1];
		for (int i = nums.length - 2; i > 0; i--) {
			output[i] = output[i - 1] * t;
			t = t * nums[i];
		}
		output[0] = t;
		return output;
	}
}
