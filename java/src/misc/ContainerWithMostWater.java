package misc;
public class ContainerWithMostWater {
	public int maxArea(int[] height) {
		int startIndex = 0;
		int endIndex = height.length - 1;
		int maxArea = 0;
		while (startIndex < endIndex) {
			int area = (endIndex - startIndex)
					* min(height[startIndex], height[endIndex]);
			if (maxArea < area) {
				maxArea = area;
			}
			if (height[startIndex] < height[endIndex]) {
				startIndex++;
			} else if (height[startIndex] > height[endIndex]) {
				endIndex--;
			} else {
				startIndex++;
				endIndex--;
			}
		}

		return maxArea;
	}

	private int min(int a, int b) {
		return a < b ? a : b;
	}
}
