package misc;
public class RemoveLinkedListElements {
	public ListNode removeElements(ListNode head, int val) {
		// Edge Cases
		if (head == null) return null;
		
		ListNode result = head;
		// Position result correctly
		while(result != null && result.val == val) {
			result = result.next;
		}
		if (result == null) return null;
		
		ListNode current = result.next;
		ListNode previous = result;
		while(current != null) {
			if (current.val == val) {
				previous.next = current.next;
			} else {
				previous = current;
			}
			current = current.next;
		}
		return result;
	}
}
