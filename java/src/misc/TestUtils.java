package misc;
import java.util.ArrayDeque;
import java.util.Queue;


public class TestUtils {
	public String convertToString(ListNode root) {
		if (root == null) return "NULL";
		ListNode t = root;
		StringBuilder builder = new StringBuilder();
		builder.append(t.val);
		while (t.next != null) {
			builder.append(t.next.val);
			t = t.next;
		}
		return builder.toString();
	}
	
	public String convertToString(int[] arr) {
		if (arr == null) return "NULL";
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < arr.length; i++) {
			builder.append(arr[i]);	
		}
		return builder.toString();
	}
	
	public String convertToString(int[] arr, int length) {
		if (arr == null) return "NULL";
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < length; i++) {
			builder.append(arr[i]);	
		}
		return builder.toString();
	}
	
	public String convertToString(TreeNode root) {
		if (root == null) return "NULL";
		StringBuilder builder = new StringBuilder();
		Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);
		while(!queue.isEmpty()) {
			TreeNode node = queue.remove();
			builder.append(",");
			builder.append(node.val);
			if (node.left != null) queue.add(node.left);
			if (node.right != null) queue.add(node.right);

		}
		return builder.toString().replaceFirst(",", "");
	}
	
	public String convertToString(int[][] array) {
		StringBuilder builder = new StringBuilder();
		int M = array.length;
		if (M == 0) return "NULL";
		int N = array[0].length;
		builder.append("[");
		for (int r = 0; r < M; r++) {
			StringBuilder rb = new StringBuilder();
			for (int c = 0; c < N; c++) {
				rb.append(",");
				rb.append(array[r][c]);
			}
			builder.append(";");
			builder.append(rb.toString().replaceFirst(",", ""));
		}
		builder.append("]");
		return builder.toString().replaceFirst(";", "");
	}
	
}
