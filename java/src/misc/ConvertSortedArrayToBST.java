package misc;
public class ConvertSortedArrayToBST {
	public TreeNode sortedArrayToBST(int[] nums) {
		if (nums.length == 0)
			return null;
		return arrangeBST(nums, 0, nums.length - 1);
	}

	private TreeNode arrangeBST(int[] nums, int start, int end) {
		if (start == end)
			return new TreeNode(nums[start]);

		int center = (start + end) / 2;
		TreeNode node = new TreeNode(nums[center]);
		if (center - 1 >= start) {
			node.left = arrangeBST(nums, start, center - 1);
		}
		if (center + 1 <= end) {
			node.right = arrangeBST(nums, center + 1, end);
		}
		return node;
	}
}
