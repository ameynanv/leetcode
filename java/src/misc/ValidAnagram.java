package misc;
import java.util.Arrays;

public class ValidAnagram {
	public boolean isAnagram(String s, String t) {
		if (s == null || t == null) return false;
		char[] arrS = s.toCharArray();
		char[] arrT = t.toCharArray();
		if (arrS.length != arrT.length) return false;
		Arrays.sort(arrS);
		Arrays.sort(arrT);
		for (int i = 0; i < arrS.length; i++) {
			if (arrS[i] != arrT[i]) return false;
		}
		return true;
	}
}
