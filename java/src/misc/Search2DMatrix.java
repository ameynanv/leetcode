package misc;
public class Search2DMatrix {
	int M, N;

	public boolean searchMatrix(int[][] matrix, int target) {
		if (matrix.length == 0)
			return false;
		M = matrix.length;
		N = matrix[0].length;
		if (M == 1 && N == 1) {
			if (target == matrix[0][0]) return true;
			return false;
		}
		int start = 0;
		int end = M - 1;
		while (start < end) {
			int mid = (start + end) / 2 + 1;
			if (target == matrix[mid][0])
				return true;
			if (target < matrix[mid][0]) {
				end = mid - 1;
			} else {
				start = mid;
				
			}
		}
		int row = start;
		start = 0; end = N - 1;
		while (start <= end) {
			int mid = (start + end) / 2;
			if (target == matrix[row][mid])
				return true;
			if (target < matrix[row][mid]) {
				end = mid - 1;
			} else {
				start = mid + 1;
			}
		}
		
		return false;
	}
}
