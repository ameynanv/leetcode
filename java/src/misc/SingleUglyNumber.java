package misc;

public class SingleUglyNumber {
    public boolean isUgly(int num) {
        int temp = num;
        while (true) {
            if (temp == 0) return false;
            if (temp == 1) return true;
            if (temp % 2 != 0 && temp % 3 != 0 && temp % 5 != 0) return false;
            if (temp %2 == 0) temp = temp / 2;
            else if (temp % 3 == 0) temp = temp / 3;
            else temp = temp / 5;
        }
    }
}
