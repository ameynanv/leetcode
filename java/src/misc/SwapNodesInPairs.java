package misc;

public class SwapNodesInPairs {
	public ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null) return head;
        
        head = swapNodes(head);
        ListNode r = head.next.next;
        ListNode prev = head.next;
        
        while(r != null && r.next != null) {
            r = swapNodes(r);    
            prev.next = r;
            prev = r.next;
            r = r.next.next;
        }
        return head;
    }
    
    public ListNode swapNodes(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode tail = head.next.next;
        ListNode t = head;
        head = head.next;
        head.next = t;
        head.next.next = tail;
        return head;
    }
}
