package misc;
public class UniquePaths {
    int[][] board;
    public int uniquePaths(int m, int n) {
        if (m == 0 || n == 0) return 0;
        board = new int[m][n];
        for (int r = 0; r < m; r++)
            board[r][n-1] = 1;
        for (int c = 0; c < n; c++)
            board[m-1][c] = 1;
        for (int r = m - 2; r >= 0; r--)
            for (int c = n - 2; c >= 0; c--)
                board[r][c] = board[r+1][c] + board[r][c+1];
        return board[0][0];
    }
}
