package misc;

import java.util.ArrayDeque;

public class IntegerToEnglishWord {
	public String numberToWords(int num) {
        if (num == 0) return "Zero";
        ArrayDeque<String> deque = new ArrayDeque<String>();
        int k = 0;
        while(num/1000 > 0) {
        	if (num%1000  != 0) {
	            String partString = convertThreeDigitsToString(num%1000);
	            if (k == 1) partString += " Thousand";
	            if (k == 2) partString += " Million";
	            if (k == 3) partString += " Billion";
	            deque.addFirst(partString);
        	}
            k++;
            num = num/1000;
        }
        String partString = convertThreeDigitsToString(num%1000);
        if (k == 1) partString += " Thousand";
        if (k == 2) partString += " Million";
        if (k == 3) partString += " Billion";
        deque.addFirst(partString);
        
        StringBuffer buffer = new StringBuffer();
        for (String str: deque) {
            buffer.append(str);
            buffer.append(" ");
        }
        return buffer.toString().trim();
    }

    private String convertThreeDigitsToString(int num) {
        String firstTwoDigits = convertTwoDigitsToString(num%100);
        String thirdDigit = num/100 > 0 ? (convertDigitToString(num/100) + " Hundred") : "";
        String result = thirdDigit + " " + firstTwoDigits;
        return result.trim();
    }

    private String convertTwoDigitsToString(int num) {
        if (num < 10) return convertDigitToString(num);
        if (num == 10) return "Ten";
        if (num == 11) return "Eleven";
        if (num == 12) return "Twelve";
        if (num == 13) return "Thirteen";
        if (num == 14) return "Fourteen";
        if (num == 15) return "Fifteen";
        if (num == 16) return "Sixteen";
        if (num == 17) return "Seventeen";
        if (num == 18) return "Eighteen";
        if (num == 19) return "Nineteen";
        if (num > 19) {
            String unit = convertDigitToString(num%10);
            String tens = convertSecondDigitToString(num/10);
            return tens + " " + unit;
        }
        return "";
    }
    
    private String convertSecondDigitToString(int num) {
        if (num == 2) return "Twenty";
        if (num == 3) return "Thirty";
        if (num == 4) return "Forty";
        if (num == 5) return "Fifty";
        if (num == 6) return "Sixty";
        if (num == 7) return "Seventy";
        if (num == 8) return "Eighty";
        if (num == 9) return "Ninety";
        return "";
    }
    
    private String convertDigitToString(int num) {
        if (num == 1) return "One";
        if (num == 2) return "Two";
        if (num == 3) return "Three";
        if (num == 4) return "Four";
        if (num == 5) return "Five";
        if (num == 6) return "Six";
        if (num == 7) return "Seven";
        if (num == 8) return "Eight";
        if (num == 9) return "Nine";
        return "";
    }
    
	public static void main(String[] args) {
		IntegerToEnglishWord service = new IntegerToEnglishWord();
		System.out.println(service.numberToWords(0));
		System.out.println(service.numberToWords(1000010));
		System.out.println(service.numberToWords(1234567));
	}

}

