package misc;

import java.util.HashMap;

public class WordPattern {
    public boolean wordPattern(String pattern, String str) {
        if (pattern == null || str == null) return false;
        String[] strs = str.split(" ");
        if (pattern.length() != strs.length) return false;
        HashMap<String, String> map = new HashMap<String, String>();
        HashMap<String, String> mapRev = new HashMap<String, String>();
        for (int i = 0; i < pattern.length(); i++) {
            String pat = pattern.charAt(i) + "";
            
            if (map.containsKey(pat)) {
                if (!strs[i].equals(map.get(pat))) return false;
            } else {
                map.put(pat, strs[i]);
            }
            
            if (mapRev.containsKey(strs[i])) {
                if (!mapRev.get(strs[i]).equals(pat)) return false;
            } else {
                mapRev.put(strs[i], pat);
            }
        }
        return true;
    }
    
    public static void main(String[] args) {
    	WordPattern service = new WordPattern();
    	System.out.println(service.wordPattern("abba","a b b c"));
    }
}
