package misc;
public class HappyNumber2 {
	public boolean isHappy(int n) {
		int one = n, two = n;
		while (true) {
			one = sumOfSqDigits(one);
			two = sumOfSqDigits(two);
			two = sumOfSqDigits(two);
			if (one == 1)
				return true;
			if (one == two)
				return false;
		}
	}

	private int sumOfSqDigits(int m) {
		int ans = 0;
		int t = m;
		while (t / 10 > 0) {
			ans += (t % 10) * (t % 10);
			t = t / 10;
		}
		ans += (t % 10) * (t % 10);
		return ans;
	}
}
