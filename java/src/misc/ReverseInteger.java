package misc;
public class ReverseInteger {
    public int reverse(int x) {
    	boolean negativeFlag = x < 0 ? true : false;
    	if (negativeFlag) x = x * -1;
    	
        int ans = 0;
        while (x / 10 > 0 || x % 10 > 0) {
        	int digit = x % 10;
        	if (ans > Integer.MAX_VALUE / 10) return 0;
        	ans = ans * 10 + digit;
        	x = x / 10;
        }
        
        if (negativeFlag) ans = ans * -1;
    	return ans;
    }
}
