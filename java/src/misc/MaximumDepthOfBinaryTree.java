package misc;

public class MaximumDepthOfBinaryTree {
	public int maxDepth(TreeNode root) {
        // Edge Cases
        if (root == null) return 0;
        
        int leftDepth = root.left == null ? 1 : (1 + maxDepth(root.left));
        int rightDepth = root.right == null ? 1 : (1 + maxDepth(root.right));
        if (leftDepth < rightDepth) return rightDepth;
        else return leftDepth;
    }
}
