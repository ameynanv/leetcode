package misc;

import java.util.ArrayList;
import java.util.List;

public class RestoreIpAddresses {
	public List<String> restoreIpAddresses(String s) {
		return convertNumToString(s, 1);
	}

	private List<String> convertNumToString(String s, int num) {
		List<String> results = new ArrayList<String>();
		if (num < 4) {
			for (int i = 0; i < 3 && i < s.length(); i++) {
				String part = s.substring(0, i + 1);
				if (isValid(part)) {
					List<String> nextParts = convertNumToString(
							s.substring(i + 1, s.length()), num + 1);
					for (String nextPart : nextParts) {
						if (!nextPart.equalsIgnoreCase("-1")) {
							results.add(part + "." + nextPart);
						}
					}
				}
			}
		} else {
			if (isValid(s)) {
				results.add(s);
			} else {
				results.add("-1");
			}
		}
		return results;
	}

	private boolean isValid(String s) {
		if (s.equalsIgnoreCase(""))
			return false;
		if (s.length() > 3)
			return false;
		Integer num = Integer.parseInt(s);
		if (num < 10 && s.length() > 1)
			return false;
		if (num < 100 && s.length() > 2)
			return false;
		if (num > 255)
			return false;
		return true;
	}
	
	public static void main(String[] args) {
		RestoreIpAddresses service = new RestoreIpAddresses();
		System.out.println(service.restoreIpAddresses("0000"));
		System.out.println(service.restoreIpAddresses("00000"));
		System.out.println(service.restoreIpAddresses("000256"));
		System.out.println(service.restoreIpAddresses("0100100"));
	}
}
