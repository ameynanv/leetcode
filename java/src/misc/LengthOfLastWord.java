package misc;
public class LengthOfLastWord {
    public int lengthOfLastWord(String s) {
        if (s == null || s.length() == 0 ) return 0;
        
        int start = 0;
        while (start < s.length() && s.charAt(start) == ' ') {
            start++;
        }
        if (start > (s.length() - 1)) return 0;
        
        int end = s.length() - 1;
        while (end >= 0 && s.charAt(end) == ' ') {
            end--;
        }
        if (end < 0) return 0;
        
        int length = 0;
        for (int i = start; i < end + 1; i++) {
            if (s.charAt(i) == ' ') {
                length = 0;
            } else {
                length++;
            }
        }
		return length;
    }
}
