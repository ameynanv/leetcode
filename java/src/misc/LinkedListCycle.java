package misc;
public class LinkedListCycle {
	public boolean hasCycle(ListNode head) {
		if (head == null || head.next == null) return false;
		if (head.next.val == head.val) return true;
		ListNode pt1 = head.next;
		ListNode pt2 = head.next.next;
		
		while (pt1 != null && pt2 != null) {
			if (pt1.val == pt2.val) return true;
			if (pt2.next == null) return false;
			pt1 = pt1.next;
			pt2 = pt2.next.next;
		}
		return false;
	}
}
