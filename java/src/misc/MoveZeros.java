package misc;

public class MoveZeros {
    public void moveZeroes(int[] nums) {
        if (nums.length < 2) return;
        int wi = 0, ri = 0;
        for ( ; ri < nums.length; ) {
            if (nums[ri] != 0) {
                if (wi != ri)
                    nums[wi] = nums[ri];
                wi++;
                ri++;
            } else {
                ri++;
            }
        }
        for ( ; wi < nums.length; wi++)
            nums[wi] = 0;
    }
}
