package misc;
import java.util.ArrayList;
import java.util.List;


public class GrayCode {
    public List<Integer> grayCode(int n) {
        List<Integer> result = new ArrayList<Integer>();
        if(n < 0) return result;
        if (n == 0) {
        	result.add(0);
        	return result;
        }
       
        int max = 0x1;
        for (int i = 0; i < n; i++)
        	max = max << 1;
        
        for (int num = 0; num < max; num++) {
            int[] bits = convertToBits(num, n);
            int[] outBits = new int[n];
            outBits[n-1] = bits[n-1]; 
            for (int i = 1; i < n; i++) {
            	outBits[n - 1 -i] = bits[n - 1 - i] ^ bits[n - i];
            }
            result.add(convertToNumber(outBits, n));
        }
        return result;
    }
    
    public int[] convertToBits(int num, int n) {
        int numCopy = num;
        int[] bits = new int[n];
        for (int i = 0; i < n; i++) {
        	bits[i] = numCopy & 0x01;
            numCopy = numCopy >> 1;
        }
        return bits;
    }
    
    public int convertToNumber(int[] bits, int n) {
        int ans = 0;
        for (int i = 0; i < n; i++) {
        	ans = (ans << 1) + bits[n - 1 - i];
        }
        return ans;
    }
}
