package binaryTree;

//Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.
//Initially, all next pointers are set to NULL.
//Note:
//    You may only use constant extra space.
//    You may assume that it is a perfect binary tree (ie, all leaves are at the same level, and every parent has two children).
public class PopulatingNextRightPointersInEachNode {
	public void connect(TreeLinkNode root) {
		// Handling Edge Cases
		if (root == null)
			return;
		if (root.left != null)
			connect(root.left);
		if (root.right != null)
			connect(root.right);
		if (root.left != null && root.right != null) {
			root.left.next = root.right;
		} else {
			return;
		}
		TreeLinkNode left, right;
		left = root.left.right != null ? root.left.right : root.left.left;
		right = root.right.left != null ? root.right.left : root.right.right;

		while (left != null && right != null) {
			left.next = right;
			left = left.right != null ? left.right : left.left;
			right = right.left != null ? right.left : right.right;
		}
	}
}
