package binaryTree;

import java.util.List;
import java.util.ArrayList;

public class UniqueBSTII {
    @SuppressWarnings("unchecked")
	public List<TreeNode> generateTrees(int n) {
        List<TreeNode> empty = new ArrayList<TreeNode>();
        empty.add(null);
        if (n == 0) return empty;
        List<TreeNode>[] F = new List[n+1];
        F[0] = new ArrayList<TreeNode>();
        List<TreeNode> list = new ArrayList<TreeNode>();
        list.add(new TreeNode(1));
        F[1] = list;
        for (int num = 2; num < n+1; num++) {
            F[num] = new ArrayList<TreeNode>();
            for (int j = 1; j < num+1; j++) {
            	List<TreeNode> leftList = new ArrayList<TreeNode>();
            	if (j-1 > 0) {
	        		for (TreeNode node : F[j-1]) {
	        			leftList.add(cloneTree(node));
	        		}
            	}
            	
            	List<TreeNode> rightList = new ArrayList<TreeNode>();
            	if (num - j > 0) {
            		for (TreeNode node : F[num - j]) {
            			rightList.add(addNumberToTree(j, node));
            		}
            	}
            	
            	if (leftList.size() == 0) {
            		for (TreeNode right: rightList) {
            			TreeNode root = new TreeNode(j);
            			root.left = null;
            			root.right = cloneTree(right);
            			F[num].add(root);		
            		}            		
            	} else if (rightList.size() == 0)  {
            		for (TreeNode left: leftList) {
            			TreeNode root = new TreeNode(j);
            			root.left = cloneTree(left);
            			root.right = null;
            			F[num].add(root);		
            		}            	
            	
            	}
            	else {
	            	for (TreeNode left: leftList) {
	            		for (TreeNode right: rightList) {
	            			TreeNode root = new TreeNode(j);
	            			root.left = cloneTree(left);
	            			root.right = cloneTree(right);
	            			F[num].add(root);		
	            		}
	            	}
            	}
            }
        }
        return F[n];
   }
   
    public TreeNode cloneTree(TreeNode node) {
        if (node == null) return null;
        TreeNode result = new TreeNode(node.val);
        result.left = cloneTree(node.left);
        result.right = cloneTree(node.right);
        return result;
   }
    
   public TreeNode addNumberToTree(int k, TreeNode node) {
        if (node == null) return null;
        TreeNode result = new TreeNode(node.val + k);
        result.left = addNumberToTree(k, node.left);
        result.right = addNumberToTree(k, node.right);
        return result;
   }
   
	public static void main(String[] args) {
		UniqueBSTII service = new UniqueBSTII();
		for (TreeNode t: service.generateTrees(3))
			System.out.println(TreeNode.convertToString(t));

	}

}
