package binaryTree;

public class BinaryTreeMaxPathSum {
	
	int maxSum;
    public int maxPathSum(TreeNode root) {
        maxSum = Integer.MIN_VALUE;
        if (root != null) {
            int maxSinglePathSum = findMaxSinglePathSum(root);
            return Math.max(maxSum, maxSinglePathSum);
        } else {
            return 0;
        }
    }
    
    private int findMaxSinglePathSum(TreeNode node) {
        int leftSinglePathSum = node.left != null ? findMaxSinglePathSum(node.left) : Integer.MIN_VALUE;
        int rightSinglePathSum = node.right != null ? findMaxSinglePathSum(node.right) : Integer.MIN_VALUE;
        maxSum = Math.max(maxSum, node.val);
        if (node.left != null && node.right != null) {
            maxSum = Math.max(maxSum, Math.max(leftSinglePathSum, rightSinglePathSum) + node.val);    
            maxSum = Math.max(maxSum, (leftSinglePathSum + rightSinglePathSum + node.val));    
            return Math.max(node.val, node.val + Math.max(leftSinglePathSum , rightSinglePathSum));    
        } else if (node.left != null || node.right != null) {
            int pathSum = node.left != null ? leftSinglePathSum : rightSinglePathSum;
            maxSum = Math.max(maxSum, pathSum);
            maxSum = Math.max(maxSum, pathSum + node.val);    
            return Math.max(node.val, node.val + pathSum);
        } else {
            return node.val;
        }
    }
    
	public static void main(String[] args) {
		BinaryTreeMaxPathSum service = new BinaryTreeMaxPathSum();
		TreeNode root = new TreeNode(-1);
		root.left = new TreeNode(5);
		root.left.left = new TreeNode(4);
		root.left.left.right = new TreeNode(2);
		root.left.left.right.left = new TreeNode(-4);
		System.out.println(service.maxPathSum(root));
	}

}
