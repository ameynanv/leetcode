package binaryTree;
import java.util.ArrayList;
import java.util.List;


public class InorderTraversalRecursive {
	List<Integer> result;
	
	public List<Integer> inorderTraversal(TreeNode root) {
		result = new ArrayList<Integer>();
		iot(root);
		return result;
    }
	
	private void iot(TreeNode root) {
		if (root == null) return;
		if (root.left != null) iot(root.left);
		result.add(root.val);
		if (root.right != null) iot(root.right);
	}
}
