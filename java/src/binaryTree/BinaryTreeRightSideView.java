package binaryTree;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreeRightSideView {

    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        if (root != null) dfs(root, 0, result);
        return result;
    }
    
    private void dfs(TreeNode root, int level, List<Integer> result) {
        if (result.size() < level + 1) result.add(root.val);
        else result.set(level, root.val);
        if (root.left != null) dfs(root.left, level + 1, result);
        if (root.right != null) dfs(root.right, level + 1, result);
    }
}
