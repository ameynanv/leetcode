package binaryTree;

//
// The diameter of a tree (sometimes called the width) is the number of nodes on the longest path 
// between two leaves in the tree. The diagram below shows two trees each with diameter nine, 
// the leaves that form the ends of a longest path are shaded (note that there is more than 
// one path in each tree of length nine, but no path longer than nine nodes).
//
public class DiameterOfBinaryTree {

	private class Result {
		public int maxDistance;
		public int diameter;

		public Result(int distance, int diameter) {
			maxDistance = distance;
			this.diameter = diameter;
		}
	}

	public int diameterOfBinaryTree(TreeNode root) {
		if (root == null)
			return 0;
		Result result = diaOfBinaryTree(root);
		return result.diameter;
	}

	private Result diaOfBinaryTree(TreeNode root) {
		int maxDistance = 0;
		int diameter = 0;
		if (root == null)
			return new Result(maxDistance, diameter);

		Result leftResult = diaOfBinaryTree(root.left);
		Result rightResult = diaOfBinaryTree(root.right);
		int dLeft = leftResult.diameter;
		int dRight = rightResult.diameter;
		int dCombine = leftResult.maxDistance + rightResult.maxDistance;
		if (dLeft >= dRight && dLeft >= dCombine)
			diameter = dLeft;
		else if (dRight >= dLeft && dRight >= dCombine)
			diameter = dRight;
		else
			diameter = dCombine;

		maxDistance = 1 + (leftResult.maxDistance > rightResult.maxDistance ? leftResult.maxDistance
				: rightResult.maxDistance);
		return new Result(maxDistance, diameter);
	}
}
