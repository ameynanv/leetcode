package binaryTree;

import java.util.List;
import java.util.ArrayList;

/**
 *  https://leetcode.com/problems/binary-tree-level-order-traversal/
 */
 public class BinaryTreeLevelOrderTraversal {
    List<List<Integer>> result;
    
    public List<List<Integer>> levelOrder(TreeNode root) {
        result = new ArrayList<List<Integer>>();    
        if (root != null)
            levelOrderTraversal(root, 1);
        return result;
    }
    
    private void levelOrderTraversal(TreeNode node, int level) {
        if (result.size() < level) {
            ArrayList<Integer> list = new ArrayList<Integer>();
            list.add(node.val);
            result.add(list);
        } else {
            result.get(level - 1).add(node.val);
        }
        
        if (node.left != null)
            levelOrderTraversal(node.left, level + 1);
        if (node.right != null)
            levelOrderTraversal(node.right, level + 1);
            
        return;
    }
    
    public static void main(String[] args) {
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(9);
        root.right = new TreeNode(20);
        root.right.left = new TreeNode(15);
        root.right.right = new TreeNode(7);
        
        BinaryTreeLevelOrderTraversal service = new BinaryTreeLevelOrderTraversal();
        List<List<Integer>> result = service.levelOrder(root);
        System.out.println(result);
    }
}