package binaryTree;

import java.util.Stack;

public class BSTSum {
	
	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int x) {
			val = x;
		}
	}
	
	public class Result {
		TreeNode x;
		TreeNode y;
		public Result(TreeNode x, TreeNode y) {
			this.x = x; this.y = y;
		}
	}
	
	Stack<TreeNode> inorderStack;
	Stack<TreeNode> revInorderStack;
	
	public Result findNodesWithSum(TreeNode root, int target) {
		inorderStack = new Stack<TreeNode>();
		revInorderStack = new Stack<TreeNode>();
		
		TreeNode left = root;
		TreeNode right = root;
		
		GoLeft(left);
		GoRight(right);
		
		while(!inorderStack.isEmpty() && !revInorderStack.isEmpty()) {
			left = inorderStack.pop();
			right = revInorderStack.pop();
			if ((left.val + right.val) == target) {
				return new Result(left, right);
			} else if ((left.val + right.val) > target) {
				IncrementRevInorder(right);
				inorderStack.push(left);
			} else {
				IncrementInorder(left);
				revInorderStack.push(right);
			}
		}
		
		return null;
	}
	
	private void GoLeft(TreeNode node) {
		while (node != null) { 
			inorderStack.push(node);
			node = node.left;
		}
	}
	
	private void GoRight(TreeNode node) {
		while (node != null) { 
			revInorderStack.push(node);
			node = node.right;
		}
	}
	
	private void IncrementRevInorder(TreeNode node) {
		if (node.left != null) {
			GoRight(node.left);
		}
	}
	
	private void IncrementInorder(TreeNode node) {
		if (node.right != null) {
			GoLeft(node.right);
		}
	}
	
	public static void main(String[] args) {
		BSTSum service = new BSTSum();
		TreeNode root = service.new TreeNode(6);
		root.left = service.new TreeNode(2);
		root.right = service.new TreeNode(7);
		root.left.left = service.new TreeNode(1);
		root.left.right = service.new TreeNode(4);
		root.left.right.left = service.new TreeNode(3);
		root.left.right.right = service.new TreeNode(5);
		root.right.right = service.new TreeNode(9);
		root.right.right.left = service.new TreeNode(8);
		
		Result result = service.findNodesWithSum(root, 3);
		if (result == null) System.out.println("NULL");
		else System.out.println(result.x.val + " , " + result.y.val);
		
	}
}

