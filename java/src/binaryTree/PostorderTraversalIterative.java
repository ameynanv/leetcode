package binaryTree;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class PostorderTraversalIterative {
	LinkedList<Integer> result;
	Stack<TreeNode> stack;

	public List<Integer> postorderTraversal(TreeNode root) {
		result = new LinkedList<Integer>();
		if (root == null)
			return result;
		stack = new Stack<TreeNode>();
		stack.push(root);

		while (!stack.empty()) {
			TreeNode node = stack.pop();
			result.addFirst(node.val);
			if (node.left != null)
				stack.push(node.left);
			if (node.right != null)
				stack.push(node.right);
		}
		return result;
	}

}
