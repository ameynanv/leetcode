package binaryTree;

//Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.
//Initially, all next pointers are set to NULL.
//Note:
//    You may only use constant extra space.
public class PopulatingNextRightPointersInEachNodeII {
	
	public void connect(TreeLinkNode root) {
		TreeLinkNode node = root;
		TreeLinkNode firstNodeAtLevel = node;
		
		while(node != null) {
			System.out.print(node.val + " ");
			if (node.left != null) {
				TreeLinkNode aa = findNextNodeAtSameLevel(node.next);
				node.left.next = node.right != null ? node.right : aa;
			} 
			if (node.right != null) {
				node.right.next = findNextNodeAtSameLevel(node.next);
			}
			if (node.next != null) {
				node = node.next;
			}
			else {
				node = findNextNodeAtSameLevel(firstNodeAtLevel);
				firstNodeAtLevel = node;
			}
		}
	}
	
	private TreeLinkNode findNextNodeAtSameLevel(TreeLinkNode root) {
		TreeLinkNode node = root;
		if (node == null) return null;
		if (node.left == null && node.right == null) return findNextNodeAtSameLevel(node.next);
		if (node.left != null) return node.left;
		if (node.right != null) return node.right;
		
		return null;
	}
	
}
