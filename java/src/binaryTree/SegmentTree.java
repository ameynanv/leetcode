package binaryTree;
import java.util.Stack;
public class SegmentTree {
    
    private STNode root;
    
    public SegmentTree(int[] array) {
        root = buildSegmentTree(array, 0, array.length - 1);
    }
    
    private STNode buildSegmentTree(int[] array, int start, int end) {
        if (start > end) return null;
        if (start == end) return new STNode(array[start], start, start);
        int mid = (start + end) >>> 1;
        STNode left = buildSegmentTree(array, start, mid);
        STNode right = buildSegmentTree(array, mid+1, end);
        int sum = (left != null ? left.sum : 0)  + (right != null ? right.sum : 0);
        STNode node = new STNode(sum, start, end);
        node.left = left;
        node.right = right;
        return node;
    }
    
    
    public int sumRange(int startIndex, int endIndex) {
        return sumRange(root, startIndex, endIndex);
    }
    
    private int sumRange(STNode node, int startIndex, int endIndex) {
        if (startIndex <= node.rangeLow && endIndex >= node.rangeHigh)
            return node.sum;
        if (node.rangeLow > endIndex || node.rangeHigh < startIndex) 
            return 0;
        else {
            return sumRange(node.left, startIndex, endIndex) + sumRange(node.right, startIndex, endIndex);
        }
    }

    public void update(int index, int value) {
        Stack<STNode> stack = new Stack<STNode>();
        STNode node = root;
        while (node.rangeHigh != index || node.rangeLow != index) {
            stack.push(node);
            int mid = (node.rangeLow + node.rangeHigh) >>> 1;
            if (index <= mid) node = node.left;
            else node = node.right;
        }
        int oldVal = node.sum;
        node.sum = value;
        for (STNode parent: stack)
            parent.sum += (value - oldVal);
    }
    
    public static void main(String[] args) {
        int[] array = {1, 3, 5, 7, 9, 11};
        SegmentTree st = new SegmentTree(array);
        System.out.println(st.sumRange(1,2));
        System.out.println(st.sumRange(0,3));
        st.update(3, 8);
        System.out.println(st.sumRange(1,2));
        System.out.println(st.sumRange(0,3));
    }
    
    private class STNode {
        int sum;
        int rangeLow;
        int rangeHigh;
        STNode left;
        STNode right;
        public STNode(int val, int low, int high) {
            sum = val;
            rangeLow = low;
            rangeHigh = high;
        }
    }
    
    
    
}

