package binaryTree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class PreorderTraversalIterative {
	List<Integer> result;
	Stack<TreeNode> stack;

	public List<Integer> preorderTraversal(TreeNode root) {
		result = new ArrayList<Integer>();
		if (root == null)
			return result;
		stack = new Stack<TreeNode>();
		stack.push(root);
		while (!stack.empty()) {
			TreeNode node = stack.pop();
			result.add(node.val);
			if (node.right != null)
				stack.push(node.right);
			if (node.left != null)
				stack.push(node.left);
		}
		return result;
	}
}
