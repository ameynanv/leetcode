package binaryTree;

import java.util.ArrayList;
import java.util.List;

public class PreorderTraversalRecursive {
	List<Integer> result;

	public List<Integer> preorderTraversal(TreeNode root) {
		result = new ArrayList<Integer>();
		pre_ot(root);
		return result;
	}

	private void pre_ot(TreeNode node) {
		if (node == null)
			return;
		result.add(node.val);
		if (node.left != null)
			pre_ot(node.left);
		if (node.right != null)
			pre_ot(node.right);
	}
}
