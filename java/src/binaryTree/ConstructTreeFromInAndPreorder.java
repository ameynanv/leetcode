package binaryTree;

import java.util.ArrayDeque;

public class ConstructTreeFromInAndPreorder {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        ArrayDeque<Integer> preOrderDeque = new ArrayDeque<Integer>();
        for (int i = 0; i < preorder.length; i++)
            preOrderDeque.add(preorder[i]);
        return build(preOrderDeque, inorder);
    }
    
    private TreeNode build(ArrayDeque<Integer> preOrder, int[] inorder) {
        if (preOrder.size() == 0) return null;
        TreeNode root = new TreeNode(preOrder.removeFirst());
        if (inorder.length == 0) return root;
        int i = 0;
        while(i < inorder.length && inorder[i] != root.val) 
            i++;
        if (i == 0) {
            root.left = null;
        } else {
            int[] leftInorder = new int[i];    
            for (int k = 0; k < i; k++)
                leftInorder[k] = inorder[k];
            TreeNode left = build(preOrder, leftInorder);
            root.left = left;
        }
        
        if ((inorder.length - i - 1) == 0) {
            root.right = null;
        } else {
            int[] rightInorder = new int[inorder.length - i - 1];
            for (int k = i+1; k < inorder.length; k++)
                rightInorder[k-i-1] = inorder[k];
            TreeNode right = build(preOrder, rightInorder);
            root.right = right;
        }
        return root;
    }
    
	public static void main(String[] args) {
		ConstructTreeFromInAndPreorder service = new ConstructTreeFromInAndPreorder();
		int[] preOrder = {6,2,1,4,3,5,8,7,9};
		int[] inOrder = {1,2,3,4,5,6,7,8,9};
		TreeNode node = service.buildTree(preOrder, inOrder);
		System.out.println(TreeNode.convertToString(node));

	}

}
