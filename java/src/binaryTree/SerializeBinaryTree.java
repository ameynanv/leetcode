package binaryTree;

import java.util.ArrayDeque;

public class SerializeBinaryTree {
	
	// Encodes a tree to a single string.
	public String serialize(TreeNode root) {
		if (root == null) return "#";
		StringBuffer buffer = new StringBuffer();
		TreeNode nullNode = new TreeNode(-1);
		ArrayDeque<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);
		while(!queue.isEmpty()) {
			TreeNode node = queue.remove();
			if (node == nullNode) { 
				buffer.append(",#");
			} else {
				buffer.append("," + node.val);
				queue.add(node.left == null ? nullNode : node.left);
				queue.add(node.right == null ? nullNode : node.right);
			}
		}
		return buffer.toString().replaceFirst(",","");
	}

	// Decodes your encoded data to tree.
	public TreeNode deserialize(String data) {
		if (data.equalsIgnoreCase("#")) return null;
		TreeNode nullNode = new TreeNode(-1);
		String[] strs = data.split(",");
		if (strs.length == 0) return null;
		ArrayDeque<TreeNode> queue = new ArrayDeque<TreeNode>();
		
		queue.add(parseNode(strs[0]));
		int i = 1;
		TreeNode result = queue.peek(); 
		while (i < strs.length && !queue.isEmpty()) {
			TreeNode node = queue.remove();
			if (node == nullNode) continue;
			node.left = i < strs.length ? parseNode(strs[i++]) : null;
			node.right = i < strs.length ? parseNode(strs[i++]) : null;
			queue.add(node.left == null ? nullNode : node.left);
			queue.add(node.right == null ? nullNode : node.right);
		}
		return result;
	}
	
	private TreeNode parseNode(String str) {
		if (str.equalsIgnoreCase("#")) {
			return null;
		} else {
			return new TreeNode(Integer.parseInt(str));
		}
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.right.left = new TreeNode(4);
		root.right.right = new TreeNode(5);
		SerializeBinaryTree service = new SerializeBinaryTree();
		String str = service.serialize(root);
		System.out.println(str);
		TreeNode root2 = service.deserialize(str);
		System.out.println(TreeNode.convertToString(root2));
		
		
		TreeNode root3 = null;
		String str3 = service.serialize(root3);
		System.out.println(str3);
		TreeNode root4 = service.deserialize(str3);
		System.out.println(TreeNode.convertToString(root4));
	}

}

