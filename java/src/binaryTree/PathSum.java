package binaryTree;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Iterator;

public class PathSum {
    
    
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> solutions = new ArrayList<List<Integer>>();
        ArrayDeque<Integer> pathStack = new ArrayDeque<Integer>();    
        if (root == null) return solutions;
        dfs(root, 0, sum, pathStack, solutions);
        return solutions;
    }
    
    private void dfs(TreeNode node, int currentSum, int targetSum, ArrayDeque<Integer> pathStack, List<List<Integer>> solutions) {
        if (node.left == null && node.right == null) {
            if ((currentSum + node.val) == targetSum) {
                ArrayList<Integer> list = new ArrayList<Integer>();
                list.add(node.val);
                Iterator<Integer> it = pathStack.iterator();
                while(it.hasNext()) {
                    list.add(it.next());
                }
                Collections.reverse(list);
                solutions.add(list);
                return;
            }
        } else {
            pathStack.push(node.val);
            if (node.left != null) {
                dfs(node.left, currentSum + node.val, targetSum, pathStack, solutions);
            }
            if (node.right != null) {
                dfs(node.right, currentSum + node.val, targetSum, pathStack, solutions);
            }
            pathStack.pop();
        }
    }
    
    
    
    
    
    
    
    
    
    
    

    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) return false;
        return dfs(root, 0, sum);
    }
    
    private boolean dfs(TreeNode node, int currentSum, int targetSum) {
        if (node.left == null && node.right == null) {
            return ((currentSum+node.val) == targetSum);
        }
        else {
            boolean result = false;
            if (node.left != null) 
                result = result || dfs(node.left, currentSum+node.val, targetSum);
            if (node.right != null) 
                result = result || dfs(node.right, currentSum+node.val, targetSum);
            return result;
        }
    }
    
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(-2);
        root.right = new TreeNode(-3);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);
        root.left.left.left = new TreeNode(-1);
        root.right.left = new TreeNode(-2);
        
        PathSum service = new PathSum();
        System.out.println(service.hasPathSum(root, 2));
        System.out.println(service.pathSum(root, 2));
        
    }
}