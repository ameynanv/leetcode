package binaryTree;

import java.util.ArrayList;
import java.util.List;

public class MinHeightTrees {

    @SuppressWarnings("unchecked")
	public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        List<Integer>[] adj = new ArrayList[n];
        buildAdjList(adj, edges);
        List<Integer> leaves = findAllLeafNodes(adj);
        while(n > 2) {
            n = n - leaves.size();
            deleteLeaves(adj, leaves);
            leaves = findAllLeafNodes(adj);
        }
        List<Integer> result = new ArrayList<Integer>();
        for (Integer i: leaves) {
            result.add(i);
        }
        return result;
    }
    
    private void deleteLeaves(List<Integer>[] adj, List<Integer> leaves) {
        for (Integer i: leaves) {
            for (Integer k : adj[i]) {
                if (k != null)
                    adj[k].remove(i);
            }
            adj[i] = null;
        }
    }
    
    private List<Integer> findAllLeafNodes(List<Integer>[] adj) {
        List<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < adj.length; i++) {
            if (adj[i] != null && adj[i].size() <= 1) {
                result.add(i);
            }
        }
        return result;
    }
    
    private void buildAdjList(List<Integer>[] adj, int[][] edges) {
        for (int i = 0; i < adj.length; i++) {
            adj[i] = new ArrayList<Integer>();
        }
        for (int r = 0; r < edges.length; r++) {
            adj[edges[r][0]].add(edges[r][1]);
            adj[edges[r][1]].add(edges[r][0]);
        }
    }
    
	public static void main(String[] args) {
		MinHeightTrees service = new MinHeightTrees();
		int[][] edges = {{0,3},{1,3},{2,3},{4,3},{5,4}};
        System.out.println(service.findMinHeightTrees(6, edges));
        
        int[][] edges1 = {{1,0},{1,2},{1,3}};
        System.out.println(service.findMinHeightTrees(4, edges1));
        
        int[][] edges2 = {{0,1},{1,2},{1,3},{3,5},{2,4},{4,6}};
        System.out.println(service.findMinHeightTrees(7, edges2));        
        
	}

}
