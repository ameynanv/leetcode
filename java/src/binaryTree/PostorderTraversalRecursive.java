package binaryTree;

import java.util.LinkedList;
import java.util.List;

public class PostorderTraversalRecursive {
	LinkedList<Integer> result;

	public List<Integer> postorderTraversal(TreeNode root) {
		result = new LinkedList<Integer>();
		if (root == null)
			return result;
		pot(root);
		return result;
	}

	private void pot(TreeNode node) {
		if (node == null)
			return;
		if (node.left != null)
			pot(node.left);
		if (node.right != null)
			pot(node.right);
		result.add(node.val);
	}

}
