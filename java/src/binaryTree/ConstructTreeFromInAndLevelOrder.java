package binaryTree;
import java.util.ArrayList;
import java.util.HashSet;

public class ConstructTreeFromInAndLevelOrder {

	public TreeNode constructTree(ArrayList<Integer> inOrder, ArrayList<Integer> levelOrder) {
		if (levelOrder.size() == 0) return null;
		TreeNode root = new TreeNode(levelOrder.remove(0));
		ArrayList<Integer> leftInOrderList = new ArrayList<Integer>();
		HashSet<Integer> leftInOrderSet = new HashSet<Integer>();
		ArrayList<Integer> rightInOrderList = new ArrayList<Integer>();
		HashSet<Integer> rightInOrderSet = new HashSet<Integer>();
		boolean left = true;
		for (int i = 0; i < inOrder.size(); i++) {
			if (left) {
				if (inOrder.get(i) == root.val) {
					left = false;
				} else {
					leftInOrderList.add(inOrder.get(i));
					leftInOrderSet.add(inOrder.get(i));
				}
			} else {
				rightInOrderList.add(inOrder.get(i));
				rightInOrderSet.add(inOrder.get(i));
			}
		}
		ArrayList<Integer> leftLevelOrderList = new ArrayList<Integer>();
		ArrayList<Integer> rightLevelOrderList = new ArrayList<Integer>();
		for (int i = 0; i < levelOrder.size(); i++) {
			if (leftInOrderSet.contains(levelOrder.get(i))) {
				leftLevelOrderList.add(levelOrder.get(i));
			} else {
				rightLevelOrderList.add(levelOrder.get(i));
			}
		}
		root.left = constructTree(leftInOrderList, leftLevelOrderList);
		root.right = constructTree(rightInOrderList, rightLevelOrderList);
		return root;
	}
	
	public static void main(String[] args) {
		ArrayList<Integer> inOrderList = new ArrayList<Integer>();
		ArrayList<Integer> levelOrderList = new ArrayList<Integer>();
		inOrderList.add(4);inOrderList.add(8);inOrderList.add(10);inOrderList.add(12);inOrderList.add(14);
		inOrderList.add(20);inOrderList.add(22);
		levelOrderList.add(20);levelOrderList.add(8);levelOrderList.add(22);levelOrderList.add(4);
		levelOrderList.add(12);levelOrderList.add(10);levelOrderList.add(14);
		ConstructTreeFromInAndLevelOrder service = new ConstructTreeFromInAndLevelOrder();
		TreeNode root = service.constructTree(inOrderList, levelOrderList);
		System.out.println(TreeNode.convertToString(root));
		
	}

}

