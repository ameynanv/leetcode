package binaryTree;

import java.util.TreeMap;
import java.util.ArrayList;

/**
 * http://www.geeksforgeeks.org/print-binary-tree-vertical-order-set-2/
 *
 */
public class VerticalTreePrinting {

	public void printVertical(TreeNode root) {
		if (root == null)
			return;
		TreeMap<Integer, ArrayList<Integer>> levelMap = new TreeMap<Integer, ArrayList<Integer>>();
		dfs(root, 0, levelMap);
		System.out.println(levelMap);
	}

	private void dfs(TreeNode node, Integer level,
			TreeMap<Integer, ArrayList<Integer>> levelMap) {
		addNodeToMap(levelMap, node, level);
		if (node.left != null)
			dfs(node.left, level - 1, levelMap);
		if (node.right != null)
			dfs(node.right, level + 1, levelMap);
	}

	private void addNodeToMap(TreeMap<Integer, ArrayList<Integer>> levelMap,
			TreeNode node, Integer level) {
		if (levelMap.containsKey(level)) {
			levelMap.get(level).add(node.val);
		} else {
			ArrayList<Integer> list = new ArrayList<Integer>();
			list.add(node.val);
			levelMap.put(level, list);
		}
	}
	
	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(5);
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(7);
		root.right.left.right = new TreeNode(8);
		root.right.right.right = new TreeNode(9);
		VerticalTreePrinting service = new VerticalTreePrinting();
		service.printVertical(root);
	}
}