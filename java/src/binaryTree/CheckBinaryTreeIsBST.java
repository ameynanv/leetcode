package binaryTree;

import java.util.ArrayList;

public class CheckBinaryTreeIsBST {
	
	public boolean checkBinaryTreeIsBST(TreeNode root) {
		Integer prevValue = null;
		for (TreeNode node: inOrderAccess(root)) {
			if (prevValue == null) prevValue = node.val;
			if (node.val < prevValue) return false;
			prevValue = node.val;
		}
		return true;
	}
	
	private Iterable<TreeNode> inOrderAccess(TreeNode root) {
		ArrayList<TreeNode> inOrderList = new ArrayList<TreeNode>();
		dfs(root, inOrderList);
		return inOrderList;
	}
	
	private void dfs(TreeNode node, ArrayList<TreeNode> inOrderList) {
		if (node == null) return;
		if (node.left != null) dfs(node.left, inOrderList);
		inOrderList.add(node);
		if (node.right != null) dfs(node.right, inOrderList);
	}
}
