package binaryTree;

public class FlattenBinaryTreeToLinkedList {
    public void flatten(TreeNode root) {
        if (root == null) return;
        if (root.left == null) {
            flatten(root.right);
            return;
        }
        if (root.right == null) {
            root.right = root.left;
            root.left = null;
            flatten(root.right);
            return;
        }
        TreeNode temp = root.right;
        root.right = root.left;
        root.left = null;
        TreeNode rightNode = rightMostNode(root.right);
        rightNode.right = temp;
        flatten(root.right);
    }
    
    private TreeNode rightMostNode(TreeNode root) {
        if (root.right == null) return root;
        else return rightMostNode(root.right);
    }
}
