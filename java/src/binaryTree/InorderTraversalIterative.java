package binaryTree;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class InorderTraversalIterative {
	List<Integer> result;
	Stack<TreeNode> stack;
	public List<Integer> inorderTraversal(TreeNode root) {
		result = new ArrayList<Integer>();
		stack = new Stack<TreeNode>();

		TreeNode temp = root;
		while (true) {
			GoLeft(temp);
			if (stack.empty()) {
				break;
			}
			temp = stack.pop();
			result.add(temp.val);
			temp = temp.right;
		}
		return result;
	}
	
	private void GoLeft(TreeNode x) {
		while(x != null) {
			stack.push(x);
			x = x.left;
		}
			
	}
}
