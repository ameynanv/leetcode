package binaryTree;

public class BalancedBinaryTree {
	public boolean isBalanced(TreeNode root) {
		int ans = getDepth(root);
		if (ans == -1)
			return false;
		return true;
	}

	private int getDepth(TreeNode node) {
		if (node == null)
			return 0;
		int left = getDepth(node.left);
		if (left == -1)
			return -1;
		int right = getDepth(node.right);
		if (right == -1)
			return -1;
		if (Math.abs(left - right) > 1)
			return -1;
		return (max(left, right) + 1);
	}

	private int max(int a, int b) {
		if (a > b)
			return a;
		return b;
	}
}
