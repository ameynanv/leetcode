package binaryTree;
import java.util.Stack;
import java.util.Iterator;
public class SumRootToLeafNumbers {
    public int sumNumbers(TreeNode root) {
        Stack<TreeNode> stack = new Stack<TreeNode>();
        if (root == null) return 0;
        return dfs(root, stack);
    }
    
    private int dfs(TreeNode node, Stack<TreeNode> stack) {
        int sum = 0;
        stack.push(node);
        if (node.left == null && node.right == null)
            sum += getRootToLeafNumber(stack);
        else {
            if (node.left != null)
                sum += dfs(node.left, stack);
            if (node.right != null)
                sum += dfs(node.right, stack);
        }
        stack.pop();
        return sum;
    }
    
    private int getRootToLeafNumber(Stack<TreeNode> stack) {
        int num = 0;
        Iterator<TreeNode> it = stack.iterator();
        while(it.hasNext()) {
            TreeNode node = it.next();
            num = num * 10 + node.val;
        }
        return num;
    }
    
    
    public static void main(String[] args) {
        TreeNode root = new TreeNode(6);
		root.left = new TreeNode(2);
		root.right = new TreeNode(7);
		root.left.left = new TreeNode(1);
		root.left.right = new TreeNode(4);
		root.left.right.left = new TreeNode(3);
		root.left.right.right = new TreeNode(5);
		root.right.right = new TreeNode(9);
		root.right.right.left = new TreeNode(8);
		
		SumRootToLeafNumbers service = new SumRootToLeafNumbers();
		System.out.println(service.sumNumbers(root));
    }
}