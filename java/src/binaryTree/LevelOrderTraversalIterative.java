package binaryTree;

import java.util.ArrayDeque;
import java.util.ArrayList;

public class LevelOrderTraversalIterative {
    
    public Iterable<TreeNode> levelOrderTraversal(TreeNode root) {
        ArrayList<TreeNode> output = new ArrayList<TreeNode>();
        ArrayDeque<TreeNode> queue = new ArrayDeque<TreeNode>();
        TreeNode node = root;
        
        queue.add(node);
        while(!queue.isEmpty()) {
            node = queue.remove();
            output.add(node);
            if (node.left != null)
                queue.add(node.left); 
            if (node.right != null) 
                queue.add(node.right);
        }
        return output;
    }
    
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left =  new TreeNode(2);
        root.right =  new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.right.left = new TreeNode(5);
        root.right.right = new TreeNode(6);
        root.right.right.left = new TreeNode(7);
        root.right.right.right = new TreeNode(8);
        root.right.right.right.right = new TreeNode(9);
        
        LevelOrderTraversalIterative service = new LevelOrderTraversalIterative();
        Iterable<TreeNode> order = service.levelOrderTraversal(root);
        for(TreeNode t : order) {
            System.out.print(t.val + " ");
        }
        System.out.println(" ");
        
    }
    
    
}