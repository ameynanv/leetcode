package binaryTree;
import java.util.Stack;
import java.util.ArrayList;
import java.lang.Iterable;


public class KthSmallestInBST {
    
    public int kthSmallest(TreeNode root, int k) {
        InorderTraversal traverse = new InorderTraversal(root);
        int i = 1;
        for (TreeNode node: traverse.getInorderList()) {
            if (i == k) 
                return node.val;
            i++;
            
        }
        return 0;
    }

    private class InorderTraversal {
        private Stack<TreeNode> stack;
        private ArrayList<TreeNode> list;
        public InorderTraversal(TreeNode root) {
            stack = new Stack<TreeNode>();
            list = new ArrayList<TreeNode>();
            TreeNode node = root;
            GoLeft(node);
            while(!stack.isEmpty()) {
                node = stack.pop();
                list.add(node);
                if (node.right != null) {
                    node = node.right;
                    GoLeft(node);
                }
            }
        }
        private void GoLeft(TreeNode node) {
            while(node != null) {
                stack.push(node);
                node = node.left;
            }
        }
        Iterable<TreeNode> getInorderList() {
            return list;            
        }
    }    
    
    public static void main(String[] args) {
        TreeNode root = new TreeNode(6);
		root.left = new TreeNode(2);
		root.right = new TreeNode(7);
		root.left.left = new TreeNode(1);
		root.left.right = new TreeNode(4);
		root.left.right.left = new TreeNode(3);
		root.left.right.right = new TreeNode(5);
		root.right.right = new TreeNode(9);
		root.right.right.left = new TreeNode(8);
		
		KthSmallestInBST service = new KthSmallestInBST();
		System.out.println(service.kthSmallest(root, 4));
    }
}

