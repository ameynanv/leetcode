package binaryTree;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

public class BinaryTreePaths {

    public List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<String>();
        if (root == null) return result;
        ArrayDeque<TreeNode> stack = new ArrayDeque<TreeNode>();
        dfs(root, stack, result);
        return result;
    }
    
    private void dfs(TreeNode root, ArrayDeque<TreeNode> stack, List<String> result) {
        if (root.left == null && root.right == null) {
            StringBuffer buffer = new StringBuffer();
            for (TreeNode node: stack) {
                buffer.append("->");
                buffer.append(node.val);
            }
            buffer.append("->");
            buffer.append(root.val);
            result.add(buffer.toString().replaceFirst("->", ""));
        } else {
            stack.add(root);
            if (root.left != null) dfs(root.left, stack, result);
            if (root.right != null) dfs(root.right, stack, result);
            stack.removeLast();
        }
    }
    
	public static void main(String[] args) {
		BinaryTreePaths service = new BinaryTreePaths();
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.right.left = new TreeNode(4);
		root.right.right = new TreeNode(5);
		System.out.println(service.binaryTreePaths(root));
	}

}

