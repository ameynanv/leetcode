package binaryTree;

public class LowestCommonAncestorOfBST {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || p == null || q == null) {
            return null;
        }
        int max = p.val >= q.val ? p.val : q.val;
        int min = p.val <= q.val ? p.val : q.val;
        if (root.val <= max && root.val >= min)
            return root;
        else if (root.val >= max) 
            return lowestCommonAncestor(root.left, p, q);
        else
            return lowestCommonAncestor(root.right, p, q);
    }
}
