package binaryTree;
import java.util.HashMap;

public class Trie {
    
    private TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    // Inserts a word into the trie.
    public void insert(String word) {
        int i = 0;
        TrieNode node = root;
        while (i < word.length()) {
            TrieNode child = new TrieNode();
            Character key = word.charAt(i);
            if (node.children.containsKey(key)) {
                node = node.children.get(key);
            } else {
                node.children.put(key, child);
                node = child;
            }
            i++;
        }
        node.wordEnd = true;
    }

    // Returns if the word is in the trie.
    public boolean search(String word) {
        int i = 0;
        TrieNode node = root;
        while(i < word.length() && node.children.containsKey(word.charAt(i))) {
            node = node.children.get(word.charAt(i));
            i++;
        }
        if (i == word.length()) return node.wordEnd;
        return false;
    }

    // Returns if there is any word in the trie
    // that starts with the given prefix.
    public boolean startsWith(String prefix) {
        int i = 0;
        TrieNode node = root;
        while(i < prefix.length() && node.children.containsKey(prefix.charAt(i))) {
            node = node.children.get(prefix.charAt(i));
            i++;
        }
        if (i == prefix.length()) return true;
        return false;
    }
    
    public static void main(String[] args) {
        Trie trie = new Trie();
        trie.insert("the");trie.insert("a");trie.insert("there");
        trie.insert("answer");trie.insert("any");trie.insert("by");
        trie.insert("bye");trie.insert("their");
        
        System.out.println(trie.search("answer"));
        System.out.println(trie.search("ans"));
        System.out.println(trie.startsWith("ans"));
    }
}

class TrieNode {
    // Initialize your data structure here.
    boolean wordEnd;
    HashMap<Character, TrieNode> children;
    
    public TrieNode() {
        wordEnd = false;
        children = new HashMap<Character, TrieNode>();
    }
}