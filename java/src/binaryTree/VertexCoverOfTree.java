package binaryTree;

/**
 * http://www.geeksforgeeks.org/vertex-cover-problem-set-2-dynamic-programming-solution-tree/
 *
 */
public class VertexCoverOfTree {
	
	public int findVertexCover(TreeNode node) {
		int[] result = vertexCover(node);
		return Math.min(result[0], result[1]);
	}
	
	public int[] vertexCover(TreeNode node) {
		int[] result = new int[2];
		if (node == null) {
			result[0] = 0; result[1] = 0;
			return result;
		}
		int[] left;
		int[] right;
		if (node.left == null) {
			left = new int[2]; left[0] = 0; left[1] = 0;
		} else {
			left = vertexCover(node.left);
		}
		if (node.right == null) {
			right = new int[2]; right[0] = 0; right[1] = 0;
		} else {
			right = vertexCover(node.right);
		}
		// if node is included
		int include = 1 + Math.min(left[0], left[1]) + Math.min(right[0], right[1]); 
		// if node is not included
		int exclude = left[0] + right[0];
		result[0] = include;
		result[1] = exclude;
		return result;
	}
	
	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(5);
		root.right.right = new TreeNode(6);
		root.left.right.left = new TreeNode(7);
		root.left.right.right = new TreeNode(8);
		VertexCoverOfTree service = new VertexCoverOfTree();
		System.out.println(service.findVertexCover(root));
		
	    TreeNode root2 = new TreeNode(20);
	    root2.left = new TreeNode(8);
	    root2.left.left = new TreeNode(4);
	    root2.left.right = new TreeNode(12);
	    root2.left.right.left = new TreeNode(10);
	    root2.left.right.right = new TreeNode(14);
	    root2.right = new TreeNode(22);
	    root2.right.right = new TreeNode(25);
	    System.out.println(service.findVertexCover(root2));

	}
}

