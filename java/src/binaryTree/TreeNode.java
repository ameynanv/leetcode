package binaryTree;

import java.util.ArrayDeque;
import java.util.Queue;

public class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
	
	public String toString() {
		return val + "";
	}
	public static TreeNode createTree(String str) {
		String[] arr = str.split(",");
		int k = 0;
		ArrayDeque<TreeNode> queue = new ArrayDeque<TreeNode>();
		TreeNode rootNode = new TreeNode(Integer.parseInt(arr[0]));
		k++;
		TreeNode node = rootNode;
		queue.add(node);
		while (!queue.isEmpty()) {
			node = queue.remove();
			if (k < arr.length && !arr[k].equals("#")) {
				TreeNode tmp = new TreeNode(Integer.parseInt(arr[k]));
				node.left = tmp;
				queue.add(node.left);	
			}
			k++;
			if (k < arr.length && !arr[k].equals("#")) {
				TreeNode tmp = new TreeNode(Integer.parseInt(arr[k]));
				node.right = tmp;
				queue.add(node.right);	
			}
			k++;
		}
		return rootNode;
	}
	
	public static String convertToString(TreeNode root) {
		if (root == null) return "#";
		StringBuilder builder = new StringBuilder();
		Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);
		while(!queue.isEmpty()) {
			TreeNode node = queue.remove();
			builder.append(",");
			if (node == null || node.val == -1) {
				builder.append("#");
			} else {
				builder.append(node.val);	
				if (node.left == null) queue.add(new TreeNode(-1));
				else queue.add(node.left);
				if (node.right == null) queue.add(new TreeNode(-1));
				else queue.add(node.right);
			}

		}
		return builder.toString().replaceFirst(",", "");
	}
}
