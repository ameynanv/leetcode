package sort;

public class SortNumbersInRangeN2 {
    
    private void countSort(int[] nums, int base, int exp) {
        int[] output = new int[nums.length];
        int[] count = new int[base];
        for (int i = 0; i < nums.length; i++)
            count[(nums[i]/exp)%base]++;
        // find actual positions
        for (int i = 1; i < base; i++)
            count[i] += count[i-1];
        
        // write it to output array
        for (int i = nums.length - 1; i >= 0 ; i--) {
            output[count[(nums[i]/exp)%base] - 1] = nums[i];
            count[(nums[i]/exp)%base]--;
        }
        
        for (int i = 0; i < nums.length; i++)
            nums[i] = output[i];
        
    }
    
    public void sort(int[] nums) {
        countSort(nums, nums.length, 1);
        countSort(nums, nums.length, nums.length);
    }
    
    public static void main(String[] args) {
        int[] nums = {40, 12, 45, 32, 33, 1, 22};
        SortNumbersInRangeN2 service = new SortNumbersInRangeN2();
        service.sort(nums);
        for (int i = 0; i < nums.length; i++)
            System.out.print(nums[i] + " ");
        System.out.println("");
    }
    
}