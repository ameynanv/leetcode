package sort;

import java.util.Random;

public class QuickSort {

	@SuppressWarnings("rawtypes")
	public void shuffle(Comparable[] array) {
		Random randGen = new Random();
		int N = array.length;
		for (int i = 0; i < N; i++) {
			int randIndex = i + randGen.nextInt(N-i);
			Comparable temp = array[i];
			array[i] = array[randIndex];
			array[randIndex] = temp;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void sort(Comparable[] array) {
		// Random Shuffle the array
		shuffle(array);
		sort(array, 0, array.length - 1);
	}
	
	@SuppressWarnings("rawtypes")
	private void sort(Comparable[] array, int start, int end) {
		if (start >= end) return;
		int p = partition(array, start, end);
		sort(array, start, p-1);
		sort(array, p+1, end);
	}
	
	@SuppressWarnings("rawtypes")
	private int partition(Comparable[] array, int start, int end) {
		// partition places array[start] to correct position and spits out its index;
		int lo = start;
		int hi = end;
		int i = start + 1;
		Comparable value = array[start];
		while (lo < hi) {
			if (less(array[i], value)) {
				exchange(array, i, lo);
				lo++;
				i++;
			} else {
				exchange(array, i, hi);
				hi--;
			}
		}
		return lo;
	}
	
	
	@SuppressWarnings("rawtypes")
	private void exchange(Comparable[] array, int index1, int index2) {
		Comparable temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private boolean less(Comparable a, Comparable b) {
		return a.compareTo(b) < 0;
	}
	
	public static void main(String[] args) {
		Integer[] array = {6,5,4,3,2,1};
		QuickSort service = new QuickSort();
		service.sort(array);
		for (int i = 0; i < array.length; i++)
			System.out.print(array[i] + ",");
	}
	
}
