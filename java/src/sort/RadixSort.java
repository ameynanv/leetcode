package sort;

import java.util.Arrays;

public class RadixSort {
    
    private int findMax(int[] nums) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < nums.length; i++)
            max = Math.max(max, nums[i]);
        return max;
    }
    
    private void countSort(int[] nums, int exp) {
        int[] output = new int[nums.length]; // output array
        int i;
        int count[] = new int[10];
        Arrays.fill(count,0);
        
        // Store count of occurrences in count[]
        for (i = 0; i < nums.length; i++)
            count[(nums[i]/exp)%10]++;
        
        // Change count[i] so that count[i] now contains actual
        //  position of this digit in output[]
        for (i = 1; i < 10; i++)
            count[i] += count[i - 1];
 
        // Build the output array
        for (i = nums.length - 1; i >= 0; i--)
        {
            output[count[(nums[i]/exp)%10] - 1] = nums[i];
            count[(nums[i]/exp)%10 ]--;
        }
        
        
        // Copy the output array to arr[], so that arr[] now
        // contains sorted numbers according to curent digit
        for (i = 0; i < nums.length; i++)
            nums[i] = output[i];
    }
    
    public void sort(int[] nums) {
        int max = findMax(nums);
        
        for (int exp = 1; max/exp > 0; exp *= 10) 
            countSort(nums, exp);
        
    }
    
    public static void main(String[] args) {
        int[] nums = {82,31,14,55,33,23};
        RadixSort service = new RadixSort();
        service.sort(nums);
        for (int i = 0; i < nums.length; i++)
            System.out.print(nums[i] + " ");
        System.out.println("");
    }
}