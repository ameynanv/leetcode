package sort;

public class HeapSort {

	@SuppressWarnings("rawtypes")
	public void sort(Comparable[] array) {
		int L = array.length;
		for (int i = L/2 - 1; i >= 0; i--)
			sink(array, i, L);
		
		int LastIndex = L - 1;
		while (LastIndex >= 0) {
			exchange(array, 0, LastIndex);
			sink(array, 0, LastIndex);
			LastIndex--;
		}
	}
	
	// for element of index i, its children are present in 2*i + 1 and 2*i + 2
	@SuppressWarnings("rawtypes")
	private void sink(Comparable[] array, int i, int L) {
		if (2*i + 1 < L) {
			int maxChildIndex;
			if (2*i+2 < L) {
				if (less(array[2*i + 1], array[2*i + 2])) 
					maxChildIndex = 2*i + 2;
				else 
					maxChildIndex = 2*i + 1;
			} else {
				maxChildIndex = 2*i + 1;
			}
			
			if (less(array[i], array[maxChildIndex])) {
				exchange(array, i, maxChildIndex);
				sink(array, maxChildIndex, L);
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	private void exchange(Comparable[] array, int index1, int index2) {
		Comparable temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private boolean less(Comparable a, Comparable b) {
		return a.compareTo(b) < 0;
	}
	
	public static void main(String[] args) {
		Integer[] array = {1,2,3,4,5,6};
		HeapSort service = new HeapSort();
		service.sort(array);
		for (int i = 0; i < array.length; i++)
			System.out.print(array[i] + ",");
	}
}
