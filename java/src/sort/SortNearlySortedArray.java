package sort;
import java.util.PriorityQueue;

/**
 * Given an array of n elements, where each element is at most k away from its
 * target position, devise an algorithm that sorts in O(n log k) time.
 * 
 * http://www.geeksforgeeks.org/nearly-sorted-algorithm/
 */
public class SortNearlySortedArray {

	public void sort(int[] nums, int k) {
		PriorityQueue<Integer> minHeap = new PriorityQueue<Integer>();
		int i = 0;
		while (i < nums.length && i < k) 
			minHeap.add(nums[i++]);
		for (i = 0; i < nums.length; i++) {
			nums[i] = minHeap.remove();
			if (i+k < nums.length)
				minHeap.add(nums[i+k]);
		}
	}
	
	private void printArray(int[] nums) {
		for (int i = 0; i < nums.length; i++)
			System.out.print(nums[i] + " ");
		System.out.println("");
	}
	
	public static void main(String[] args) {
		SortNearlySortedArray service = new SortNearlySortedArray();
		
		int[] nums = {2, 6, 3, 12, 56, 8};
		service.sort(nums, 3);
		service.printArray(nums);
		
		int[] nums2 = {2};
		service.sort(nums2, 3);
		service.printArray(nums2);

		
		int[] nums3 = {1,2,3,4,5,6};
		service.sort(nums3, 1);
		service.printArray(nums3);

		int[] nums4 = {};
		service.sort(nums4, 10);
		service.printArray(nums4);
		
	}
}
