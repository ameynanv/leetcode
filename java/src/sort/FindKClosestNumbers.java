package sort;

/**
 * Given a sorted array arr[] and a value X, find the k closest elements to X in
 * arr[]. Note that if the element is present in array, then it should not be in
 * output, only the other closest elements are required.
 * 
 * http://www.geeksforgeeks.org/find-k-closest-elements-given-value/
 */
public class FindKClosestNumbers {

	// find k closest numbers to target.
	// nums is sorted array
	public int[] findKClosestNumbers(int[] nums, int k, int target) {
		int[] result = new int[k];
		int n = binarySearch(nums, target);
		int left = n - 1;
		int right = n;
		int count = 0;
		while (left >= 0 && right <= nums.length - 1 && count < k) {
			if (target - nums[left] == 0) {
				left--;
			} 
			if (target - nums[right] == 0) {
				right++;
			}
			if (target - nums[left] < nums[right] - target) {
				result[count++] = nums[left];
				left--;
			} else {
				result[count++] = nums[right];
				right++;
			}
		}
		while (count < k && left >= 0) {
			result[count++] = nums[left];
			left--;
		}
		while (count < k && right <= nums.length - 1) {
			result[count++] = nums[right];
			right++;
		}
		return result;
	}
	
	public int binarySearch(int[] nums, int target) {
		int start = 0;
		int end = nums.length-1;
		
		while (start < end) {
			int mid = (start + end) >>> 1;
			if (nums[mid] == target) return mid;
			if (nums[mid] < target)
				start = mid + 1;
			else
				end = mid;
		}
		return start;
	}
	
	private void printArray(int[] nums) {
		for (int i = 0; i < nums.length; i++)
			System.out.print(nums[i] + " ");
		System.out.println("");
	}

	public static void main(String[] args) {
		FindKClosestNumbers service = new FindKClosestNumbers();
		int[] nums = {12, 16, 22, 30, 35, 39, 42, 45, 48, 50, 53, 55, 56};
		int[] result = service.findKClosestNumbers(nums, 4, 35);
		service.printArray(result);
	}

}
