package sort;

/**
 * http://www.geeksforgeeks.org/minimum-length-unsorted-subarray-sorting-which-
 * makes-the-complete-array-sorted/
 * 
 * Given an unsorted array arr[0..n-1] of size n, find the minimum length
 * subarray arr[s..e] such that sorting this subarray makes the whole array
 * sorted.
 *
 * Complexity O(n)
 */
public class MinimumLengthArrayToSort {

	public int minLengthToSort(int[] nums) {
		int left = 0;
		int right = nums.length - 1;
		
		// find out first occurrence of unsorted behavior from left and from right 
		while (left < nums.length - 1 && nums[left] <= nums[left+1]) 
			left++;
		if (left == nums.length - 1) return 0;
		while (right > 0 && nums[right] >= nums[right-1])
			right--;
		
		// find min and max within nums(left .. right)
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for (int i = left; i <= right; i++) {
			min = Math.min(min, nums[i]);
			max = Math.max(max, nums[i]);
		}
			
		while(right < nums.length && nums[right] <= max) 
			right++;
		while(left >= 0 && nums[left] >= min)
			left--;
		
		return (right < left) ? 0 : right-left-1;
		
	}
	
	
	
	public static void main(String[] args) {
		MinimumLengthArrayToSort service = new MinimumLengthArrayToSort();
		int[] nums = {0, 1, 15, 25, 6, 7, 30, 40, 50};
		System.out.println(service.minLengthToSort(nums)); // 4
		int[] nums2 = {10, 12, 20, 30, 25, 40, 32, 31, 35, 50, 60};
		System.out.println(service.minLengthToSort(nums2)); // 6
		int[] nums3 = {10, 12, 20, 30, 40, 50, 60};
		System.out.println(service.minLengthToSort(nums3)); // 0
		int[] nums4 = {10, 12, 20, 19, 40, 50, 60};
		System.out.println(service.minLengthToSort(nums4)); // 2

	}

}
