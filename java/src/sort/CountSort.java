package sort;

public class CountSort {

	public void sort(int[] nums) {
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < nums.length; i++)
			max = Math.max(max, nums[i]);

		int[] output = new int[nums.length];
		int[] count = new int[max + 1];
		for (int i = 0; i < nums.length; i++)
			count[nums[i]]++;
		for (int i = 1; i < max + 1; i++)
			count[i] += count[i - 1];
		for (int i = nums.length - 1; i >= 0; i--) {
			output[count[nums[i]] - 1] = nums[i];
			count[nums[i]]--;
		}
		for (int i = 0; i < nums.length; i++)
			nums[i] = output[i];
	}

	public static void main(String[] args) {
		CountSort service = new CountSort();
		int[] nums = { 1, 4, 1, 2, 7, 5, 2 };
		service.sort(nums);
		for (int i = 0; i < nums.length; i++)
			System.out.print(nums[i] + " ");
		System.out.println("");
	}
}
