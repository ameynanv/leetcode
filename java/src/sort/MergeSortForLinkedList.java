package sort;

public class MergeSortForLinkedList {
	public static class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
			next = null;
		}
	}
	
	public ListNode sort(ListNode head) {
		int N = size(head);
		return sort(head, 0, N);
	}

	private int size(ListNode head) {
		int N = 0;
		ListNode temp = head;
		while (temp != null) {
			N++;
			temp = temp.next;
		}
		return N;
	}

	private ListNode merge(ListNode n1, ListNode n2) {
		ListNode result = null;
		ListNode tmp = null;;
		while (n1 != null && n2 != null) {
			if (n1.val <= n2.val) {
				if (result == null) {
					result = n1;
					tmp = result;
				} else {
					tmp.next = n1;
					tmp = tmp.next;
				}
				n1 = n1.next;
			} else {
				if (result == null) {
					result = n2;
					tmp = result;
				} else {
					tmp.next = n2;
					tmp = tmp.next;
				}
				n2 = n2.next;
			}
		}
		while (n1 != null) {
			tmp.next = n1;
			n1 = n1.next;
			tmp = tmp.next;
		}
		while (n2 != null) {
			tmp.next = n2;
			n2 = n2.next;
			tmp = tmp.next;
		}
		return result;
	}

	private ListNode sort(ListNode head, int start, int end) {
		if (end <= start + 1)
			return head;

		int mid = (start + end) >> 1;
		ListNode midNode = head;
		for (int k = 0; k < mid - 1; k++)
			midNode = midNode.next;
		ListNode prevNode = midNode;
		midNode = midNode.next;
		prevNode.next = null;
		
		ListNode n1 = sort(head, start, mid);
		ListNode n2 = sort(midNode, 0, end-mid);

		ListNode result = merge(n1, n2);
		return result;
	}
	
	public String convertToString(ListNode root) {
		if (root == null) return "NULL";
		ListNode t = root;
		StringBuilder builder = new StringBuilder();
		builder.append(t.val);
		while (t.next != null) {
			builder.append(t.next.val);
			t = t.next;
		}
		return builder.toString();
	}
	
	public static void main(String[] args) {
		ListNode head = new MergeSortForLinkedList.ListNode(5);
		head.next = new ListNode(1);
		head.next.next = new ListNode(2);
		head.next.next.next = new ListNode(4);
		head.next.next.next.next = new ListNode(3);
		MergeSortForLinkedList service = new MergeSortForLinkedList();
		head = service.sort(head);
		System.out.println(service.convertToString(head));
	}
}
