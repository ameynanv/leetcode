package queues;

import java.util.ArrayDeque;

public class StackUsingQueues {
	
	private ArrayDeque<Integer> q1;
	private ArrayDeque<Integer> q2;
	Integer top;
	
	public StackUsingQueues() {
		q1 = new ArrayDeque<Integer>();
		q2 = new ArrayDeque<Integer>();
		top = null;
	}
	
	// Push element x onto stack.
    public void push(int x) {
    	top = x;
        q1.add(x);
    }

    // Removes the element on top of the stack.
    public void pop() {
    	if (!empty()) {
    		while(q1.size() > 1) {
    			top = q1.remove();
    			q2.add(top);
    		}
    		q1.remove();
    		ArrayDeque<Integer> temp = q2;
    		q2 = q1;
    		q1 = temp;
    	}
    }

    // Get the top element.
    public int top() {
        return top;
    }

    // Return whether the stack is empty.
    public boolean empty() {
        return q1.isEmpty();
    }
    
    public static void main(String[] args) {
    	StackUsingQueues service = new StackUsingQueues();
    	service.push(1);
    	service.pop();
    	service.push(2);
    	service.push(3);
    	while(!service.empty()) {
    		System.out.println(service.top());
    		service.pop();
    	}
    }
}

