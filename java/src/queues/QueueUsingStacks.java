package queues;
import java.util.Stack;

public class QueueUsingStacks {
	
	private Stack<Integer> S1 = new Stack<Integer>();
	private Stack<Integer> S2 = new Stack<Integer>();

	// Push element x to the back of queue.
	public void push(int x) {
		S2.push(x);
	}

	// Removes the element from in front of queue.
	public void pop() {
		peek();
		S1.pop();
	}

	// Get the front element.
	public int peek() {
		if (S1.isEmpty()) {
			while(!S2.isEmpty()) {
				S1.push(S2.pop());
			}
		}
		return S1.peek();
	}

	// Return whether the queue is empty.
	public boolean empty() {
		return S1.isEmpty() && S2.isEmpty();
	}
	
	
	public static void main(String[] args) {
		QueueUsingStacks queue = new QueueUsingStacks();
		queue.push(1);
		queue.pop();
		queue.push(2);
		queue.push(3);
		
		while(!queue.empty()) {
			System.out.println(queue.peek());
			queue.pop();
		}
	}

}
