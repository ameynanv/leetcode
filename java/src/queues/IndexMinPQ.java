package queues;

import java.util.ArrayList;

public class IndexMinPQ<K extends Comparable<K>, V> {

	private ArrayList<Node> list;
	private int L;

	private class Node {
		K weight;
		V value;

		public Node(K weight, V value) {
			this.weight = weight;
			this.value = value;
		}
	}

	public IndexMinPQ() {
		list = new ArrayList<Node>();
		L = 0;
	}

	public void insert(K weight, V value) {
		list.add(new Node(weight, value));
		swim(L);
		L++;
	}

	// return a minimal weight
	public K minWeight() {
		return list.get(0).weight;
	}

	// return a value of the item with min weight
	public V minValue() {
		return list.get(0).value;
	}

	// remove a minimal item and return its value
	public V delMin() {
		V minValue = list.get(0).value;
		exchange(0, L-1);
		sink(0);
		list.remove(L-1);
		L--;
		return minValue;
	}

	// is the priority queue empty?
	public boolean isEmpty() {
		return L == 0;
	}

	// number of items in the priority queue
	public int size() {
		return L;
	}

	private void exchange(int index1, int index2) {
		Node temp = list.get(index1);
		list.get(index1).weight = list.get(index2).weight;
		list.get(index1).value = list.get(index2).value;
		list.get(index2).weight = temp.weight;
		list.get(index2).value = temp.value;
	}
	
	private void swim(int k) {
		while (k > 0 && more((k-1)/2, k)) {
			exchange((k-1)/2, k);
			k = (k-1)/2;
		}
	}
	
	private void sink(int k) {
		while (2*k+1 < L) {
			int minChildIndex;
			if (2*k+2 < L) {
				minChildIndex = more(2*k+1, 2*k+2) ? 2*k+2 : 2*k+1;
			} else {
				minChildIndex = 2*k+1;
			}
			if (more(k, minChildIndex)) {
				exchange(k, minChildIndex);
				k = minChildIndex;
			}
		}
	}
	
	private boolean more(int i, int j) {
		return list.get(i).weight.compareTo(list.get(j).weight) > 0;
	}

}
