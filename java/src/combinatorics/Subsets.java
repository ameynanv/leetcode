package combinatorics;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

/**
 * https://leetcode.com/problems/subsets/
 * 
 * Given a set of distinct integers, nums, return all possible subsets.
 * 
 * Note: Elements in a subset must be in non-descending order. The solution set
 * must not contain duplicate subsets. For example, If nums = [1,2,3], a
 * solution is:
 * 
 * [ [3], [1], [2], [1,2,3], [1,3], [2,3], [1,2], [] ]
 */
public class Subsets {

	public List<List<Integer>> subsets(int[] nums) {
		Arrays.sort(nums);
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		result.add(new ArrayList<Integer>());
		int L = nums.length;
		for (int k = 0; k < L ; k++) {
			List<List<Integer>> t = new ArrayList<List<Integer>>();
			for (List<Integer> list : result) {
				List<Integer> clone = new ArrayList<Integer>(list);
				clone.add(nums[k]);
				t.add(clone);
			}
			result.addAll(t);
		}
		return result;
	}


	
	public static void main(String[] args) {
		Subsets service = new Subsets();
		int[] nums = {1,3,2};
		System.out.println(service.subsets(nums));
	}

}
