package combinatorics;
import java.util.Arrays;
/**
 * https://leetcode.com/problems/next-permutation/
 * 
 * Implement next permutation, which rearranges numbers into the
 * lexicographically next greater permutation of numbers.
 * 
 * If such arrangement is not possible, it must rearrange it as the lowest
 * possible order (ie, sorted in ascending order).
 * 
 * The replacement must be in-place, do not allocate extra memory.
 * 
 * Here are some examples. Inputs are in the left-hand column and its
 * corresponding outputs are in the right-hand column. 
 * 1,2,3 → 1,3,2 
 * 3,2,1 → 1,2,3 
 * 1,1,5 → 1,5,1
 * 
 * 
 * https://en.wikipedia.org/wiki/Permutation#Generation_in_lexicographic_order
 */
public class NextPermutation {

	public void nextPermutation(int[] nums) {
		// find max i such that nums[i] < nums[i+1]
		int maxI = -1;
		for (int i = 0; i < nums.length - 1; i++) 
			if (nums[i] < nums[i+1]) maxI = i;
		if (maxI == -1) {
			Arrays.sort(nums);
			return;
		}
		// find max j greater than i such that nums[i] < nums[j]
		int maxJ = maxI+1;
		for (int j = maxI+1; j < nums.length; j++)
			if (nums[maxI] < nums[j]) maxJ = j;
		
		// swap i and j
		swap(nums, maxI, maxJ);
		
		// reverse elements from i+1 onwards.
		int l = maxI+1;
		int r = nums.length-1;
		while( l < r ) {
			swap(nums, l, r);
			l++; r--;
		}
	}
	
	private void swap(int[] nums, int i, int j) {
		int temp = nums[i];
		nums[i] = nums[j];
		nums[j] = temp;
	}

	public static void main(String[] args) {
		NextPermutation service = new NextPermutation();
		int[] nums = {1,4,3,2};
		service.nextPermutation(nums);
		for (int i = 0; i < nums.length; i++)
			System.out.print(nums[i] + " ");
		System.out.println("");
	}

}
