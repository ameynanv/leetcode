package combinatorics;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class SubsetsII {
	public List<List<Integer>> subsetsWithDup(int[] nums) {
		Arrays.sort(nums);
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		result.add(new ArrayList<Integer>());
		int i = 0;
		
		while(i < nums.length) {
			List<List<Integer>> tmp = new ArrayList<List<Integer>>();
			int dupCnt = 1;
			while (dupCnt > 0) {
				for (List<Integer> list: result) {
					List<Integer> clone = new ArrayList<Integer>(list);
					for (int k = 0; k < dupCnt; k++)
						clone.add(nums[i]);
					tmp.add(clone);
				}
				if (i+1 < nums.length && nums[i] == nums[i+1]) {
					dupCnt++;
				} else {
					dupCnt = 0;
				}
				i++;
			}			
			result.addAll(tmp);
		}
		return result;
	}
	
	public static void main(String[] args) {
		SubsetsII service = new SubsetsII();
		int[] nums = {1,2,2,3};
		System.out.println(service.subsetsWithDup(nums));
	}

}
