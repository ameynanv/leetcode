package combinatorics;

/**
 * The set [1,2,3, .. ,n] contains a total of n! unique permutations.
 * 
 * By listing and labeling all of the permutations in order, We get the
 * following sequence (ie, for n = 3):
 * 1: "123" 
 * 2: "132" 
 * 3: "213" 
 * 4: "231" 
 * 5: "312" 
 * 6: "321" 
 * Given n and k, return the kth permutation sequence.
 * Note: Given n will be between 1 and 9 inclusive.
 *
 */
public class PermutationSequence {
	
	public String getPermutation(int n, int k) {
		int[] factorial = new int[10];
		factorial[1] = 1;
		for (int i = 2; i < 10; i++)
			factorial[i] = factorial[i-1] * i;
		int[] nums = new int[n];
		for (int i = 0; i < n; i++)
			nums[i] = i+1;
		arrange(nums, 0, k-1, factorial);
		StringBuilder build = new StringBuilder();
		for (int i = 0; i < n; i++)
			build.append(nums[i]+"");
		
        return build.toString();
    }
	
	private void arrange(int[] nums, int start, int k, int[] factorial) {
		int L = nums.length - start;
		if (L == 1) return;
		int d = k/factorial[L-1];
		int r = k%factorial[L-1];
		int temp = nums[start + d];
		for (int i = start + d; i > start; i--)
			nums[i] = nums[i-1];
		nums[start] = temp;
		arrange(nums, start + 1, r, factorial);
	}
	
	public static void main(String[] args) {
		PermutationSequence service = new PermutationSequence();
		System.out.println(service.getPermutation(8,8590));
	}

}
