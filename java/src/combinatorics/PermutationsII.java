package combinatorics;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;


public class PermutationsII {
	public List<List<Integer>> permuteUnique(int[] nums) {
		Arrays.sort(nums);
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		result.add(toArrayList(nums));
		while(true) {
			if (!nextPermutation(nums)) break;
			result.add(toArrayList(nums));	
		}
        return result;
    }
	
	private boolean nextPermutation(int[] nums) {
		int i = -1, j = -1;
		for (int t = 0; t < nums.length-1; t++)
			if (nums[t] < nums[t+1]) i = t;
		if (i == -1) return false;
		for (int t = i+1; t < nums.length; t++)
			if (nums[i] < nums[t]) j = t;
		swap(nums, i, j);
		int l = i+1, r = nums.length-1;
		while(l < r) {
			swap(nums, l, r); 
			l++; 
			r--;
		}
		
		return true;
	}
	
	private void swap(int[] nums, int i, int j) {
		int temp = nums[i];
		nums[i] = nums[j];
		nums[j] = temp;
	}
	
	private ArrayList<Integer> toArrayList(int[] nums) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i= 0; i < nums.length; i++)
			list.add(nums[i]);
		return list;
	}
	
	public static void main(String[] args) {
		PermutationsII service = new PermutationsII();
		int[] nums = {1,2,3};
		System.out.println(service.permuteUnique(nums));
	}

}
