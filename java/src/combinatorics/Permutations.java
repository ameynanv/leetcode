package combinatorics;
import java.util.ArrayList;
import java.util.List;


public class Permutations {
	public List<List<Integer>> permute(int[] nums) {
		
        ArrayList<List<Integer>> solutions = new ArrayList<List<Integer>>();
        if (nums.length == 1) {
        	List<Integer> singleList= new ArrayList<Integer>();
        	singleList.add(nums[0]);
        	solutions.add(singleList);
        	return solutions;
        }
        int[] temp = new int[nums.length - 1];
        for (int i = 0; i < nums.length; i++) {
            copyNumbersExcept(nums, temp, nums[i]);
            List<List<Integer>> tempSolutions = permute(temp);
            for (int m = 0; m < tempSolutions.size(); m++) {
                List<Integer> list = tempSolutions.get(m);
                list.add(nums[i]);
                solutions.add(list);
            }
        }
        return solutions;
    }
    
    private void copyNumbersExcept(int[] input, int[] output, int number) {
        int k = 0;
        for (int i = 0; i < input.length; i++) {
            if (input[i] != number) {
                output[k++] = input[i];
            }
        }
    }
}
