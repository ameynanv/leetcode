package combinatorics;

import java.util.ArrayList;
import java.util.List;

//
// Given two integers n and k, return all possible combinations 
// of k numbers out of 1 ... n.
//
public class Combinations {
	public List<List<Integer>> combine(int n, int k) {
		if (n < k) {
			List<List<Integer>> result = new ArrayList<List<Integer>>();
			return result;
		}
		if (n == k || k == 0) {
			List<List<Integer>> result = new ArrayList<List<Integer>>();
			List<Integer> list = new ArrayList<Integer>();
			for (int i = 1; i <= k; i++)
				list.add(i);
			result.add(list);
			return result;
		}

		// C(n, k) = C(n-1, k-1) + C(n-1, k)
		List<List<Integer>> result = combine(n - 1, k - 1);
		for (int i = 0; i < result.size(); i++)
			result.get(i).add(n);
		result.addAll(combine(n - 1, k));
		return result;
	}

	public static void main(String[] args) {
		Combinations service = new Combinations();
		List<List<Integer>> result = service.combine(3, 0);
		System.out.println(result.size());
		for (List<Integer> list : result) {
			System.out.println(list);
		}
	}
}
