package linkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IntersectionOfTwoLinkedListTest {
	IntersectionOfTwoLinkedList service;

	@Before
	public void setUp() throws Exception {
		service = new IntersectionOfTwoLinkedList();
	}

	@Test
	public void test() {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(3);
		head.next.next.next = new ListNode(4);
		head.next.next.next.next = new ListNode(5);

		ListNode head2 = new ListNode(6);
		head2.next = head.next.next;

		Assert.assertEquals(3, service.getIntersection(head, head2).val);
	}
	
	@Test
	public void test2() {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(3);
		head.next.next.next = new ListNode(4);
		head.next.next.next.next = new ListNode(5);

		ListNode head2 = new ListNode(6);

		Assert.assertEquals(null, service.getIntersection(head, head2));
	}

}
