package linkedList;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class CheckLinkedListPalindromeTest {
	CheckLinkedListPalindrome service;

	@Before
	public void setUp() throws Exception {
		service = new CheckLinkedListPalindrome();
	}

	@Test
	public void test() {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(5);
		head.next.next.next = new ListNode(2);
		head.next.next.next.next = new ListNode(1);
		Assert.assertEquals(true, service.isPalindrome(head));
	}
	
	@Test
	public void test2() {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(5);
		head.next.next.next = new ListNode(3);
		head.next.next.next.next = new ListNode(1);
		Assert.assertEquals(false, service.isPalindrome(head));
	}

}
