package graph;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class BreadthFirstSearchTest {

	Graph G;

	@Before
	public void setUp() throws Exception {
		G = new Graph(13);
		G.addEdge(0, 5);
		G.addEdge(0, 1);
		G.addEdge(0, 2);
		G.addEdge(0, 6);
		G.addEdge(3, 5);
		G.addEdge(3, 4);
		G.addEdge(4, 5);
		G.addEdge(4, 6);
		G.addEdge(7, 8);
		G.addEdge(9, 10);
		G.addEdge(9, 11);
		G.addEdge(9, 12);
		G.addEdge(11, 12);
	}

	@Test
	public void test1() {
		BreadthFirstSearch dfs = new BreadthFirstSearch(G, 0);
		Assert.assertEquals(7, dfs.count());
		Assert.assertEquals(true, dfs.marked(4));
		Assert.assertEquals(false, dfs.marked(7));
	}
	
	@Test
	public void test2() {
		BreadthFirstSearch dfs = new BreadthFirstSearch(G, 7);
		Assert.assertEquals(2, dfs.count());
		Assert.assertEquals(true, dfs.marked(8));
		Assert.assertEquals(false, dfs.marked(2));
	}
	
	@Test
	public void test3() {
		BreadthFirstSearch dfs = new BreadthFirstSearch(G, 9);
		Assert.assertEquals(4, dfs.count());
		Assert.assertEquals(true, dfs.marked(11));
		Assert.assertEquals(false, dfs.marked(2));
	}

}
