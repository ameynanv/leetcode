package graph;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class DirectedCycleTest {

	Digraph G;

	@Before
	public void setUp() throws Exception {
		G = new Digraph(13);
		G.addEdge(4, 2);
		G.addEdge(2, 3);
		G.addEdge(6, 0);
		G.addEdge(0, 1);
		G.addEdge(2, 0);
		G.addEdge(11, 12);
		G.addEdge(12, 9);
		G.addEdge(9, 10);
		G.addEdge(9, 11);
		G.addEdge(8, 9);
		G.addEdge(10, 12);
		G.addEdge(11, 4);
		G.addEdge(4, 3);
		G.addEdge(3, 5);
		G.addEdge(7, 8);
		G.addEdge(8, 7);
		G.addEdge(5, 4);
		G.addEdge(0, 5);
		G.addEdge(6, 4);
		G.addEdge(6, 9);
		G.addEdge(7, 6);
	}

	@Test
	public void test() {
		DirectedCycle cycleDetect = new DirectedCycle(G);
		Assert.assertEquals(true, cycleDetect.hasCycle());
		System.out.println(cycleDetect.cycle());
	}

}
