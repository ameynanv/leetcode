package graph;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class BreadthFirstPathsTest {
	Graph G;
	@Before
	public void setUp() throws Exception {
		G = new Graph(13);
		G.addEdge(0, 5);
		G.addEdge(0, 1);
		G.addEdge(0, 2);
		G.addEdge(0, 6);
		G.addEdge(3, 5);
		G.addEdge(3, 4);
		G.addEdge(4, 5);
		G.addEdge(4, 6);
		G.addEdge(7, 8);
		G.addEdge(9, 10);
		G.addEdge(9, 11);
		G.addEdge(9, 12);
		G.addEdge(11, 12);
	}

	@Test
	public void test() {
		BreadthFirstPaths bfsPaths = new BreadthFirstPaths(G, 0);
		Assert.assertEquals(true, bfsPaths.hasPathTo(4));
		System.out.println(bfsPaths.pathTo(4));
	}

	@Test
	public void test1() {
		BreadthFirstPaths bfsPaths = new BreadthFirstPaths(G, 0);
		Assert.assertEquals(false, bfsPaths.hasPathTo(7));
		System.out.println(bfsPaths.pathTo(7));
	}
}
