package graph;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class DirectedDFSTest {
	Digraph G;

	@Before
	public void setUp() throws Exception {
		G = new Digraph(13);
		G.addEdge(4, 2);
		G.addEdge(2, 3);
		G.addEdge(3, 2);
		G.addEdge(6, 0);
		G.addEdge(0, 1);
		G.addEdge(2, 0);
		G.addEdge(11, 12);
		G.addEdge(12, 9);
		G.addEdge(9, 10);
		G.addEdge(9, 11);
		G.addEdge(8, 9);
		G.addEdge(10, 12);
		G.addEdge(11, 4);
		G.addEdge(4, 3);
		G.addEdge(3, 5);
		G.addEdge(7, 8);
		G.addEdge(8, 7);
		G.addEdge(5, 4);
		G.addEdge(0, 5);
		G.addEdge(6, 4);
		G.addEdge(6, 9);
		G.addEdge(7, 6);
	}

	@Test
	public void test() {
		DirectedDFS reachable = new DirectedDFS(G, 1);
		ArrayList<Integer> reachableList = new ArrayList<Integer>();
		for (int v = 0; v < G.V(); v++)
			if (reachable.marked(v)) reachableList.add(v);
		Assert.assertEquals("[1]", reachableList.toString());
	}
	
	@Test
	public void test1() {
		DirectedDFS reachable = new DirectedDFS(G, 2);
		ArrayList<Integer> reachableList = new ArrayList<Integer>();
		for (int v = 0; v < G.V(); v++)
			if (reachable.marked(v)) reachableList.add(v);
		Assert.assertEquals("[0, 1, 2, 3, 4, 5]", reachableList.toString());
	}
	
	@Test
	public void test2() {
		ArrayList<Integer> sources = new ArrayList<Integer>();
		sources.add(1);
		sources.add(2);
		sources.add(6);
		DirectedDFS reachable = new DirectedDFS(G, sources);
		ArrayList<Integer> reachableList = new ArrayList<Integer>();
		for (int v = 0; v < G.V(); v++)
			if (reachable.marked(v)) reachableList.add(v);
		Assert.assertEquals("[0, 1, 2, 3, 4, 5, 6, 9, 10, 11, 12]", reachableList.toString());
	}

}

