package graph;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
public class TopologicalSortTest {

	Digraph G;

	@Before
	public void setUp() throws Exception {
		G = new Digraph(13);
		G.addEdge(0, 1);
		G.addEdge(0, 5);
		G.addEdge(0, 6);
		G.addEdge(2, 0);
		G.addEdge(2, 3);
		G.addEdge(3, 5);
		G.addEdge(5, 4);
		G.addEdge(6, 4);
		G.addEdge(6, 9);
		G.addEdge(7, 6);
		G.addEdge(8, 7);
		G.addEdge(9, 10);
		G.addEdge(9, 11);
		G.addEdge(9, 12);
		G.addEdge(11, 12);
	}

	@Test
	public void test() {
		TopologicalSort tSort = new TopologicalSort(G);
		Assert.assertEquals("[8, 7, 2, 3, 0, 6, 9, 11, 12, 10, 5, 4, 1]", tSort.order().toString());
	}

}

