package graph;

import org.junit.Assert;

import org.junit.Test;

public class GraphTest {


	@Test
	public void test() {
		Graph g = new Graph(4);
		Assert.assertEquals(4, g.V());
		Assert.assertEquals(0, g.E());
		g.addEdge(0,1);
		g.addEdge(0,2);
		g.addEdge(2,3);
		g.addEdge(2,1);
		Assert.assertEquals(4, g.E());
		System.out.println(g.toString());
		
	}

}
