package graph;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class DepthFirstPathsTest {
	Graph G;
	@Before
	public void setUp() throws Exception {
		G = new Graph(13);
		G.addEdge(0, 5);
		G.addEdge(0, 1);
		G.addEdge(0, 2);
		G.addEdge(0, 6);
		G.addEdge(3, 5);
		G.addEdge(3, 4);
		G.addEdge(4, 5);
		G.addEdge(4, 6);
		G.addEdge(7, 8);
		G.addEdge(9, 10);
		G.addEdge(9, 11);
		G.addEdge(9, 12);
		G.addEdge(11, 12);
	}

	@Test
	public void test() {
		DepthFirstPaths dfsPaths = new DepthFirstPaths(G, 0);
		Assert.assertEquals(true, dfsPaths.hasPathTo(4));
		System.out.println(dfsPaths.pathTo(4));
	}

	@Test
	public void test1() {
		DepthFirstPaths dfsPaths = new DepthFirstPaths(G, 0);
		Assert.assertEquals(false, dfsPaths.hasPathTo(7));
		System.out.println(dfsPaths.pathTo(7));
	}
}
