package graph;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TwoColorProblemTest {

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void test() {
		Graph G = new Graph(5);
		G.addEdge(0, 1);
		G.addEdge(0, 2);
		G.addEdge(0, 3);
		G.addEdge(0, 4);
		G.addEdge(4, 3);

		TwoColorProblem bipartite = new TwoColorProblem(G);
		Assert.assertEquals(false, bipartite.isBipartite());
	}

	@Test
	public void test2() {
		Graph G = new Graph(5);
		G.addEdge(0, 1);
		G.addEdge(0, 2);
		G.addEdge(0, 3);
		G.addEdge(0, 4);

		TwoColorProblem bipartite = new TwoColorProblem(G);
		Assert.assertEquals(true, bipartite.isBipartite());
	}

}
