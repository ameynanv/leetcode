package maths;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class GreatestCommonDivisorTest {

	GreatestCommonDivisor service;
	@Before
	public void setUp() throws Exception {
		service = new GreatestCommonDivisor();
	}

	@Test
	public void test() {
		Assert.assertEquals(3, service.gcd(18, 3));
	}

}
