package dynamicProgramming;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class UglyNumbersTest {
	UglyNumbers service;
	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void test1() {
		service = new UglyNumbers();
		Assert.assertEquals(1, service.getNthUglyNumber(1));
	}

	@Test
	public void test2() {
		service = new UglyNumbers();
		Assert.assertEquals(2, service.getNthUglyNumber(2));
	}
	
	@Test
	public void test4() {
		service = new UglyNumbers();
		Assert.assertEquals(4, service.getNthUglyNumber(4));
	}
	
	@Test
	public void test11() {
		service = new UglyNumbers();
		Assert.assertEquals(15, service.getNthUglyNumber(11));
	}
}
