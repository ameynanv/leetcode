package dynamicProgramming;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class LongestCommonSubsequenceITest {
	LongestCommonSubsequenceI service;
	@Before
	public void setUp() throws Exception {
		service = new LongestCommonSubsequenceI();
	}

	@Test
	public void test() {
		int[] num1 = {1,2,3,4,5,6};
		int[] num2 = {1,7,4,8,6,9};
		Assert.assertEquals(3, service.longestCommonSubsequence(num1, num2));
	}

	@Test
	public void test1() {
		int[] num1 = {1};
		int[] num2 = {1,7,4,8,6,9};
		Assert.assertEquals(1, service.longestCommonSubsequence(num1, num2));
	}

	
	@Test
	public void test2() {
		int[] num1 = {1,2,3,4,5,6};
		int[] num2 = {};
		Assert.assertEquals(0, service.longestCommonSubsequence(num1, num2));
	}

	
	@Test
	public void test3() {
		int[] num1 = {1,2,3,4,5,6};
		int[] num2 = {7,8,9};
		Assert.assertEquals(0, service.longestCommonSubsequence(num1, num2));
	}

	@Test
	public void test4() {
		int[] num1 = {1,2,3,4,5,6};
		int[] num2 = {1,2,3,4,5,6};
		Assert.assertEquals(6, service.longestCommonSubsequence(num1, num2));
	}
}
