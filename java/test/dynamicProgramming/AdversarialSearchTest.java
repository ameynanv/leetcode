package dynamicProgramming;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class AdversarialSearchTest {
	AdversarialSearch service;
	@Before
	public void setUp() throws Exception {
		service = new AdversarialSearch();
	}

	@Test
	public void test() {
		int[] nums = {5, 3, 7, 10};
		Assert.assertEquals(15, service.maximizeGameValue(nums));
	}
	
	@Test
	public void test1() {
		int[] nums = {8, 15, 3, 7};
		Assert.assertEquals(22, service.maximizeGameValue(nums));
	}
	
	@Test
	public void test2() {
		int[] nums = {2,2,2,2};
		Assert.assertEquals(4, service.maximizeGameValue(nums));
	}
	
	@Test
	public void test3() {
		int[] nums = {20, 30, 2, 2, 2, 10};
		Assert.assertEquals(42, service.maximizeGameValue(nums));
	}
	
}
