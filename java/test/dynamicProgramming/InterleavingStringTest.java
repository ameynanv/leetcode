package dynamicProgramming;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class InterleavingStringTest {
	InterleavingStringIII service;
	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void test() {
		service = new InterleavingStringIII();
    	String s1 = "aabcc";
    	String s2 = "dbbca";
    	String s3 = "aadbbcbcac";
    	Assert.assertEquals(true, service.isInterleave(s1, s2, s3));
	}
	
	@Test
	public void test1() {
		service = new InterleavingStringIII();
    	String s1 = "aabcc";
    	String s2 = "dbbca";
    	String s3 = "aadbbcbccc";
    	Assert.assertEquals(false, service.isInterleave(s1, s2, s3));
	}
	
	@Test
	public void test2() {
		service = new InterleavingStringIII();
    	String s1 = "aabc";
    	String s2 = "aae";
    	String s3 = "aabcaae";
    	Assert.assertEquals(true, service.isInterleave(s1, s2, s3));
	}

	@Test
	public void test3() {
		service = new InterleavingStringIII();
    	String s1 = "a";
    	String s2 = "b";
    	String s3 = "ab";
    	Assert.assertEquals(true, service.isInterleave(s1, s2, s3));
	}
	
	@Test
	public void test4() {
		service = new InterleavingStringIII();
    	String s1 = "ab";
    	String s2 = "def";
    	String s3 = "adbef";
    	Assert.assertEquals(true, service.isInterleave(s1, s2, s3));
	}

}
