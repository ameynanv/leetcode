package dynamicProgramming;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class CoinChangeTest {
	CoinChange service;
	
	@Before
	public void setUp() throws Exception {
		service = new CoinChange();
	}

	@Test
	public void test() {
		int N = 1;
		int[] S = {1,2,3};
		Assert.assertEquals(1, service.numberOfWaysOfChange(S,N));
	}

	@Test
	public void test2() {
		int N = 2;
		int[] S = {1,2,3};
		Assert.assertEquals(2, service.numberOfWaysOfChange(S,N));
	}

	@Test
	public void test3() {
		int N = 3;
		int[] S = {1,2,3};
		Assert.assertEquals(3, service.numberOfWaysOfChange(S,N));
	}
	
	@Test
	public void test4() {
		int N = 10;
		int[] S = {5, 2, 3, 6};
		Assert.assertEquals(5, service.numberOfWaysOfChange(S,N));
	}
}
