package dynamicProgramming;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EditDistanceTest {
	EditDistance service;

	@Before
	public void setUp() throws Exception {
		service = new EditDistance();
	}

	@Test
	public void test() {
		String source = "SUNDAY";
		String target = "SATURDAY";
		Assert.assertEquals(3, service.editDistance(source, target));
	}

}
