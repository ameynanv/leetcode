package dynamicProgramming;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class LongestIncreasingSubsequenceITest {
	LongestIncreasingSubsequenceI service;
	LongestIncreasingSubsequenceII service2;

	@Before
	public void setUp() throws Exception {
		service = new LongestIncreasingSubsequenceI();
		service2 = new LongestIncreasingSubsequenceII();
	}

	@Test
	public void test() {
		int[] num = {10,22,9,33,21,50,41,60,80};
		Assert.assertEquals(6, service.longestIncrSubsequenceLength(num));
		Assert.assertEquals(6, service2.longestIncrSubsequenceLength(num));
	}

}
