package arrays;

public class TestUtils {
	public static String convertToString(int[] arr) {
		if (arr == null) return "NULL";
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < arr.length; i++) {
			builder.append(arr[i]);	
		}
		return builder.toString();
	}
}
