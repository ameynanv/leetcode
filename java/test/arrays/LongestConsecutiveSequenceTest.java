package arrays;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class LongestConsecutiveSequenceTest {
	LongestConsecutiveSequence service;
	@Before
	public void setUp() throws Exception {
		service = new LongestConsecutiveSequence();
	}

	@Test
	public void test() {
		int[] nums = {100,4,200,1,3,2};
		Assert.assertEquals(4, service.longestConsecutive(nums));
	}
	
	@Test
	public void test2() {
		int[] nums = {100,4,-1,1,3,2,0};
		Assert.assertEquals(6, service.longestConsecutive(nums));
	}

}
