package arrays;

import org.junit.Before;
import org.junit.Test;

public class NextGreaterElementTest {
	NextGreaterElement service;
	@Before
	public void setUp() throws Exception {
		service = new NextGreaterElement();
	}

	@Test
	public void test() {
		int[] nums = {13,7,6,12};
		service.nextGreaterElement(nums);
	}

}
