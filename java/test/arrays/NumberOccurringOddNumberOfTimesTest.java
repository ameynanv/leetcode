package arrays;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class NumberOccurringOddNumberOfTimesTest {
	NumberOccurringOddNumberOfTimes service;
	@Before
	public void setUp() throws Exception {
		service = new NumberOccurringOddNumberOfTimes();
	}

	@Test
	public void test() {
		int[] nums = {1,2,3,1,2,3,5,5,5};
		Assert.assertEquals(5, service.oddNumber(nums));
	}

	@Test
	public void test1() {
		int[] nums = {1};
		Assert.assertEquals(1, service.oddNumber(nums));
	}

	@Test
	public void test2() {
		int[] nums = {1,2,3,1,2};
		Assert.assertEquals(3, service.oddNumber(nums));
	}
}
