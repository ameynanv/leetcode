package arrays;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class CountInversionTest {
	CountInversions service;
	@Before
	public void setUp() throws Exception {
		service = new CountInversions();
	}

	@Test
	public void test() {
		int[] nums = {2, 4, 1, 3, 5};
		Assert.assertEquals(3, service.countInversions(nums));
	}

	@Test
	public void test1() {
		int[] nums = {1,5,8,2,3,4};
		Assert.assertEquals(6, service.countInversions(nums));
	}
}
