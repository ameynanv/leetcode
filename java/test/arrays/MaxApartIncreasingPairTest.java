package arrays;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class MaxApartIncreasingPairTest {
	MaxApartIncreasingPair service; 
	@Before
	public void setUp() throws Exception {
		service = new MaxApartIncreasingPair();
	}

	@Test
	public void test() {
		int[] nums = {34,8,10,3,2,80,30,33,1};
		Assert.assertEquals(6, service.maxDistanceOfIncreasingPair(nums));
	}
	
	@Test
	public void test1() {
		int[] nums = {9, 2, 3, 4, 5, 6, 7, 8, 18, 0};
		Assert.assertEquals(8, service.maxDistanceOfIncreasingPair(nums));
	}

	@Test
	public void test2() {
		int[] nums = {1, 2, 3, 4, 5, 6};
		Assert.assertEquals(5, service.maxDistanceOfIncreasingPair(nums));
	}
	
	@Test
	public void test3() {
		int[] nums = {6, 5, 4, 3, 2, 1};
		Assert.assertEquals(0, service.maxDistanceOfIncreasingPair(nums));
	}
}
