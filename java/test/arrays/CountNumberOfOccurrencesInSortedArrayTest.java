package arrays;
import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

import arrays.CountNumberOfOccurrencesInSortedArray;


public class CountNumberOfOccurrencesInSortedArrayTest {
	CountNumberOfOccurrencesInSortedArray service;
	@Before
	public void setUp() throws Exception {
		service = new CountNumberOfOccurrencesInSortedArray();
	}

	@Test
	public void test() {
		int nums[] = {0,0,0};
		Assert.assertEquals(0, service.countOccurrences(nums, 1));
	}

	@Test
	public void test1() {
		int nums[] = {0,0,1};
		Assert.assertEquals(1, service.countOccurrences(nums, 1));
	}
	
	@Test
	public void test2() {
		int nums[] = {1,1,1};
		Assert.assertEquals(3, service.countOccurrences(nums, 1));
	}
	
	@Test
	public void test3() {
		int nums[] = {2,3,4};
		Assert.assertEquals(0, service.countOccurrences(nums, 1));
	}
}
