package arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MergeTwoSortedArraysIITest {
	MergeTwoSortedArraysII service;
	@Before
	public void setUp() throws Exception {
		service = new MergeTwoSortedArraysII();
	}

	@Test
	public void test() {
		int[] arr = {1,2,3,-1,-1,-1};
		int[] nums = {4,5,6};
		service.merge(arr, nums);
		Assert.assertEquals("123456", TestUtils.convertToString(arr));
	}
	


}
