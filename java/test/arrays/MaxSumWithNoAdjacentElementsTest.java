package arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MaxSumWithNoAdjacentElementsTest {
	MaxSumWithNoAdjacentElements service;

	@Before
	public void setUp() throws Exception {
		service = new MaxSumWithNoAdjacentElements();
	}

	@Test
	public void test() {
		int[] nums = {3,2,7,10};
		Assert.assertEquals(13, service.maxSumWithNoAdj(nums));
	}
	
	@Test
	public void test1() {
		int[] nums = {5,5,10,40,50,35};
		Assert.assertEquals(80, service.maxSumWithNoAdj(nums));
	}

	@Test
	public void test2() {
		int[] nums = {5, 5, 10, 100, 10, 5};
		Assert.assertEquals(110, service.maxSumWithNoAdj(nums));
	}
	
}
