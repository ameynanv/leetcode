package arrays;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class RotateArrayTest {
	RotateArrayII service;
	@Before
	public void setUp() throws Exception {
		service = new RotateArrayII();
	}

	@Test
	public void test() {
		int[] nums = {1,2,3,4,5,6};
		service.rotate(nums, 3);
		Assert.assertEquals("456123", TestUtils.convertToString(nums));
	}
	
	@Test
	public void test1() {
		int[] nums = {1};
		service.rotate(nums, 5);
		Assert.assertEquals("1", TestUtils.convertToString(nums));
	}
	
	@Test
	public void test2() {
		int[] nums = {1,2,3};
		service.rotate(nums, 1);
		Assert.assertEquals("231", TestUtils.convertToString(nums));
	}
	
	@Test
	public void test3() {
		int[] nums = {1,2,3,4,5,6};
		service.rotate(nums, 6);
		Assert.assertEquals("123456", TestUtils.convertToString(nums));
	}

}
