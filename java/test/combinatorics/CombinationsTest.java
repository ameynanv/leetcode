package combinatorics;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class CombinationsTest {
	Combinations service;
	
	@Before
	public void setUp() throws Exception {
		service = new Combinations();
	}

	@Test
	public void test1() {
		Assert.assertEquals(15, service.combine(6, 4).size());
	}

	@Test
	public void test2() {
		Assert.assertEquals(1, service.combine(6, 6).size());
	}
	
	@Test
	public void test3() {
		Assert.assertEquals(15, service.combine(6, 2).size());
	}
	
	@Test
	public void test4() {
		Assert.assertEquals(20, service.combine(6, 3).size());
	}
}
