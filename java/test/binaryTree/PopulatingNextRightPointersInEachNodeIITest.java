package binaryTree;

import org.junit.Before;
import org.junit.Test;

public class PopulatingNextRightPointersInEachNodeIITest {
	PopulatingNextRightPointersInEachNodeII service;
	@Before
	public void setUp() throws Exception {
		service = new PopulatingNextRightPointersInEachNodeII();
	}

	@Test
	public void test() {
		TreeLinkNode root = new TreeLinkNode(1);
		root.left = new TreeLinkNode(2);
		root.right = new TreeLinkNode(3);
		root.left.left = new TreeLinkNode(4);
		root.left.right = new TreeLinkNode(5);
		root.right.right = new TreeLinkNode(6);
		root.left.left.left = new TreeLinkNode(7);
		root.right.right.right = new TreeLinkNode(8);
		service.connect(root);


	}

}

