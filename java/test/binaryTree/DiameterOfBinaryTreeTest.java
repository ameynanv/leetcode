package binaryTree;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class DiameterOfBinaryTreeTest {

	DiameterOfBinaryTree service;
	@Before
	public void setUp() throws Exception {
		service = new DiameterOfBinaryTree();
	}

	@Test
	public void test() {
		TreeNode root = TreeNode.createTree("1,4,2,9,5,#,3,10,#,#,6,#,4,11,#,7,8");
		Assert.assertEquals(7, service.diameterOfBinaryTree(root));
	}

}
