package binaryTree;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class CheckBinaryTreeIsBSTTest {
	CheckBinaryTreeIsBST service;

	@Before
	public void setUp() throws Exception {
		service = new CheckBinaryTreeIsBST();
	}

	@Test
	public void test() {
		TreeNode root = TreeNode.createTree("20,8,22,4,12,#,#,#,#,10,14");
		Assert.assertEquals(true, service.checkBinaryTreeIsBST(root));
	}

	@Test
	public void test2() {
		TreeNode root = TreeNode.createTree("20,8,22,4,12,#,#,#,#,10,21");
		Assert.assertEquals(false, service.checkBinaryTreeIsBST(root));
	}
}
